﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using CommandLine;
using CommandLine.Text;

namespace DOL.Commands
{
    /// <summary>
    /// Base Class for Command Handler.
    /// </summary>
    public abstract class AbstractCommandHandler
    {
        /// <summary>
        /// Default Verb when none given.
        /// </summary>
        protected abstract string DefaultVerb { get; }

        /// <summary>
        /// Collection of Verbs Subtypes.
        /// </summary>
        Type[] Verbs { get; set; }
        /// <summary>
        /// Default Verb if available.
        /// </summary>
        Type Default { get; set; }
        
        /// <summary>
        /// Execute this Command with given Arguments.
        /// </summary>
        /// <param name="args">Arguments Collection.</param>
        /// <returns>string.Empty if no errors.</returns>
        public virtual string ParseCommand(string[] args)
        {
            using (var parser = new Parser())
            {
                var err = Enumerable.Empty<Error>();
                
                var result = parser.ParseArguments(args, Verbs)
                    .WithParsed(verb => OnVerbCommand(verb as ICommandVerb))
                    .WithNotParsed(errors => err = errors);
                
                if (err.Any())
                {
                    if (Default != null && err.Any(error => error.Tag == ErrorType.BadVerbSelectedError || error.Tag == ErrorType.NoVerbSelectedError))
                    {
                        result = parser.ParseArguments(new [] { DefaultVerb }.Concat(args), Default)
                            .WithParsed(verb => OnVerbCommand(verb as ICommandVerb))
                            .WithNotParsed(errors => err = errors);
                    }
                }
                
                if (result.Tag == ParserResultType.NotParsed)
                    return HelpTextBuild(result, err);
            }
            
            return string.Empty;
        }
        
        /// <summary>
        /// Trigger Verb Command Handler.
        /// </summary>
        /// <param name="verb"></param>
        protected virtual void OnVerbCommand(ICommandVerb verb)
        {
            verb.OnCommand();
        }
        
        /// <summary>
        /// Build a Help Text from Command Parsing Results and Errors.
        /// </summary>
        /// <param name="result">Parser Results.</param>
        /// <param name="errors">Passer Errors.</param>
        /// <returns>Help/Error Text to display.</returns>
        protected virtual string HelpTextBuild(ParserResult<object> result, IEnumerable<Error> errors)
        {
            return HelpText.AutoBuild(result, 120);
        }

        /// <summary>
        /// Create a new instance of <see cref="AbstractCommandHandler"/>
        /// </summary>
        protected AbstractCommandHandler()
        {
            // Retrieve Verbs Nested Class
            var commandVerb = GetType().GetNestedTypes()
                .Select(type => new { Type = type, Attr = type.GetCustomAttribute<VerbAttribute>() })
                .Where(verb => verb.Attr != null && typeof(ICommandVerb).IsAssignableFrom(verb.Type))
                .ToArray();
            
            Verbs = commandVerb.Select(verb => verb.Type).ToArray();
            Default = commandVerb.Where(verb => verb.Attr.Name.Equals(DefaultVerb, StringComparison.OrdinalIgnoreCase)).Select(verb => verb.Type).FirstOrDefault();
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Reflection;

namespace DOL
{
    /// <summary>
    /// Disposable Component with Event Clean up.
    /// </summary>
    public abstract class DisposableComponent : IDisposable
    {
        int _Disposed;
        
        /// <summary>
        /// Is this Component Initialized ?
        /// </summary>
        public bool Initialized { get { return System.Threading.Interlocked.CompareExchange(ref _Disposed, 0, 0) == 1; } }
        
        /// <summary>
        /// Is this Component Disposed ?
        /// </summary>
        public bool Disposed { get { return System.Threading.Interlocked.CompareExchange(ref _Disposed, 0, 0) == 2; } }
        
        /// <summary>
        /// Create a new Instance of <see cref="DisposableComponent"/>
        /// </summary>
        protected DisposableComponent()
        {
            _Disposed = 0;
        }
        
        /// <summary>
        /// Disposable Component Post-Init Method.
        /// </summary>
        protected abstract void OnInit();
        
        /// <summary>
        /// Disposable Component Pre-Dispose Method.
        /// </summary>
        protected abstract void OnDispose();
        
        /// <summary>
        /// Initialize Disposable Component.
        /// </summary>
        public void Init()
        {
            if (System.Threading.Interlocked.CompareExchange(ref _Disposed, 1, 0) == 0)
                OnInit();
            else
                throw new InvalidOperationException("Component Already Initialized");
        }
        
        /// <summary>
        /// Dispose Component.
        /// </summary>
        public void Dispose()
        {
            if (System.Threading.Interlocked.CompareExchange(ref _Disposed, 2, 1) == 1)
            {
                OnDispose();
                CleanEvents();
            }
            else
                throw new InvalidOperationException("Component Disposed or Uninitialized");
        }
        
        /// <summary>
        /// Clean up all Events attached to this Disposable Component.
        /// </summary>
        void CleanEvents()
        {
            foreach (var eventHandler in GetType().GetEvents(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                var eventInfo = eventHandler;
                var field = GetType().GetField(eventInfo.Name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                
                if (field != null)
                    field.SetValue(this, null);
            }
        }
    }
}

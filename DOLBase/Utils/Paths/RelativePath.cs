﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.IO;

namespace DOL.Utils.Paths
{
    /// <summary>
    /// Extensions Handling Relative Path
    /// </summary>
    public static class RelativePath
    {
        /// <summary>
        /// Build a Relative Path from given folder.
        /// </summary>
        /// <param name="file">Target file's path.</param>
        /// <param name="folder">Initial directory start path.</param>
        /// <returns>Resulting Relative Path.</returns>
        public static string GetRelativePath(this FileInfo file, DirectoryInfo folder)
        {
            return GetRelativePath(file.FullName, folder.FullName);
        }
        
        /// <summary>
        /// Build a Relative Path from given folder.
        /// </summary>
        /// <param name="filepath">Target file's path.</param>
        /// <param name="folder">Initial directory start path.</param>
        /// <returns>Resulting Relative Path.</returns>
        public static string GetRelativePath(string filepath, string folder)
        {
            var pathUri = new Uri(filepath);
            
            // Folders must end in a slash
            if (!folder.EndsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.Ordinal))
            {
                folder += Path.DirectorySeparatorChar;
            }
            var folderUri = new Uri(folder);
            
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('/', Path.DirectorySeparatorChar));
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;

namespace DOL.Utils.Text
{
    /// <summary>
    /// Utils for displaying HexDump
    /// </summary>
    public static class HexDump
    {
        /// <summary>
        /// Return a formatted Hex Dump from byte array.
        /// </summary>
        /// <param name="data">byte array to format.</param>
        /// <returns>Formatted string.</returns>
        public static string ToHexDump(this byte[] data)
        {
            return data.Select((b, i) => new { Byte = b, Line = i / 16 })
                .GroupBy(lineObj => lineObj.Line)
                .Select(
                    grp => string.Format("{0,-47} {1}",
                                         grp.Select(lineObj => string.Format("{0:X2}", lineObj.Byte))
                                         .Aggregate((str, byteVal) => string.Format("{0} {1}", str, byteVal)),
                                         new String(grp.Select(lineObj => lineObj.Byte < 32 ? '.' : (char)lineObj.Byte).ToArray())
                                        )
                   )
                .Select((byteStr, line) => string.Format("{0:X4}: {1}", line * 16, byteStr))
                .Aggregate("", (dump, byteLine) => string.Format("{0}{1}{2}", dump, byteLine, Environment.NewLine));
        }
    }
}

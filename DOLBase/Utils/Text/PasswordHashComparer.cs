﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace DOL.Utils.Text
{
    /// <summary>
    /// Extension Class for comparing Password String to Hash
    /// </summary>
    public static class PasswordHash
    {
        static byte[] CreateSalt(int size)
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var buff = new byte[size];
                rng.GetBytes(buff);
                return buff;
            }
        }
        
        public static string GeneratePasswordHash(this string password)
        {
            using (var hashManager = new SHA512Managed())
            {
                var salt = CreateSalt(16);
                return string.Format("{{SHA512}}{{{0}}}{1}",
                                     Convert.ToBase64String(salt),
                                     Convert.ToBase64String(hashManager.ComputeHash(Encoding.UTF8.GetBytes(password).Concat(salt).ToArray())));
            }
        }
        
        public static bool EqualsPasswordHash(this string password, string hash)
        {
            var matches = Regex.Matches(hash, "{(.*)}{(.*)}(.+)");
            
            if (matches.Count == 1 && matches[0].Groups.Count == 4)
            {
                var algorithm = matches[0].Groups[1].Value;
                switch(algorithm.ToLower())
                {
                    case "sha512":
                        return EqualsPasswordHashSha512(Convert.FromBase64String(matches[0].Groups[3].Value), Convert.FromBase64String(matches[0].Groups[2].Value), password);
                    default:
                        return false;
                }
            }
            
            return false;
        }
        
        /// <summary>
        /// PHP Equivalent (as UTF-8 obviously), $hash = base64_encode(hash("sha512", $password.$salt, true))
        /// </summary>
        /// <param name="hash"></param>
        /// <param name="salt"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        static bool EqualsPasswordHashSha512(byte[] hash, byte[] salt, string password)
        {
            using (var hashManager = new SHA512Managed())
            {
                return hashManager.ComputeHash(Encoding.UTF8.GetBytes(password).Concat(salt).ToArray()).SequenceEqual(hash);
            }
        }
    }
}

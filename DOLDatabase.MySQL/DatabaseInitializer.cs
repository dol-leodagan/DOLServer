﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;

using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.Annotations;

using log4net;

namespace DOL.Database.MySQL
{
    /// <summary>
    /// MySQL Database Initializer
    /// </summary>
    public class DatabaseInitializer<TContext> : IDatabaseInitializer<TContext> where TContext : BaseContext<TContext>
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Database Model Builder Object.
        /// </summary>
        readonly DbModelBuilder ModelBuilder;
        
        readonly Handler Handler;
        
        public void InitializeDatabase(TContext context)
        {
            // Build the Model and execute alteration in a transaction without foreign keys checks
            var model = ModelBuilder.Build(context.Database.Connection);
            
            try
            {
                context.Database.UseTransaction(Handler.InitializationTransaction);
                
                try
                {
                    CreateOrUpdateDatabase(context.Database, model);
                }
                catch (Exception e)
                {
                    if (log.IsErrorEnabled)
                        log.Error("Error while Initializing Database Context", e);
                    
                    throw e;
                }
            }
            finally
            {
                context.Database.UseTransaction(null);
            }
        }
        
        /// <summary>
        /// Create Database Schema or Update Current Schema
        /// </summary>
        /// <param name="database"></param>
        /// <param name="model"></param>
        void CreateOrUpdateDatabase(System.Data.Entity.Database database, DbModel model)
        {
            // Retrieving Existing Metadata.
            var databaseMetadata = Handler.GetDatabaseMetadata(database);
            string[] existingTables = databaseMetadata.Tables;
            Dictionary<string, TableInfo[]> existingTablesInfo = databaseMetadata.TablesInfo;
            Dictionary<string, Dictionary<string, IndexInfo[]>> existingTablesIndexInfo = databaseMetadata.TablesIndexInfo;
            Dictionary<string, Dictionary<string, ForeignKeyList[]>> existingTablesForeignKeys = databaseMetadata.TablesForeignKeys;
            Dictionary<string, Dictionary<string, ForeignKeyConstraint>> existingTablesForeignKeyConstraints = databaseMetadata.TablesForeignKeyConstraints;
            
            var container = model.StoreModel.Container;
            
            // Check for any change to a table or if table is missing.
            foreach(var entitySet in container.EntitySets.Where(es => !Handler.InitializedTables.Contains(es.Table)))
            {
                // Build index definitions for comparison with existing ones
                var indicies = entitySet.ElementType.Properties
                    .Select(prop => prop.MetadataProperties
                            .Select(x => x.Value)
                            .OfType<IndexAnnotation>()
                            .SelectMany(ia => ia.Indexes)
                            .Select(it => new {
                                        Name = it.Name ?? string.Format("{2}_{0}_{1}", entitySet.Table, prop.Name, it.IsUnique ? "UQ" : "IX"),
                                        Column = prop.Name,
                                        it.Order,
                                        it.IsUnique,
                                    })
                           )
                    .SelectMany(ind => ind)
                    .GroupBy(ind => ind.Name, StringComparer.OrdinalIgnoreCase)
                    .ToDictionary(k => k.Key, v => new IndexDefinition {
                                      Columns = v.OrderBy(ind => ind.Order).Select(ind => ind.Column).ToArray(),
                                      IsUnique = v.Any(ind => ind.IsUnique)
                                  });
                
                // Match table name to existing tables
                var tableName = existingTables.SingleOrDefault(tbl => tbl.Equals(entitySet.Table, StringComparison.OrdinalIgnoreCase));
                
                if (tableName != null)
                {
                    var sqls = UpdateTable(entitySet,
                                           container,
                                           indicies,
                                           existingTablesInfo[tableName],
                                           existingTablesIndexInfo[tableName],
                                           existingTablesForeignKeys[tableName],
                                           existingTablesForeignKeyConstraints[tableName]);
                    
                    foreach (var sql in sqls)
                        database.ExecuteSqlCommand(sql);
                }
                else
                {
                    // Table Creation
                    var sql = CreateTable(entitySet, entitySet.Table, container, indicies);
                    database.ExecuteSqlCommand(sql);
                }
                
                // Register Table as Initialized
                Handler.InitializedTables.Add(entitySet.Table);
            }
        }
        
        string[] UpdateTable(EntitySet entitySet,
                             EntityContainer container,
                             Dictionary<string,
                             IndexDefinition> indicies,
                             TableInfo[] tableInfo,
                             Dictionary<string, IndexInfo[]> tableIndexInfo,
                             Dictionary<string, ForeignKeyList[]> tableForeignKeys,
                             Dictionary<string, ForeignKeyConstraint> tableForeignKeyConstraints)
        {
            var entityPK = entitySet.ElementType.KeyProperties.Select(prop => prop.Name).ToArray();
            var existingIndicies = tableIndexInfo
                .Where(idx => !idx.Key.Equals("PRIMARY", StringComparison.OrdinalIgnoreCase))
                .Where(idx => !tableForeignKeys.Any(fk => fk.Key.Equals(idx.Key, StringComparison.OrdinalIgnoreCase)))
                .ToArray();
            
            var alterDefinitions = new List<string>();
            var stackDefinitions = new Stack<string>();
            
            // Check Removed Columns
            var removedCols = tableInfo.Select(tbl => tbl.Field).Except(entitySet.ElementType.Properties.Select(prop => prop.Name), StringComparer.OrdinalIgnoreCase);
            foreach (var removedCol in removedCols)
                alterDefinitions.Add(string.Format("    DROP COLUMN `{0}`", removedCol));
            
            // Check Missing Columns
            var missingCols = entitySet.ElementType.Properties.Select(prop => prop.Name).Except(tableInfo.Select(tbl => tbl.Field), StringComparer.OrdinalIgnoreCase);
            foreach (var missingCol in missingCols.Select(col => entitySet.ElementType.Properties.Single(prop => prop.Name.Equals(col, StringComparison.OrdinalIgnoreCase))))
                alterDefinitions.Add(string.Format("    ADD COLUMN {0}", BuildColumnDefinition(missingCol, entityPK)));
            
            // Check Changed Columns
            var changedCols = entitySet.ElementType.Properties.Select(prop => prop.Name).Intersect(tableInfo.Select(tbl => tbl.Field), StringComparer.OrdinalIgnoreCase);
            foreach (var changedCol in changedCols.Select(col => new {
                                                              Existing = tableInfo.Single(field => field.Field.Equals(col, StringComparison.OrdinalIgnoreCase)),
                                                              Target = entitySet.ElementType.Properties.Single(prop => prop.Name.Equals(col, StringComparison.OrdinalIgnoreCase))
                                                          }))
            {
                if ((changedCol.Existing.Null.Equals("NO", StringComparison.OrdinalIgnoreCase) == changedCol.Target.Nullable)
                    || (!changedCol.Existing.Type.Equals(BuildTypeDefinition(changedCol.Target), StringComparison.OrdinalIgnoreCase))
                    || ((entityPK.Length == 1 && entityPK[0].Equals(changedCol.Target.Name, StringComparison.OrdinalIgnoreCase) && changedCol.Target.IsStoreGeneratedIdentity) != changedCol.Existing.Extra.Equals("AUTO_INCREMENT", StringComparison.OrdinalIgnoreCase)))
                    alterDefinitions.Add(string.Format("    CHANGE COLUMN `{0}` {1}", changedCol.Target.Name, BuildColumnDefinition(changedCol.Target, entityPK)));
            }
            
            // Check Primary Key
            var tablePK = tableInfo.Where(field => field.Key.Equals("PRI", StringComparison.OrdinalIgnoreCase)).Select(field => field.Field).ToArray();
            if (!((entityPK.Length == tablePK.Length)
                  && (entityPK.Intersect(tablePK, StringComparer.OrdinalIgnoreCase).Count() == entityPK.Length)))
            {
                alterDefinitions.Add("    DROP PRIMARY KEY");
                alterDefinitions.Add(string.Format("    ADD {0}", BuildPrimaryKeyDefinition(entityPK)));
            }
            
            // Check Removed Indicies
            var removedIdxs = existingIndicies.Select(idx => idx.Key)
                .Except(indicies
                        .Select(idx => idx.Key), StringComparer.OrdinalIgnoreCase);
            foreach (var removeIdx in removedIdxs)
                alterDefinitions.Add(string.Format("    DROP KEY `{0}`", removeIdx));
            
            // Check Missing Indicies
            var missingIdxs = indicies
                .Select(idx => idx.Key)
                .Except(existingIndicies
                        .Select(idx => idx.Key), StringComparer.OrdinalIgnoreCase);
            foreach (var missingIdx in missingIdxs.Select(idx => indicies.Single(ind => ind.Key.Equals(idx, StringComparison.OrdinalIgnoreCase))))
                alterDefinitions.Add(string.Format("    ADD {0}", BuildKeyDefinition(missingIdx.Key, missingIdx.Value)));
            
            // Check Changed Inidicies
            var changedIdxs = indicies
                .Select(idx => idx.Key)
                .Intersect(existingIndicies
                           .Select(idx => idx.Key), StringComparer.OrdinalIgnoreCase);
            foreach (var changedIdx in changedIdxs.Select(idx => new {
                                                              Existing = existingIndicies.Single(ind => ind.Key.Equals(idx, StringComparison.OrdinalIgnoreCase)),
                                                              Target = indicies.Single(ind => ind.Key.Equals(idx, StringComparison.OrdinalIgnoreCase))
                                                          }))
            {
                if (changedIdx.Existing.Value.All(idx => idx.Non_Unique) == changedIdx.Target.Value.IsUnique
                    || !changedIdx.Existing.Value
                    .OrderBy(idx => idx.Seq_in_index)
                    .Select(idx => idx.Column_name)
                    .SequenceEqual(changedIdx.Target.Value.Columns, StringComparer.OrdinalIgnoreCase)
                   )
                {
                    alterDefinitions.Add(string.Format("    DROP KEY `{0}`", changedIdx.Existing.Key));
                    alterDefinitions.Add(string.Format("    ADD {0}", BuildKeyDefinition(changedIdx.Target.Key, changedIdx.Target.Value)));
                }
            }
            
            var foreignKeys = container.AssociationSets.Where(assoc => assoc.ElementType.Constraint.ToRole.Name == entitySet.Name).ToArray();
            // Check Removed Foreign Keys
            var removedFKs = tableForeignKeys
                .Select(fk => fk.Key)
                .Except(foreignKeys
                        .Select(fk => BuildForeignKeyName(fk, entitySet.Table, container.GetEntitySetByName(fk.ElementType.Constraint.FromRole.Name, true).Table)),
                        StringComparer.OrdinalIgnoreCase);
            
            foreach (var removedFK in removedFKs)
                alterDefinitions.Add(string.Format("    DROP FOREIGN KEY `{0}`", removedFK));
            
            // Check For Missing Foreign Keys
            var missingFKs = foreignKeys
                .Select(fk => BuildForeignKeyName(fk, entitySet.Table, container.GetEntitySetByName(fk.ElementType.Constraint.FromRole.Name, true).Table))
                .Except(tableForeignKeys.Select(fk => fk.Key), StringComparer.OrdinalIgnoreCase);
            foreach (var missingFK in missingFKs.Select(fk => foreignKeys.Single(assoc => fk.Equals(BuildForeignKeyName(assoc, entitySet.Table, container.GetEntitySetByName(assoc.ElementType.Constraint.FromRole.Name, true).Table), StringComparison.OrdinalIgnoreCase))))
            {
                stackDefinitions.Push(string.Format("ALTER TABLE `{0}` ADD {1}",
                                                    entitySet.Table,
                                                    BuildForeignKeyDefinition(missingFK, container)));
            }
            
            // Check For Changed Foreign Keys
            var changedFKs = foreignKeys
                .Select(fk => BuildForeignKeyName(fk, entitySet.Table, container.GetEntitySetByName(fk.ElementType.Constraint.FromRole.Name, true).Table))
                .Intersect(tableForeignKeys.Select(fk => fk.Key), StringComparer.OrdinalIgnoreCase);
            
            foreach (var changedFK in changedFKs
                     .Select(fk => new {
                                 Existing = tableForeignKeys.Single(assoc => assoc.Key.Equals(fk, StringComparison.OrdinalIgnoreCase)),
                                 Target = foreignKeys.Single(assoc => fk.Equals(BuildForeignKeyName(assoc, entitySet.Table, container.GetEntitySetByName(assoc.ElementType.Constraint.FromRole.Name, true).Table), StringComparison.OrdinalIgnoreCase))
                             }))
            {
                var existingToField = changedFK.Existing.Value.OrderBy(efk => efk.ORDINAL_POSITION).Select(efk => efk.COLUMN_NAME);
                var existingFromField = changedFK.Existing.Value.OrderBy(efk => efk.ORDINAL_POSITION).Select(efk => efk.REFERENCED_COLUMN_NAME);
                var existingCascadeDelete = changedFK.Existing.Value.All(efk => tableForeignKeyConstraints[efk.CONSTRAINT_NAME].DELETE_RULE.Equals("CASCADE", StringComparison.OrdinalIgnoreCase));
                
                var targetToField = changedFK.Target.ElementType.Constraint.ToProperties.Select(tfk => tfk.Name);
                var targetFromField = changedFK.Target.ElementType.Constraint.FromProperties.Select(tfk => tfk.Name);
                var targetCascadeDelete = changedFK.Target.ElementType.Constraint.FromRole.DeleteBehavior == OperationAction.Cascade;
                
                if (!(existingCascadeDelete == targetCascadeDelete
                      && existingFromField.SequenceEqual(targetFromField, StringComparer.OrdinalIgnoreCase)
                      && existingToField.SequenceEqual(targetToField, StringComparer.OrdinalIgnoreCase)
                      && changedFK.Existing.Value.All(efk => tableForeignKeyConstraints[efk.CONSTRAINT_NAME].UPDATE_RULE.Equals("CASCADE", StringComparison.OrdinalIgnoreCase))))
                {
                    alterDefinitions.Add(string.Format("    DROP FOREIGN KEY `{0}`", changedFK.Existing.Key));
                    stackDefinitions.Push(string.Format("ALTER TABLE `{0}` ADD {1}",
                                                        entitySet.Table,
                                                        BuildForeignKeyDefinition(changedFK.Target, container)));
                }
            }
            
            if (alterDefinitions.Any())
                stackDefinitions.Push(string.Format("ALTER TABLE `{0}`{2}{1}",
                                                    entitySet.Table,
                                                    string.Join(string.Format(",{0}", Environment.NewLine), alterDefinitions),
                                                    Environment.NewLine));
            
            return stackDefinitions.ToArray();
        }
        
        string CreateTable(EntitySet entitySet, string tableName, EntityContainer container, Dictionary<string, IndexDefinition> indicies)
        {
            var entityPK = entitySet.ElementType.KeyProperties.Select(prop => prop.Name).ToArray();

            return string.Format("CREATE TABLE `{0}` ({2}{1}{2}){2}ENGINE=INNODB AUTO_INCREMENT=0",
                                 tableName,
                                 string.Join(string.Format(",{0}", Environment.NewLine),
                                             entitySet.ElementType.Properties
                                             .Select(prop => string.Format("    {0}", BuildColumnDefinition(prop, entityPK)))
                                             .Concat(new []{ string.Format("    {0}", BuildPrimaryKeyDefinition(entityPK)) })
                                             .Concat(container.AssociationSets.Where(assoc => assoc.ElementType.Constraint.ToRole.Name == entitySet.Name)
                                                     .Select(assoc => string.Format("    {0}", BuildForeignKeyDefinition(assoc, container))))
                                             .Concat(indicies
                                                     .Select(indx => string.Format("    {0}", BuildKeyDefinition(indx.Key, indx.Value))))
                                            ),
                                 Environment.NewLine
                                );
        }
        
        string BuildForeignKeyDefinition(AssociationSet assoc, EntityContainer container)
        {
            return string.Format("CONSTRAINT `{4}` FOREIGN KEY ({0}) REFERENCES `{1}` ({2}) {3}",
                                 string.Join(", ", assoc.ElementType.Constraint.ToProperties.Select(x => string.Format("`{0}`", x.Name))),
                                 container.GetEntitySetByName(assoc.ElementType.Constraint.FromRole.Name, true).Table,
                                 string.Join(", ", assoc.ElementType.Constraint.FromProperties.Select(x => string.Format("`{0}`", x.Name))),
                                 assoc.ElementType.Constraint.FromRole.DeleteBehavior == OperationAction.Cascade ? "ON UPDATE CASCADE ON DELETE CASCADE" : "ON UPDATE CASCADE",
                                 BuildForeignKeyName(assoc,
                                                     container.GetEntitySetByName(assoc.ElementType.Constraint.ToRole.Name, true).Table,
                                                     container.GetEntitySetByName(assoc.ElementType.Constraint.FromRole.Name, true).Table));
        }
        
        string BuildPrimaryKeyDefinition(string[] entityPK)
        {
            return string.Format("PRIMARY KEY ({0})",
                                 string.Join(", ", entityPK.Select(x => string.Format("`{0}`", x))));
        }
        
        string BuildKeyDefinition(string key, IndexDefinition definition)
        {
            return string.Format("{0}KEY `{1}` ({2})",
                                 definition.IsUnique ? "UNIQUE " : string.Empty,
                                 key,
                                 string.Join(", ",definition.Columns.Select(col => string.Format("`{0}`", col))));
        }
        
        string BuildColumnDefinition(EdmProperty prop, string[] entityPK)
        {
            return string.Format("`{0}` {1}{4} {2}{3}",
                                 prop.Name,
                                 BuildTypeDefinition(prop),
                                 prop.Nullable ? "NULL" : "NOT NULL",
                                 entityPK.Length == 1 && entityPK[0].Equals(prop.Name, StringComparison.OrdinalIgnoreCase) && prop.IsStoreGeneratedIdentity ? " AUTO_INCREMENT" : string.Empty,
                                 prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(Guid) ? " BINARY" : string.Empty);
        }
        
        string BuildForeignKeyName(AssociationSet assoc, string table, string toTable)
        {
            return string.Format("FK_{0}_{1}_{2}_{3}",
                                 table,
                                 toTable,
                                 string.Join("_", assoc.ElementType.Constraint.ToProperties.Select(x => x.Name)),
                                 string.Join("_", assoc.ElementType.Constraint.FromProperties.Select(x => x.Name)));
        }
        
        string BuildTypeDefinition(EdmProperty prop)
        {
            var type = prop.TypeName;
            
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(bool))
                return "TINYINT(1)";
            
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(byte))
                return "TINYINT(3) UNSIGNED";
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(sbyte))
                return "TINYINT(3)";
            
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(ushort))
                return "SMALLINT(6) UNSIGNED";
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(short))
                return "SMALLINT(6)";
            
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(uint))
                return "INT(11) UNSIGNED";
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(int))
                return "INT(11)";
            
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(ulong))
                return "BIGINT(20) UNSIGNED";
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(long))
                return "BIGINT(20)";
            
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(Guid))
                return "CHAR(36)";
            
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(double)
                || prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(float))
                return "DOUBLE";
            
            if (prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(string))
                return prop.MaxLength.HasValue && prop.MaxLength <= ushort.MaxValue ? string.Format("VARCHAR({0})", prop.MaxLength) : "LONGTEXT";
            
            if (prop.MaxLength.HasValue)
                type = string.Format("{0}({1})", type, prop.MaxLength);

            return type;
        }
        
        /// <summary>
        /// Create a new instance of MySQL <see cref="DatabaseInitializer{T}"/>.
        /// </summary>
        /// <param name="ModelBuilder">Database Model Builder Object.</param>
        /// <param name="Handler">Database Handler.</param>
        public DatabaseInitializer(DbModelBuilder ModelBuilder, Handler Handler)
        {
            this.ModelBuilder = ModelBuilder;
            this.Handler = Handler;
        }
    }
}
﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace DOL.Database.MySQL
{
    /// <summary>
    /// Create Metadata from Existing Database Schema.
    /// </summary>
    public class DatabaseMetadata
    {
        internal string[] Tables;
        internal Dictionary<string, TableInfo[]> TablesInfo { get; set; }
        internal Dictionary<string, Dictionary<string, IndexInfo[]>> TablesIndexInfo { get; set; }
        internal Dictionary<string, Dictionary<string, ForeignKeyList[]>> TablesForeignKeys { get; set; }
        internal Dictionary<string, Dictionary<string, ForeignKeyConstraint>> TablesForeignKeyConstraints { get; set; }
        
        public DatabaseMetadata(System.Data.Entity.Database database)
        {
            RetrieveMetadata(database);
        }
        
        void RetrieveMetadata(System.Data.Entity.Database database)
        {
            Tables = database.SqlQuery<string>("SHOW TABLES").ToArray();
            
            TablesInfo = Tables
                .Select(tbl => new {
                            Name = tbl,
                            Fields = database
                                .SqlQuery<TableInfo>(string.Format("DESCRIBE `{0}`", tbl))
                                .ToArray()
                        })
                .ToDictionary(key => key.Name, value => value.Fields);
            
            TablesIndexInfo = Tables
                .Select(tbl => new {
                            Table = tbl,
                            Indicies = database
                                .SqlQuery<IndexInfo>(string.Format("SHOW INDEX FROM `{0}`", tbl))
                                .GroupBy(indx => indx.Key_name, StringComparer.OrdinalIgnoreCase)
                                .ToDictionary(k => k.Key, v => v.ToArray())
                        })
                .ToDictionary(k => k.Table, v => v.Indicies);
            
            var foreignKeyQuery = string.Format("SELECT{0}  `CONSTRAINT_NAME`,{0}  `TABLE_NAME`,{0}  `COLUMN_NAME`,{0}  `ORDINAL_POSITION`,{0}  `REFERENCED_TABLE_SCHEMA`,{0}  `REFERENCED_TABLE_NAME`,{0}  `REFERENCED_COLUMN_NAME`{0}  FROM `INFORMATION_SCHEMA`.`KEY_COLUMN_USAGE`{0}  WHERE `REFERENCED_TABLE_SCHEMA` IS NOT NULL{0}    AND `CONSTRAINT_SCHEMA` LIKE @p0{0}    AND `TABLE_SCHEMA` LIKE @p0{0}    AND `TABLE_NAME` LIKE @p1", Environment.NewLine);
            
            TablesForeignKeys = Tables
                .Select(tbl => new {
                            Table = tbl,
                            ForeignKeys = database
                                .SqlQuery<ForeignKeyList>(foreignKeyQuery, database.Connection.Database, tbl)
                                .GroupBy(fk => fk.CONSTRAINT_NAME, StringComparer.OrdinalIgnoreCase)
                                .ToDictionary(k => k.Key, v => v.ToArray())
                        })
                .ToDictionary(k => k.Table, v => v.ForeignKeys);
            
            var constraintQuery = string.Format("SELECT{0}  `CONSTRAINT_NAME`,{0}  `UPDATE_RULE`,{0}  `DELETE_RULE`{0}  FROM `INFORMATION_SCHEMA`.`REFERENTIAL_CONSTRAINTS` WHERE{0}  `REFERENCED_TABLE_NAME` IS NOT NULL{0}    AND `CONSTRAINT_SCHEMA` LIKE @p0{0}    AND `TABLE_NAME` LIKE @p1", Environment.NewLine);
            
            TablesForeignKeyConstraints = Tables
                .Select(tbl => new {
                            Table = tbl,
                            Constraints = database
                                .SqlQuery<ForeignKeyConstraint>(constraintQuery, database.Connection.Database, tbl)
                                .ToDictionary(k => k.CONSTRAINT_NAME, v => v)
                        })
                .ToDictionary(k => k.Table, v => v.Constraints);
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;

using System.Data.Common;
using System.Data.Entity;

namespace DOL.Database.MySQL
{
    /// <summary>
    /// MySQL Database Handler.
    /// </summary>
    public class Handler : DatabaseHandler
    {
        public override DbConnection Connection
        {
            get
            {
                return _InitializationConnection ?? new MySql.Data.MySqlClient.MySqlConnection(ConnectionString);
            }
        }
        
        public override bool ContextOwnsConnection
        {
            get
            {
                return _InitializationConnection == null;
            }
        }
        
        internal HashSet<string> InitializedTables { get; private set; }
        
        DbTransaction _InitializationTransaction;
        DatabaseMetadata _DatabaseMetadata;
        readonly object _DatabaseMetadataLock = new object();
        DbConnection _InitializationConnection;
        
        public override DbTransaction InitializationTransaction {
            get {
                return _InitializationTransaction;
            }
            set {
                var transaction = value;
                
                // If Initialization is Finished.
                if (transaction == null)
                {
                    // Add Foreign Key Check after the Update
                    var cmd = _InitializationConnection.CreateCommand();
                    cmd.CommandText = "SET FOREIGN_KEY_CHECKS=1";
                    cmd.ExecuteNonQuery();

                    _InitializationConnection.Close();
                    _InitializationConnection.Dispose();
                    
                    _InitializationTransaction = null;
                    _InitializationConnection = null;

                    // Clear Metadata
                    lock(_DatabaseMetadataLock)
                        _DatabaseMetadata = null;
                    
                    InitializedTables.Clear();
                }
                else if (_InitializationTransaction != null)
                {
                    throw new InvalidOperationException("Initialization Transaction must be set to null before using another value !");
                }
                else
                {
                    _InitializationConnection = transaction.Connection;
                    _InitializationTransaction = transaction;
                    
                    // Remove Foreign Key Check for the Update
                    var cmd = _InitializationConnection.CreateCommand();
                    cmd.CommandText = "SET FOREIGN_KEY_CHECKS=0";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public override IDatabaseInitializer<T> GetInitializer<T>(DbModelBuilder modelBuilder)
        {
            return new DatabaseInitializer<T>(modelBuilder, this);
            //return new DropCreateDatabaseAlways<T>();
        }

        internal DatabaseMetadata GetDatabaseMetadata(System.Data.Entity.Database database)
        {
            lock (_DatabaseMetadataLock)
            {
                if (_DatabaseMetadata != null)
                {
                    return _DatabaseMetadata;
                }
                
                _DatabaseMetadata = new DatabaseMetadata(database);
                return _DatabaseMetadata;
            }
        }
        
        /// <summary>
        /// Create a new Instance of MySQL Database <see cref="Handler"/>
        /// </summary>
        /// <param name="ConnectionString">Connection String for this Handler.</param>
        public Handler(string ConnectionString)
            : base(ConnectionString)
        {
            InitializedTables = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            DbConfiguration.SetConfiguration(new MySql.Data.Entity.MySqlEFConfiguration());
        }
    }
}

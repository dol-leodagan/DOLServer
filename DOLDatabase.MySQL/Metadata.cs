﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

namespace DOL.Database.MySQL
{
    class TableInfo
    {
        public string Field { get; set; }
        public string Type { get; set; }
        public string Null { get; set; }
        public string Key { get; set; }
        public string Default { get; set; }
        public string Extra { get; set; }
    }
    
    class IndexInfo
    {
        public string Table { get; set; }
        public bool Non_Unique { get; set; }
        public string Key_name { get; set; }
        public int Seq_in_index { get; set; }
        public string Column_name { get; set; }
        public string Collation { get; set; }
        public int Cardinality { get; set; }
        public string Sub_part { get; set; }
        public string Packet { get; set; }
        public string Null { get; set; }
        public string Index_type { get; set; }
        public string Comment { get; set; }
        public string Index_Comment { get; set; }
    }
    
    class ForeignKeyList
    {
        public string CONSTRAINT_NAME { get; set; }
        public string TABLE_NAME { get; set; }
        public string COLUMN_NAME { get; set; }
        public int ORDINAL_POSITION { get; set; }
        public string REFERENCED_TABLE_SCHEMA { get; set; }
        public string REFERENCED_TABLE_NAME { get; set; }
        public string REFERENCED_COLUMN_NAME { get; set; }
    }
    
    class ForeignKeyConstraint
    {
        public string CONSTRAINT_NAME { get; set; }
        public string UPDATE_RULE { get; set; }
        public string DELETE_RULE { get; set; }
    }

    class IndexDefinition
    {
        public bool IsUnique { get; set; }
        public string[] Columns { get; set; }
    }
}

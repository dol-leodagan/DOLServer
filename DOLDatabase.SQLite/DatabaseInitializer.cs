﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;

using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.Annotations;

using log4net;

namespace DOL.Database.SQLite
{
    /// <summary>
    /// SQLite Database Initializer.
    /// </summary>
    public class DatabaseInitializer<TContext> : IDatabaseInitializer<TContext> where TContext : BaseContext<TContext>
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Database Model Builder Object.
        /// </summary>
        readonly DbModelBuilder ModelBuilder;
        
        readonly Handler Handler;

        public void InitializeDatabase(TContext context)
        {
            // Build the Model and execute alteration in a transaction without foreign keys checks
            var model = ModelBuilder.Build(context.Database.Connection);
            
            try
            {
                context.Database.UseTransaction(Handler.InitializationTransaction);
            
                    try
                    {
                        CreateOrUpdateDatabase(context.Database, model);
                    }
                    catch (Exception e)
                    {
                        if (log.IsErrorEnabled)
                            log.Error("Error while Initializing Database Context", e);
                        
                        throw e;
                    }
            }
            finally
            {
                context.Database.UseTransaction(null);
            }
        }
        
        bool TableNeedsAlteration(EntitySet entitySet, TableInfo[] tableInfo, Dictionary<int, ForeignKeyList[]> FKInfo, EntityContainer container)
        {
            var entityPK = entitySet.ElementType.KeyProperties.Select(prop => prop.Name).ToArray();

            // Check Columns / Fields first.
            if (entitySet.ElementType.Properties
                .Any(prop => tableInfo
                     .SingleOrDefault(field => {
                                          if (!prop.Name.Equals(field.name, StringComparison.OrdinalIgnoreCase))
                                              return false;
                                          
                                          if (prop.Nullable == field.notnull)
                                              return false;
                                          
                                          if (!BuildTypeDefinition(prop, entityPK).Equals(field.type, StringComparison.OrdinalIgnoreCase))
                                              return false;
                                          
                                          return true;
                                      }) == null))
                return true;
            
            // Check Primary Key.
            var tablePK = tableInfo.Where(field => field.pk).Select(field => field.name).ToArray();
            if (!((entityPK.Length == tablePK.Length)
                  && (entityPK.Intersect(tablePK, StringComparer.OrdinalIgnoreCase).Count() == entityPK.Length)))
                return true;
            
            // Check Foreign Key.
            var foreignKeys = container.AssociationSets.Where(assoc => assoc.ElementType.Constraint.ToRole.Name == entitySet.Name).ToArray();
            if ((FKInfo.Keys.Count != foreignKeys.Length)
                || foreignKeys
                .Any(fk => !FKInfo
                     .Any(existfk => {
                              var existingToField = existfk.Value.OrderBy(efk => efk.seq).Select(efk => efk.from);
                              var existingFromField = existfk.Value.OrderBy(efk => efk.seq).Select(efk => efk.to);
                              var existingCascadeDelete = existfk.Value.All(efk => efk.on_delete.Equals("CASCADE", StringComparison.OrdinalIgnoreCase));
                              
                              var targetToField = fk.ElementType.Constraint.ToProperties.Select(tfk => tfk.Name);
                              var targetFromField = fk.ElementType.Constraint.FromProperties.Select(tfk => tfk.Name);
                              var targetCascadeDelete = fk.ElementType.Constraint.FromRole.DeleteBehavior == OperationAction.Cascade;
                              
                              return existingCascadeDelete == targetCascadeDelete
                                  && existingFromField.SequenceEqual(targetFromField, StringComparer.OrdinalIgnoreCase)
                                  && existingToField.SequenceEqual(targetToField, StringComparer.OrdinalIgnoreCase)
                                  && existfk.Value.All(efk => efk.on_update.Equals("CASCADE", StringComparison.OrdinalIgnoreCase));
                          })))
                return true;
            
            return false;
        }
        
        /// <summary>
        /// Create Database Schema or Update Current Schema
        /// </summary>
        /// <param name="database"></param>
        /// <param name="model"></param>
        void CreateOrUpdateDatabase(System.Data.Entity.Database database, DbModel model)
        {
            // Retrieving Existing Metadata.
            var metadata = Handler.GetDatabaseMetadata(database);
            string[] existingTables = metadata.Tables;
            Dictionary<string, TableInfo[]> existingTablesInfo = metadata.TablesInfo;
            Dictionary<string, IndexList[]> existingTablesIndexList = metadata.TablesIndexList;
            Dictionary<string, Dictionary<string, IndexInfo[]>> existingTablesIndexInfo = metadata.TablesIndexInfo;
            Dictionary<string, Dictionary<int, ForeignKeyList[]>> existingTablesForeignKeys = metadata.TablesForeignKeys;

            var container = model.StoreModel.Container;
            
            // Check for any change to a table or if table is missing.
            foreach(var entitySet in container.EntitySets.Where(es => !Handler.InitializedTables.Contains(es.Table)))
            {
                // Build index definitions for comparison with existing ones
                var indicies = entitySet.ElementType.Properties
                    .Select(prop => prop.MetadataProperties
                            .Select(x => x.Value)
                            .OfType<IndexAnnotation>()
                            .SelectMany(ia => ia.Indexes)
                            .Select(it => new {
                                        Name = it.Name ?? string.Format("{2}_{0}_{1}", entitySet.Table, prop.Name, it.IsUnique ? "UQ" : "IX"),
                                        Column = prop.Name,
                                        it.Order,
                                        it.IsUnique,
                                    })
                           )
                    .SelectMany(ind => ind)
                    .GroupBy(ind => ind.Name, StringComparer.OrdinalIgnoreCase)
                    .ToDictionary(k => k.Key, v => new IndexDefinition {
                                      Columns = v.OrderBy(ind => ind.Order).Select(ind => ind.Column).ToArray(),
                                      IsUnique = v.Any(ind => ind.IsUnique)
                                  });
                
                // Match table name to existing tables
                var tableName = existingTables.SingleOrDefault(tbl => tbl.Equals(entitySet.Table, StringComparison.OrdinalIgnoreCase));
                
                if (tableName != null)
                {
                    // Check if Table need Alteration
                    if (TableNeedsAlteration(entitySet, existingTablesInfo[tableName], existingTablesForeignKeys[tableName], container))
                    {
                        var sqls = UpdateTable(entitySet, container, existingTablesInfo[tableName], existingTables);
                        foreach (var sql in sqls)
                            database.ExecuteSqlCommand(sql);
                    }
                    else
                    {
                        // Check for any change to an index
                        foreach (var existingIndex in existingTablesIndexList[tableName]
                                 .Where(exi => !exi.origin.Equals("pk", StringComparison.OrdinalIgnoreCase)))
                        {
                            var targetIndex = indicies.Keys.SingleOrDefault(ind => ind.Equals(existingIndex.name, StringComparison.OrdinalIgnoreCase));
                            
                            if (targetIndex == null)
                            {
                                // this index doesn't exists anymore !
                                var sql = string.Format("DROP INDEX [{0}]", existingIndex.name);
                                database.ExecuteSqlCommand(sql);
                            }
                            else
                            {
                                // Check if current index is still valid
                                var existingIndexInfo = existingTablesIndexInfo[tableName][existingIndex.name];
                                var targetIndexInfo = indicies[targetIndex];
                                if (existingIndex.unique == targetIndexInfo.IsUnique
                                    && existingIndexInfo
                                    .OrderBy(exi => exi.seqno)
                                    .Select(exi => exi.name)
                                    .SequenceEqual(targetIndexInfo.Columns, StringComparer.OrdinalIgnoreCase))
                                {
                                    // Skip valid index !
                                    indicies.Remove(targetIndex);
                                }
                                else
                                {
                                    // Rebuild index !
                                    var sql = string.Format("DROP INDEX [{0}]", existingIndex.name);
                                    database.ExecuteSqlCommand(sql);
                                }
                            }
                        }
                    }
                }
                else
                {
                    // Table Creation
                    var sql = CreateTable(entitySet, entitySet.Table, container);
                    database.ExecuteSqlCommand(sql);
                }
                
                // Create index
                var sqlindicies = CreateIndicies(indicies, entitySet.Table);
                foreach(var sql in sqlindicies)
                    database.ExecuteSqlCommand(sql);
                
                // Register Table as Initialized
                Handler.InitializedTables.Add(entitySet.Table);
            }
        }
        
        string CreateTable(EntitySet entitySet, string tableName, EntityContainer container)
        {
            var entityPK = entitySet.ElementType.KeyProperties.Select(prop => prop.Name).ToArray();
            
            return string.Format("CREATE TABLE [{0}] ({2}{1}{2})",
                                 tableName,
                                 string.Join(string.Format(",{0}", Environment.NewLine),
                                             entitySet.ElementType.Properties
                                             .Select(prop =>
                                                     string.Format("    [{0}] {1} {2}{3}",
                                                                   prop.Name,
                                                                   BuildTypeDefinition(prop, entityPK),
                                                                   prop.Nullable ? "NULL" : "NOT NULL",
                                                                   prop.UnderlyingPrimitiveType.ClrEquivalentType == typeof(string) ? " COLLATE NOCASE" : ""))
                                             .Concat(new []{ string.Format("    PRIMARY KEY ({0})",
                                                                                   string.Join(", ", entityPK.Select(x => string.Format("[{0}]", x)))) })
                                             .Concat(container.AssociationSets.Where(assoc => assoc.ElementType.Constraint.ToRole.Name == entitySet.Name)
                                                     .Select(assoc => string.Format("    FOREIGN KEY ({0}) REFERENCES [{1}] ({2}) {3}",
                                                                                    string.Join(", ", assoc.ElementType.Constraint.ToProperties.Select(x => string.Format("[{0}]", x.Name))),
                                                                                    container.GetEntitySetByName(assoc.ElementType.Constraint.FromRole.Name, true).Table,
                                                                                    string.Join(", ", assoc.ElementType.Constraint.FromProperties.Select(x => string.Format("[{0}]", x.Name))),
                                                                                    assoc.ElementType.Constraint.FromRole.DeleteBehavior == OperationAction.Cascade ? "ON UPDATE CASCADE ON DELETE CASCADE" : "ON UPDATE CASCADE")))
                                            ),
                                 Environment.NewLine
                                );
        }
        
        string[] UpdateTable(EntitySet entitySet, EntityContainer container, TableInfo[] existingTableInfo, string[] existingTables)
        {
            var temptable = string.Format("updating_{0}", entitySet.Table);
            
            var inc = 0;
            while (existingTables.Contains(temptable, StringComparer.OrdinalIgnoreCase))
                temptable = string.Format("updating_{0}{1}", entitySet.Table, inc++);
            
            var targetcolumns = string.Join(", ", existingTableInfo
                                            .Select(tbl => tbl.name)
                                            .Intersect(entitySet.ElementType.Properties
                                                       .Select(prop => prop.Name), StringComparer.OrdinalIgnoreCase)
                                            .Select(col => string.Format("[{0}]", col))
                                           );
            return new [] {
                CreateTable(entitySet, temptable, container),
                string.Format("INSERT INTO [{0}] ({1}) SELECT {1} FROM [{2}]", temptable, targetcolumns, entitySet.Table),
                string.Format("DROP TABLE [{0}]", entitySet.Table),
                string.Format("ALTER TABLE [{0}] RENAME TO [{1}]", temptable, entitySet.Table),
            };
        }
        
        IEnumerable<string> CreateIndicies(Dictionary<string, IndexDefinition> indicies, string tableName)
        {
            return indicies.Select(ind => ind.Value.IsUnique
                                   ? string.Format("CREATE UNIQUE INDEX [{0}] ON [{1}] ({2})", ind.Key, tableName, string.Join(", ", ind.Value.Columns.Select(col => string.Format("[{0}]", col))))
                                   : string.Format("CREATE INDEX [{0}] ON [{1}] ({2})", ind.Key, tableName, string.Join(", ", ind.Value.Columns.Select(col => string.Format("[{0}]", col))))
                                  );
        }
                
        string BuildTypeDefinition(EdmProperty prop, string[] pk)
        {
            var type = prop.TypeName;
            
            if (pk.Length == 1 && prop.Name.Equals(pk[0], StringComparison.OrdinalIgnoreCase) && prop.IsStoreGeneratedIdentity)
                type = "INTEGER";
            
            if (prop.MaxLength.HasValue)
                type = string.Format("{0}({1})", type, prop.MaxLength);
            
            return type;
        }
        
        /// <summary>
        /// Create a new instance of SQLite <see cref="DatabaseInitializer{T}"/>.
        /// </summary>
        /// <param name="ModelBuilder">Database Model Builder Object.</param>
        /// <param name="Handler">Database Handler.</param>
        public DatabaseInitializer(DbModelBuilder ModelBuilder, Handler Handler)
        {
            this.ModelBuilder = ModelBuilder;
            this.Handler = Handler;
        }
    }
}
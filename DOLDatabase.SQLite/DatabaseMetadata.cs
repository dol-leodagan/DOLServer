﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Collections.Generic;

namespace DOL.Database.SQLite
{
    /// <summary>
    /// Create Metadata from Existing Database Schema.
    /// </summary>
    public class DatabaseMetadata
    {
        internal string[] Tables { get; set; }
        internal Dictionary<string, TableInfo[]> TablesInfo { get; set; }
        internal Dictionary<string, IndexList[]> TablesIndexList { get; set; }
        internal Dictionary<string, Dictionary<string, IndexInfo[]>> TablesIndexInfo { get; set; }
        internal Dictionary<string, Dictionary<int, ForeignKeyList[]>> TablesForeignKeys { get; set; }
        
        public DatabaseMetadata(System.Data.Entity.Database database)
        {
            RetrieveMetadata(database);
        }
        
        void RetrieveMetadata(System.Data.Entity.Database database)
        {
            Tables = database.SqlQuery<string>("select [name] from [sqlite_master] where [type] = @p0", "table").ToArray();

            TablesInfo = Tables
                .Select(tbl => new {
                            Name = tbl,
                            Fields = database
                                .SqlQuery<TableInfo>(string.Format("PRAGMA table_info([{0}])", tbl))
                                .ToArray()
                        })
                .ToDictionary(key => key.Name, value => value.Fields);
            
            TablesIndexList = Tables
                .Select(tbl => new {
                            Name = tbl,
                            Indices = database
                                .SqlQuery<IndexList>(string.Format("PRAGMA index_list([{0}])", tbl))
                                .ToArray()
                        })
                .ToDictionary(key => key.Name, value => value.Indices);
            
            TablesIndexInfo = TablesIndexList
                .Select(indx => new {
                            Table = indx.Key,
                            IndexInfo = indx.Value
                                .Select(indicies => new {
                                            Index = indicies.name,
                                            Info = database
                                                .SqlQuery<IndexInfo>(string.Format("PRAGMA index_info([{0}])", indicies.name))
                                                .ToArray()
                                        })
                                .ToDictionary(key => key.Index, value => value.Info)
                        })
                .ToDictionary(key => key.Table, value => value.IndexInfo);
            
            TablesForeignKeys = Tables
                .Select(tbl => new {
                            Table = tbl,
                            ForeignKeys = database
                                .SqlQuery<ForeignKeyList>(string.Format("PRAGMA foreign_key_list([{0}])", tbl))
                                .GroupBy(fk => fk.id)
                                .ToDictionary(key => key.Key, value => value.ToArray())
                        })
                .ToDictionary(key => key.Table, value => value.ForeignKeys);
        }
    }
}

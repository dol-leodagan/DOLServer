﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;

using System.Data.Entity;
using System.Data.Common;

using log4net;

namespace DOL.Database.SQLite
{
    /// <summary>
    /// SQLite Database Handler.
    /// </summary>
    public class Handler : DatabaseHandler
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Is this SQLite Database Memory-based ? 
        /// </summary>
        bool IsMemoryDatabase { get; set; }
        
        /// <summary>
        /// Persistent SQLite Connection Object in case of Memory-based Database.
        /// </summary>
        DbConnection _Connection { get; set; }
        
        public override DbConnection Connection
        {
            get
            {
                return _InitializationConnection ??
                    (IsMemoryDatabase
                    ? _Connection
                    : new System.Data.SQLite.SQLiteConnection(ConnectionString));
            }
        }
        
        public override bool ContextOwnsConnection
        {
            get
            {
                return _InitializationConnection == null && !IsMemoryDatabase;
            }
        }
        
        internal HashSet<string> InitializedTables { get; private set; }
        
        DbTransaction _InitializationTransaction;
        DatabaseMetadata _DatabaseMetadata;
        readonly object _DatabaseMetadataLock = new object();
        DbConnection _InitializationConnection;
        public override DbTransaction InitializationTransaction {
            get {
                return _InitializationTransaction;
            }
            set {
                var transaction = value;
                
                // If Initialization is Finished.
                if (transaction == null)
                {
                    // Add Foreign Key Check after the Update
                    var cmd = _InitializationConnection.CreateCommand();
                    cmd.CommandText = "PRAGMA foreign_keys=ON";
                    cmd.ExecuteNonQuery();

                    if (!IsMemoryDatabase)
                    {
                        _InitializationConnection.Close();
                        _InitializationConnection.Dispose();
                    }
                    
                    _InitializationTransaction = null;
                    _InitializationConnection = null;

                    // Clear Metadata
                    lock(_DatabaseMetadataLock)
                        _DatabaseMetadata = null;
                    
                    InitializedTables.Clear();
                    
                    using (var context = new DOLDatabaseContext(this))
                    {
                        // Check foreign key integrity
                        var constraintCheck = context.Database.SqlQuery<ForeignKeyConstraint>("PRAGMA foreign_key_check").ToArray();
                        if (constraintCheck.Any())
                        {
                            if (log.IsErrorEnabled)
                                foreach (var failure in constraintCheck)
                                    log.ErrorFormat("Foreign Key Integrity Check failed for Table {0}, Rowid {1}, Parent {2}, FKid {3}",
                                                    failure.table, failure.rowid, failure.parent, failure.fkid);
                            
                            throw new InvalidOperationException("Error while verifying Database Foreign Keys Integrity");
                        }
                    }
                }
                else if (_InitializationTransaction != null)
                {
                    throw new InvalidOperationException("Initialization Transaction must be set to null before using another value !");
                }
                else
                {
                   _InitializationConnection = transaction.Connection;
                   _InitializationTransaction = transaction;
                   
                     // Remove Foreign Key Check for the Update
                    var cmd = _InitializationConnection.CreateCommand();
                    cmd.CommandText = "PRAGMA foreign_keys=OFF";
                    cmd.ExecuteNonQuery();
                }
            }
        }
        
        
        public override IDatabaseInitializer<T> GetInitializer<T>(DbModelBuilder modelBuilder)
        {
            return new DatabaseInitializer<T>(modelBuilder, this);
        }
        
        internal DatabaseMetadata GetDatabaseMetadata(System.Data.Entity.Database database)
        {
            lock (_DatabaseMetadataLock)
            {
                if (_DatabaseMetadata != null)
                {
                    return _DatabaseMetadata;
                }
                
                _DatabaseMetadata = new DatabaseMetadata(database);
                return _DatabaseMetadata;
            }
        }
        
        /// <summary>
        /// Create a new Instance of SQLite Database <see cref="Handler"/>
        /// </summary>
        /// <param name="ConnectionString">Connection String for this Handler.</param>
        /// <param name="RootPath">Root Path for storing Relative Database Path.</param>
        public Handler(string ConnectionString, string RootPath)
            : base(ConnectionString)
        {
            InitializedTables = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            
            IsMemoryDatabase = true;
            
            // Check if Database is file based, and if the path is relative to RootPath
            var connectionString = new System.Data.SQLite.SQLiteConnectionStringBuilder(this.ConnectionString);
            
            if (!connectionString.DataSource.ToLower().Contains(":memory:"))
            {
                IsMemoryDatabase = false;
                if (!System.IO.Path.IsPathRooted(connectionString.DataSource))
                {
                    connectionString.DataSource = System.IO.Path.Combine(RootPath, connectionString.DataSource);
                }
            }
            
            connectionString.ForeignKeys = true;
            connectionString.BinaryGUID = false;
            
            this.ConnectionString = connectionString.ConnectionString;
            _Connection = new System.Data.SQLite.SQLiteConnection(this.ConnectionString);
            
            if (IsMemoryDatabase)
                _Connection.Open();
        }
    }
}

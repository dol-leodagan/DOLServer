﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

namespace DOL.Database.SQLite
{
    class TableInfo
    {
        public int cid { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public bool notnull { get; set; }
        public string dflt_value { get; set; }
        public bool pk { get; set; }
    }
    
    class IndexList
    {
        public int seq { get; set; }
        public string name { get; set; }
        public bool unique { get; set; }
        public string origin { get; set; }
        public bool partial { get; set; }
    }
    
    class IndexInfo
    {
        public int seqno { get; set; }
        public int cid { get; set; }
        public string name { get; set; }
    }
    
    class ForeignKeyList
    {
        public int id { get; set; }
        public int seq { get; set; }
        public string table { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string on_update { get; set; }
        public string on_delete { get; set; }
        public string match { get; set; }
    }
    
    class IndexDefinition
    {
        public bool IsUnique { get; set; }
        public string[] Columns { get; set; }
    }
    
    class ForeignKeyConstraint
    {
        public string table { get; set; }
        public int rowid { get; set; }
        public string parent { get; set; }
        public int fkid { get; set; }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Reflection;
using System.Linq;

using System.Data.Entity;
     
using log4net;

namespace DOL.Database
{
    /// <summary>
    /// Base Context Class for every connections to the Database.
    /// </summary>
    public abstract class BaseContext<T> : DbContext where T : BaseContext<T>
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Database Handler for this Context.
        /// </summary>
        protected DatabaseHandler DbHandler { get; set; }

        /// <summary>
        /// Create a new instance of <see cref="BaseContext{T}"/>
        /// </summary>
        /// <param name="DbHandler">Database Handler for this Context.</param>
        protected BaseContext(DatabaseHandler DbHandler)
            : base(DbHandler.Connection, DbHandler.ContextOwnsConnection)
        {
            this.DbHandler = DbHandler;
            this.Database.Log = s => { if (log.IsDebugEnabled) log.Debug(s); };
        }
                
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
                modelBuilder.Configurations.AddFromAssembly(asm);
            
            System.Data.Entity.Database.SetInitializer<T>(DbHandler.GetInitializer<T>(modelBuilder));
        }
        
        public override int SaveChanges()
        {
            foreach (var dbEntityEntry in ChangeTracker.Entries<DataObject>().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified))
            {
                dbEntityEntry.Property(p => p.LastModified).CurrentValue = DateTime.Now;
            }
            
            return base.SaveChanges();
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using System.Data.Entity;

using DOL.Database.DataObjects;

namespace DOL.Database
{
    /// <summary>
    /// DOL Core Database Context.
    /// </summary>
    public class DOLDatabaseContext : BaseContext<DOLDatabaseContext>
    {
        /// <summary>
        /// Account Set.
        /// </summary>
        public DbSet<Account> Accounts { get; set; }
        
        /// <summary>
        /// Characters Set.
        /// </summary>
        public DbSet<Character> Characters { get; set; }
        
        /// <summary>
        /// Server Property Set.
        /// </summary>
        public DbSet<ServerProperty> ServerProperties { get; set; }
        
        /// <summary>
        /// Create a new intance of Core <see cref="DOLDatabaseContext"/>
        /// </summary>
        /// <param name="DbHandler"></param>
        public DOLDatabaseContext(DatabaseHandler DbHandler)
            : base (DbHandler)
        {
        }
    }
}

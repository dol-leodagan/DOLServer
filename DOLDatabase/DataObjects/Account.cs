﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;

using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOL.Database.DataObjects
{
    /// <summary>
    /// Account Privileges Level.
    /// </summary>
    public enum eAccountPrivileges : byte
    {
        Player = 1,
        Privileged = 2,
        GameMaster = 3,
        Admin = 4,
    }
    
    /// <summary>
    /// Accounts Table.
    /// </summary>
    public class Account : DataObject
    {
        public int AccountId { get; set; }
        
        public string Name { get; set; }
        
        public string Password { get; set; }
        
        public eAccountPrivileges Privileges { get; set; }
        
        public virtual ICollection<Character> Characters { get; set; }
        
        public Account()
        {
            Name = string.Empty;
            Password = string.Empty;
            Privileges = eAccountPrivileges.Player;
        }
    }
    
    /// <summary>
    /// Accounts Table Configuration.
    /// </summary>
    public class AccountConfiguration : EntityTypeConfiguration<Account>
    {
        public AccountConfiguration()
        {
            Map(m => m.MapInheritedProperties())
                .ToTable("core_Account");
            
            HasKey(p => p.AccountId);
            
            Property(p => p.AccountId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            
            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(128)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName
                                     , new IndexAnnotation(
                                         new IndexAttribute { IsUnique = true }));
            Property(p => p.Password)
                .IsRequired()
                .HasMaxLength(255)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                                     new IndexAnnotation(new IndexAttribute()));
            Property(p => p.Privileges)
                .IsRequired()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                                     new IndexAnnotation(new IndexAttribute()));
            
            Property(p => p.LastModified)
                .IsConcurrencyToken();
        }
    }
}

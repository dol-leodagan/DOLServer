﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;

using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOL.Database.DataObjects
{
    /// <summary>
    /// Character Table.
    /// </summary>
    public class Character : DataObject
    {
        public Guid CharacterId { get; set; }
        
        public string Name { get; set; }
        
        public string Realm { get; set; }
        
        public string Class { get; set; }
        
        public string Race { get; set; }
        
        public bool Gender { get; set; }
        
        public short Slot { get; set; }
        
        public Account Account { get; set; }
        
        public int Account_AccountId { get; set; }
        
        public ICollection<Inventory> Inventory { get; set; }
        
        public Character()
        {
            CharacterId = Guid.NewGuid();
            Name = string.Empty;
            Realm = string.Empty;
            Class = string.Empty;
            Race = string.Empty;
        }
    }
    
    public class CharacterConfiguration : EntityTypeConfiguration<Character>
    {
        public CharacterConfiguration()
        {
            Map(m => m.MapInheritedProperties())
                .ToTable("core_Character");
            
            HasKey(p => p.CharacterId);
            
            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName
                                     , new IndexAnnotation(
                                         new IndexAttribute { IsUnique = true }));
            Property(p => p.Realm)
                .IsRequired()
                .HasMaxLength(255)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName
                                     , new IndexAnnotation(
                                         new IndexAttribute("UQ_core_Character_Realm_Slot_AccId", 1) { IsUnique = true }));
            Property(p => p.Slot)
                .IsRequired()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName
                                     , new IndexAnnotation(
                                         new IndexAttribute("UQ_core_Character_Realm_Slot_AccId", 2) { IsUnique = true }));
            Property(p => p.Account_AccountId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName
                                     , new IndexAnnotation(
                                         new IndexAttribute("UQ_core_Character_Realm_Slot_AccId", 3) { IsUnique = true }));
            HasRequired(p => p.Account)
                .WithMany(a => a.Characters)
                .HasForeignKey(p => p.Account_AccountId)
                .WillCascadeOnDelete(true);
            
            Property(p => p.LastModified)
                .IsConcurrencyToken();
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOL.Database.DataObjects
{
    /// <summary>
    /// Class Races Constraint Table.
    /// </summary>
    public class ClassConstraint : DataObject
    {
        public Guid ClassConstraintId { get; set; }
        
        public Class Class { get; set; }
        
        public string ClassName { get; set; }
        
        public Race Race { get; set; }
        
        public string RaceName { get; set; }
        
        public bool? AllowedGender { get; set; }
        
        public ClassConstraint()
        {
            ClassConstraintId = new Guid();
        }
    }
    
    public class ClassConstraintConfiguration : EntityTypeConfiguration<ClassConstraint>
    {
        public ClassConstraintConfiguration()
        {
            Map(m => m.MapInheritedProperties())
                .ToTable("conf_ClassConstraint");
            
            HasKey(p => p.ClassConstraintId);
            
            HasRequired(p => p.Class)
                .WithMany()
                .HasForeignKey(p => p.ClassName)
                .WillCascadeOnDelete();
            
            HasOptional(p => p .Race)
                .WithMany()
                .HasForeignKey(p => p.RaceName);
            
            Property(p => p.AllowedGender)
                .IsOptional();
            
            Property(p => p.ClassName)
                .IsRequired()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName
                                     , new IndexAnnotation(
                                         new IndexAttribute("UQ_conf_ClassConstraint_Class_Race", 1) { IsUnique = true }));
            Property(p => p.RaceName)
                .IsOptional()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName
                                     , new IndexAnnotation(
                                         new IndexAttribute("UQ_conf_ClassConstraint_Class_Race", 2) { IsUnique = true }));
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOL.Database.DataObjects
{
    /// <summary>
    /// Inventory Table.
    /// </summary>
    public class Inventory : DataObject
    {
        public long InventoryId { get; set; }
        
        public short Slot { get; set; }
        
        public Character Owner { get; set; }
        
        public Guid Owner_CharacterId { get; set; }
        
        public Inventory()
        {
        }
    }
    
    public class InventoryConfiguration : EntityTypeConfiguration<Inventory>
    {
        public InventoryConfiguration()
        {
            Map(m => m.MapInheritedProperties())
                .ToTable("core_Inventory");
            
            HasKey(p => p.InventoryId);
            
            Property(p => p.InventoryId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            
            Property(p => p.Owner_CharacterId)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName
                                     , new IndexAnnotation( new [] {
                                                               new IndexAttribute("UQ_core_Inventory_Character_Slot", 1) { IsUnique = true },
                                                               new IndexAttribute(),
                                                           }));
            Property(p => p.Slot)
                .IsRequired()
                .HasColumnAnnotation(IndexAnnotation.AnnotationName
                                     , new IndexAnnotation(
                                         new IndexAttribute("UQ_core_Inventory_Character_Slot", 2) { IsUnique = true }));
            HasRequired(p => p.Owner)
                .WithMany(c => c.Inventory)
                .HasForeignKey(p => p.Owner_CharacterId)
                .WillCascadeOnDelete(true);
            
            Property(p => p.LastModified)
                .IsConcurrencyToken();
        }
    }
}

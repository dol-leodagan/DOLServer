﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOL.Database.DataObjects
{
    /// <summary>
    /// Player Race Table.
    /// </summary>
    public class Race : DataObject
    {
        public string Name { get; set; }
        
        public string FemaleName { get; set; }
        
        public Realm Realm { get; set; }
        
        public bool? AllowedGender { get; set; }
        
        public short Strength { get; set; }
        
        public short Constitution { get; set; }
        
        public short Dexterity { get; set; }
        
        public short Quickness { get; set; }
        
        public short Intelligence { get; set; }
        
        public short Piety { get; set; }
        
        public short Empathy { get; set; }
        
        public short Charisma { get; set; }
        
        public Race()
        {
        }
    }
    
    public class RaceConfiguration : EntityTypeConfiguration<Race>
    {
        public RaceConfiguration()
        {
            Map(m => m.MapInheritedProperties())
                .ToTable("conf_Race");
            
            HasKey(p => p.Name);
            
            HasRequired(p => p.Realm)
                .WithMany()
                .WillCascadeOnDelete();
            
            Property(p => p.FemaleName)
                .HasMaxLength(255)
                .IsRequired();
            
            Property(p => p.AllowedGender)
                .IsOptional();
            
            Property(p => p.Strength)
                .IsRequired();
            Property(p => p.Constitution)
                .IsRequired();
            Property(p => p.Dexterity)
                .IsRequired();
            Property(p => p.Quickness)
                .IsRequired();
            Property(p => p.Intelligence)
                .IsRequired();
            Property(p => p.Piety)
                .IsRequired();
            Property(p => p.Empathy)
                .IsRequired();
            Property(p => p.Charisma)
                .IsRequired();
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using System.Data.Entity;
using System.Data.Common;

namespace DOL.Database
{
    /// <summary>
    /// Base Class for DatabaseHandler specific to each driver.
    /// </summary>
    public abstract class DatabaseHandler
    {
        /// <summary>
        /// Connection String.
        /// </summary>
        protected string ConnectionString { get; set; }
        
        /// <summary>
        /// Retrieve Database Connection Object.
        /// </summary>
        public abstract DbConnection Connection { get; }
        
        /// <summary>
        /// Connection exclusive to each Context.
        /// </summary>
        public abstract bool ContextOwnsConnection { get; }
        
        /// <summary>
        /// Retrieve the Handler Database Initializer for given Context.
        /// </summary>
        /// <param name="modelBuilder">DbModelBuilder Object.</param>
        /// <returns>Database Handler Context Initializer.</returns>
        public abstract IDatabaseInitializer<T> GetInitializer<T>(DbModelBuilder modelBuilder) where T : BaseContext<T>;
        
        /// <summary>
        /// Transaction to use for Initialization Process.
        /// </summary>
        public abstract DbTransaction InitializationTransaction { get; set; }
        
        /// <summary>
        /// Create new Instance of <see cref="DatabaseHandler"/>
        /// </summary>
        /// <param name="ConnectionString">Connection String.</param>
        protected DatabaseHandler(string ConnectionString)
        {
            this.ConnectionString = ConnectionString;
        }
    }
}

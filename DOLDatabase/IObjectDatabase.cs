﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

namespace DOL.Database
{
    /// <summary>
    /// Database Driver Type.
    /// </summary>
    public enum eDatabaseType {
        MySQL,
        SQLite,
    }

    /// <summary>
    /// Object Database Interface
    /// </summary>
    public interface IObjectDatabase
    {
        /// <summary>
        /// Initialize Database Tables.
        /// </summary>
        void Init();
        
        /// <summary>
        /// Get Database Context from this Object Database.
        /// </summary>
        /// <returns>Initialized Database Context.</returns>
        T GetContext<T>() where T : BaseContext<T>;
    }
}

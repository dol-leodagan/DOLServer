﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Reflection;

using System.Data.Entity;
using System.Data.Common;
using log4net;

namespace DOL.Database
{
    /// <summary>
    /// Database Connection For DOL.
    /// </summary>
    public class ObjectDatabase : IObjectDatabase
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Object Database Handler.
        /// </summary>
        DatabaseHandler DbHandler { get; set; }
        
        /// <summary>
        /// Create a new instance of <see cref="ObjectDatabase"/>
        /// </summary>
        /// <param name="DatabaseType">Database Driver Type.</param>
        /// <param name="DatabaseConnectionString">Database Connection String.</param>
        /// <param name="RealRootPath">Absolute Path to Root Directory.</param>
        public ObjectDatabase(eDatabaseType DatabaseType, string DatabaseConnectionString, string RealRootPath)
        {
            switch (DatabaseType)
            {
                case eDatabaseType.MySQL:
                    var MySQLImplementation = Assembly.Load("DOLDatabase.MySQL");
                    DbHandler = (DatabaseHandler)MySQLImplementation.CreateInstance("DOL.Database.MySQL.Handler", false, BindingFlags.CreateInstance, null, new object[] { DatabaseConnectionString }, null, null);
                    break;
                    
                case eDatabaseType.SQLite:
                default:
                    var SQLiteImplementation = Assembly.Load("DOLDatabase.SQLite");
                    DbHandler = (DatabaseHandler)SQLiteImplementation.CreateInstance("DOL.Database.SQLite.Handler", false, BindingFlags.CreateInstance, null, new object[] { DatabaseConnectionString, RealRootPath }, null, null);
                    break;
            }
            
            if (DbHandler == null)
                throw new InvalidOperationException("Database Handler could not be Instantiated, check your configuration.");
        }
        
        public void Init()
        {
            var contexts = AppDomain.CurrentDomain.GetAssemblies()
                .Select(asm => asm.GetTypes()
                        .Where(type => type.IsClass && !type.IsAbstract && typeof(DbContext).IsAssignableFrom(type))
                        .Where(type => {
                                   try
                                   {
                                       return typeof(BaseContext<>).MakeGenericType(type).IsAssignableFrom(type);
                                   }
                                   catch
                                   {
                                       return false;
                                   }
                               }
                              ))
                .SelectMany(types => types);
            
            var connection = DbHandler.Connection;
            
            if (connection.State != System.Data.ConnectionState.Open)
                connection.Open();
            
            using (var transaction = connection.BeginTransaction())
            {
                DbHandler.InitializationTransaction = transaction;
                try
                {
                    foreach (var context in contexts)
                    {
                        using (var ctx = (DbContext)Activator.CreateInstance(context, DbHandler))
                        {
                            if (log.IsInfoEnabled)
                                log.InfoFormat("Initializing Database Context {0} ({1})", ctx.GetType().FullName, ctx.GetType().Assembly.ManifestModule);
                            ctx.Database.Initialize(false);
                        }
                    }
                    
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    if (log.IsErrorEnabled)
                        log.Error("Error While Initializating Database", e);
                    
                    transaction.Rollback();
                    throw e;
                }
                finally
                {
                    DbHandler.InitializationTransaction = null;
                }
            }
        }
        
        public T GetContext<T>() where T : BaseContext<T>
        {
            return (T)Activator.CreateInstance(typeof(T), DbHandler);
        }
    }
}

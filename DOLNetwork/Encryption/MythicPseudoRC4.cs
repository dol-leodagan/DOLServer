﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

namespace DOL.Network.Encryption
{
    /// <summary>
    /// Simple implementation of Mythic RC4
    /// @author taferth, mlinder
    /// </summary>
    public static class MythicPseudoRC4
    {
        public static readonly int SBOX_LENGTH = 256;
        /// <summary>
        /// Generates the SBox from the common key.
        /// </summary>
        public static byte[] GenerateSBox(byte[] commonKey)
        {
            // Generate the S-Box from the common key (Code borrowed from DAoC Logger)
            var sbox = new byte[SBOX_LENGTH];
            int x, y;
            
            for (x = 0; x < sbox.Length; x++)
                sbox[x] = (byte) x;

            for (x = y = 0; x < sbox.Length; x++)
            {
                unchecked 
                {
                    y = (y + sbox[x] + commonKey[x % commonKey.Length]) & 255;
                    var tmp = sbox[x];
                    sbox[x] = sbox[y];
                    sbox[y] = tmp;
                }
            }

            return sbox;
        }
        
        /// <summary>
        /// Encode Byte Buffer with Mythic Pseudo RC4 using given sbox.
        /// </summary>
        /// <param name="buf">Byte Buffer to be Encoded.</param>
        /// <param name="offset">Start Index for Encoding.</param>
        /// <param name="len">Length to Encode.</param>
        /// <param name="sbox">Key SBox.</param>
        public static void EncodeMythicRC4Packet(byte[] buf, int offset, int len, byte[] sbox)
        {
            var s = new byte[SBOX_LENGTH];
            Buffer.BlockCopy(sbox, 0, s, 0, s.Length);

            byte x = 0;
            byte y = 0;
            byte tmp;
            // it is not standard RC4 practice to break a block in half, but packets
            // from mythic's client have a sequence number at the beginning which
            // would be easily guessable
            var midpoint = (len / 2) + offset;
            var end = len + offset;
            var pos = 0;

            for (pos = midpoint; pos < end; pos++)
            {
                unchecked
                {
                    x++;
                    y += s[x];
                    tmp = s[x];
                    s[x] = s[y];
                    s[y] = tmp;
                    tmp = (byte)(s[x] + s[y]);
                    y += buf[pos]; // this is not standard RC4 here
                    buf[pos] ^= s[tmp];
                }
            }
            
            for (pos = offset; pos < midpoint; pos++)
            {
                unchecked
                {
                    x++;
                    y += s[x];
                    tmp = s[x];
                    s[x] = s[y];
                    s[y] = tmp;
                    tmp = (byte)(s[x] + s[y]);
                    y += buf[pos]; // this is not standard RC4 here
                    buf[pos] ^= s[tmp];
                }
            }
        }
        
        /// <summary>
        /// Decode Byte Buffer with Mythic Pseudo RC4 using given sbox.
        /// </summary>
        /// <param name="buf">Byte Buffer to be Decoded.</param>
        /// <param name="offset">Start Index for Decoding.</param>
        /// <param name="len">Length to Decode.</param>
        /// <param name="sbox">Key SBox.</param>
        public static void DecodeMythicRC4Packet(byte[] buf, int offset, int len, byte[] sbox)
        {
            var s = new byte[SBOX_LENGTH];
            Array.Copy(sbox, s, s.Length);

            byte x = 0;
            byte y = 0;
            byte tmp;

            // it is not standard RC4 practice to break a block in half, but packets
            // from mythic's client have a sequence number at the beginning which
            // would be easily guessable
            var midpoint = (len / 2) + offset;
            var end = len + offset;
            var pos = 0;

            for (pos = midpoint; pos < end; pos++)
            {
                unchecked
                {
                    x++;
                    y += s[x];
                    tmp = s[x];
                    s[x] = s[y];
                    s[y] = tmp;
                    tmp = (byte)(s[x] + s[y]);
                    buf[pos] ^= s[tmp];
                    y += buf[pos]; // this is not standard RC4 here
                }
            }
            
            for (pos = offset; pos < midpoint; pos++)
            {
                unchecked
                {
                    x++;
                    y += s[x];
                    tmp = s[x];
                    s[x] = s[y];
                    s[y] = tmp;
                    tmp = (byte)(s[x] + s[y]);
                    buf[pos] ^= s[tmp];
                    y += buf[pos]; // this is not standard RC4 here
                }
            }
        }
    }
}
﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Net;

using SuperSocket.SocketBase.Protocol;

using log4net;

using DOL.Utils.Text;
using DOL.Network.Packets;
using DOL.Network.OfficialPackets;

namespace DOL.Network
{
    /// <summary>
    /// GameClient Handles Communication and Event with Official Client.
    /// </summary>
    public class GameClient
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public enum PacketProtocol
        {
            TCP,
            UDP,
        }
        
        OfficialClientSession TcpSession { get; set; }
        OfficialClientSession UdpSession { get; set; }
        
        readonly Type[] ClientPacketTypes;
        readonly Type[] ServerPacketTypes;
        
        public Type[] ClientPacketRouting { get { return ClientPacketTypes.ToArray(); } }
        public Type[] ServerPacketRouting { get { return ServerPacketTypes.ToArray(); } }
        
        public IPEndPoint UDPEndPoint { get { return UdpSession != null ? UdpSession.RemoteEndPoint : null; } }
        
        public int Version { get; private set; }
        public string Revision { get; private set; }
        
        byte[] _RC4KeySBox;
        public byte[] RC4KeySBox
        {
            get
            {
                return _RC4KeySBox;
            }
            set
            {
                try
                {
                    _RC4KeySBox = Network.Encryption.MythicPseudoRC4.GenerateSBox(value);
                }
                catch
                {
                    _RC4KeySBox = null;
                }
            }
        }
        
        public event DOLEventHandler<GameClient, int, IClientPacket, PacketProtocol> ReceivedDecodedPacket;
        
        void OnReceivedDecodedPacket(int packetCode, IClientPacket packet, PacketProtocol protocol)
        {
            var handler = ReceivedDecodedPacket;
            
            if (handler != null)
                handler(this, packetCode, packet, protocol);
        }
        
        public event DOLEventHandler<GameClient, OfficialClientPacketIn> ReceivedRawPacket;
        
        void OnReceivedRawPacket(OfficialClientPacketIn packet)
        {
            var handler = ReceivedRawPacket;
            
            if (handler != null)
                handler(this, packet);
        }
        
        public event DOLEventHandler<GameClient, OfficialClientTCPPacketOut> SentRawPacketTCP;
        
        void OnSentRawPacketTCP(OfficialClientTCPPacketOut packet)
        {
            var handler = SentRawPacketTCP;
            
            if (handler != null)
                handler(this, packet);
        }
        
        public event DOLEventHandler<GameClient, OfficialClientUDPPacketOut> SentRawPacketUDP;
        
        void OnSentRawPacketUDP(OfficialClientUDPPacketOut packet)
        {
            var handler = SentRawPacketUDP;
            
            if (handler != null)
                handler(this, packet);
        }

        public bool SendTCP<T>(int packetCode, Action<T> packetHandler) where T : IServerPacket
        {
            try
            {
                var packetType = ServerPacketTypes[packetCode];
                
                if (packetType == null)
                {
                    if (log.IsWarnEnabled)
                        log.WarnFormat("Could not find packet Type <{1}(0x{0:X2})> when trying to Send TCP Packet to Client...", packetCode, typeof(T));
                    
                    return false;
                }
                
                var instanced = (IServerPacket)Activator.CreateInstance(packetType);
                
                using (var packet = new OfficialClientTCPPacketOut(Convert.ToByte(packetCode)))
                {
                    packetHandler((T)instanced);
                    instanced.ExportTo(packet);
                    var buffer = packet.GetBuffer();
                    
                    if (log.IsDebugEnabled)
                        log.DebugFormat("Sending TCP Packet <{0}(0x{2:X2})> BodySize {1}{3}{4}",
                                        packetType.Name, packet.BodySize, packet.PacketCode, Environment.NewLine, buffer.ToHexDump());
                    
                    if (RC4KeySBox != null)
                        Network.Encryption.MythicPseudoRC4.EncodeMythicRC4Packet(buffer, 2, buffer.Length - 2, RC4KeySBox);
                    
                    OnSentRawPacketTCP(packet);
                    return TcpSession.TrySend(buffer, 0, buffer.Length);
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.ErrorFormat("Error while handling Server TCP Packet :{0}PacketCode : 0x{1:X2} - {2}{3}{4}",
                                    Environment.NewLine, packetCode, typeof(T), Environment.NewLine, ex);
                
                return false;
            }
        }
        
        internal void OnReceiveTCP(BinaryRequestInfo request)
        {
            try
            {
                // Decode if we are Encrypted.
                if (RC4KeySBox != null)
                    Network.Encryption.MythicPseudoRC4.DecodeMythicRC4Packet(request.Body, 2, request.Body.Length - 2, RC4KeySBox);
                
                using (var packet = new OfficialClientPacketIn(request.Body))
                {
                    OnReceivedRawPacket(packet);
                    if (!packet.IsCheckSumValidated)
                    {
                        if (log.IsErrorEnabled)
                            log.ErrorFormat("Wrong CheckSum for TCP Received Packet from Client ({0}){1}{2}",
                                            this, Environment.NewLine, request.Body.ToHexDump());
                        return;
                    }

                    var packetType = ClientPacketTypes[packet.PacketCode];
                    
                    if (log.IsDebugEnabled)
                        log.DebugFormat("Receiving TCP Packet <{0}(0x{1:X2})> BodySize {2}, Sequence {3}, SessionId {4}, Parameter 0x{5:X2}, CheckSum 0x{6:X2} (Validated:{7}){8}{9}",
                                        packetType == null ? "null" : packetType.Name, packet.PacketCode, packet.BodySize, packet.Sequence,
                                        packet.SessionId, packet.Parameter, packet.CheckSum, packet.IsCheckSumValidated,
                                        Environment.NewLine, request.Body.ToHexDump());
                    
                    if (packetType == null)
                    {
                        if (log.IsWarnEnabled)
                            log.WarnFormat("Received Unknown TCP Packet Code (0x{0:X2}) from Client ({1})", packet.PacketCode, this);
                        
                        return;
                    }
                    
                    var instanced = (IClientPacket)Activator.CreateInstance(packetType);
                    instanced.LoadFrom(packet);

                    if (log.IsWarnEnabled && !packet.IsEndOfStream)
                        log.WarnFormat("Remaining UnHandled Data at the End of Received TCP Packet <{0}(0x{1:X2})> from Client ({2}) (Read {3}/{4})",
                                       instanced.GetType().Name, packet.PacketCode, this, packet.Position, packet.BodySize);
                    
                    OnReceivedDecodedPacket(packet.PacketCode, instanced, PacketProtocol.TCP);
                }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.ErrorFormat("Error while handling Client TCP Packet :{0}{1}{2}{3}", Environment.NewLine, request.Body.ToHexDump(), Environment.NewLine, ex);
            }
        }
        
        internal void OnReceiveUDP(BinaryRequestInfo request)
        {
            try
            {
                 // Decode if we are Encrypted.
                if (RC4KeySBox != null)
                    Network.Encryption.MythicPseudoRC4.DecodeMythicRC4Packet(request.Body, 2, request.Body.Length - 2, RC4KeySBox);
                
               using (var packet = new OfficialClientPacketIn(request.Body))
               {
                   OnReceivedRawPacket(packet);
                   
                   if (!packet.IsCheckSumValidated)
                   {
                       if (log.IsErrorEnabled)
                           log.ErrorFormat("Wrong CheckSum for UDP Received Packet from Client ({0}){1}{2}",
                                           this, Environment.NewLine, request.Body.ToHexDump());
                       return;
                   }
                   
                   var packetType = ClientPacketTypes[packet.PacketCode];
                   
                   if (log.IsDebugEnabled)
                       log.DebugFormat("Receiving UDP Packet <{0}(0x{1:X2})> BodySize {2}, Sequence {3}, SessionId {4}, Parameter 0x{5:X2}, CheckSum 0x{6:X2} (Validated:{7}){8}{9}",
                                       packetType == null ? "null" : packetType.Name, packet.PacketCode, packet.BodySize, packet.Sequence,
                                       packet.SessionId, packet.Parameter, packet.CheckSum, packet.IsCheckSumValidated,
                                       Environment.NewLine, request.Body.ToHexDump());

                   if (packetType == null)
                   {
                       if (log.IsWarnEnabled)
                           log.WarnFormat("Received Unknown UDP Packet Code (0x{0:X2}) from Client ({1})", packet.PacketCode, this);
                       
                       return;
                   }
                   
                   var instanced = (IClientPacket)Activator.CreateInstance(packetType);
                   instanced.LoadFrom(packet);
                   
                   if (log.IsWarnEnabled && !packet.IsEndOfStream)
                       log.WarnFormat("Remaining UnHandled Data at the End of Received UDP Packet <{0}(0x{1:X2})> from Client ({2}) (Read {3}/{4})",
                                      instanced.GetType().Name, packet.PacketCode, this, packet.Position, packet.BodySize);
                   
                   OnReceivedDecodedPacket(packet.PacketCode, instanced, PacketProtocol.UDP);
               }
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.ErrorFormat("Error while handling Client UDP Packet :{0}{1}{2}{3}", Environment.NewLine, request.Body.ToHexDump(), Environment.NewLine, ex);
            }
        }
        
        internal void EnableUDP(OfficialClientSession udpSession)
        {
            if (UdpSession != null)
                UdpSession.UnsetClient();
            
            UdpSession = udpSession;
        }
        
        internal void Quit()
        {
            if (TcpSession != null)
                TcpSession.UnsetClient();
            
            TcpSession = null;
            
            if (UdpSession != null)
                UdpSession.UnsetClient();
            
            UdpSession = null;
        }
        
        public GameClient(OfficialClientSession TcpSession, BinaryRequestInfo FirstRequest, PacketProcessor PacketProcessor)
        {
            using (var firstPacket = new OfficialClientPacketIn(FirstRequest.Body))
            {
                if (log.IsDebugEnabled)
                    log.DebugFormat("Creating new Client from Session {0}/{1}, using First Request :{3}{2}",
                                    TcpSession.SessionID, TcpSession.RemoteEndPoint, firstPacket.GetBuffer().ToHexDump(), Environment.NewLine);
                
                // Check that this is a First Packet
                if (firstPacket.PacketCode != 0xF4)
                    throw new InvalidOperationException("Unsupported First Packet Client PacketCode");
                
                var clientTypeOrRc4Flag = firstPacket.ReadByte();
                
                // pre - 1115c, send 0 Flag for first welcome packet unencrypted handshake
                if (clientTypeOrRc4Flag == 0)
                {
                    firstPacket.Skip(1); // skip client type.
                    var major = (byte)firstPacket.ReadByte();
                    var minor = (byte)firstPacket.ReadByte();
                    var build = (byte)firstPacket.ReadByte();
                    Version = minor < 10 ? major * 100 + minor * 10 + build : major * 1000 + minor * 10 + build;
                    Revision = string.Empty;
                }
                else
                {
                    // if not 0 we have a post-1.115c client type
                    var major = (byte)firstPacket.ReadByte();
                    var minor = (byte)firstPacket.ReadByte();
                    var build = (byte)firstPacket.ReadByte();
                    Version = major * 1000 + minor * 100 + build;
                    
                    // Revision is Build letter "a, b, c..." appended with Build Number, should be alphabetically sortable...
                    Revision = string.Format("{0}{1}", (char)firstPacket.ReadByte(), firstPacket.ReadUShortLowEndian());
                }
            }
            
            this.TcpSession = TcpSession;
            ClientPacketTypes = PacketProcessor.GetClientProcessorFor(Version, Revision);
            ServerPacketTypes = PacketProcessor.GetServerProcessorFor(Version, Revision);
        }
    }
}

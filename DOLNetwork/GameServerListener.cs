﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Concurrent;
using System.Net;

using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Config;
using SuperSocket.SocketBase.Protocol;

namespace DOL.Network
{
    /// <summary>
    /// GameServer Listener Implement Official Client TCP and UDP Communications.
    /// </summary>
    public class GameServerListener
    {
        const int MAX_CLIENT_REQUEST_LENGTH = 2048;
        
        OfficialClientServer TCPServer { get; set; }
        OfficialClientServer UDPServer { get; set; }
        
        ConcurrentDictionary<GameClient, byte> GameClients { get; set; }
        ConcurrentDictionary<IPEndPoint, GameClient> UdpGameClients { get; set; }
        
        PacketProcessor PacketProcessor { get; set; }
        
        public event DOLEventHandler<GameServerListener, GameClient> GameClientCreated;
        
        void OnGameClientCreated(GameClient Client)
        {
            var handler = GameClientCreated;
            
            if (handler != null)
                handler(this, Client);
        }
        
        public event DOLEventHandler<GameServerListener, GameClient> GameClientRemoved;
        
        void OnGameClientRemoved(GameClient Client)
        {
            var handler = GameClientRemoved;
            
            if (handler != null)
                handler(this, Client);
        }
        
        /// <summary>
        /// Create a new Instance of <see cref="GameServerListener"/>
        /// </summary>
        /// <param name="TCPAddr">TCP Addr to bind.</param>
        /// <param name="TCPPort">TCP Port to bind.</param>
        /// <param name="UDPAddr">UDP Address to bind.</param>
        /// <param name="UDPPort">UDP Port to bind.</param>
        /// <param name="MaxConnection">Maximum Client Connection Allowed.</param>
        public GameServerListener(string TCPAddr, int TCPPort, string UDPAddr, int UDPPort, int MaxConnection)
        {
            GameClients = new ConcurrentDictionary<GameClient, byte>();
            UdpGameClients = new ConcurrentDictionary<IPEndPoint, GameClient>();
            PacketProcessor = new PacketProcessor();
            
            TCPServer = new OfficialClientServer();
            TCPServer.Setup(new ServerConfig() {
                                Mode = SocketMode.Tcp,
                                Ip = TCPAddr,
                                Port = TCPPort,
                                MaxConnectionNumber = MaxConnection,
                                MaxRequestLength = MAX_CLIENT_REQUEST_LENGTH,
                                ReceiveBufferSize = 8192,
                                SendBufferSize = 8192,
                                ClearIdleSession = true,
                            });
            
            UDPServer = new OfficialClientServer();
            UDPServer.Setup(new ServerConfig() {
                                Mode = SocketMode.Udp,
                                Ip = UDPAddr,
                                Port = UDPPort,
                                MaxConnectionNumber = MaxConnection,
                                MaxRequestLength = MAX_CLIENT_REQUEST_LENGTH,
                                ReceiveBufferSize = 8192,
                                SendBufferSize = 8192,
                                ClearIdleSession = true,
                            });
            
            TCPServer.NewRequestReceived += TCPReceive;
            UDPServer.NewRequestReceived += UDPReceive;
            TCPServer.SessionClosed += TCPClose;
            UDPServer.SessionClosed += UDPClose;
        }
        
        void TCPReceive(OfficialClientSession client, BinaryRequestInfo request)
        {
            if (client.Client == null)
            {
                // Create GameClient for this Session.
                var gameClient = new GameClient(client, request, PacketProcessor);
                client.SetClient(gameClient);
                
                GameClients.TryAdd(client.Client, 0);
                
                // Client Creation Event
                OnGameClientCreated(client.Client);
            }
            
            client.Client.OnReceiveTCP(request);
        }
        
        void UDPReceive(OfficialClientSession client, BinaryRequestInfo request)
        {
            if (client.Client == null)
            {
                // Attach GameClient for this Session.
                GameClient gameClient;
                UdpGameClients.TryGetValue(client.RemoteEndPoint, out gameClient);
                client.SetClient(gameClient);
                gameClient.EnableUDP(client);
            }
            
            client.Client.OnReceiveUDP(request);
        }
        
        void TCPClose(OfficialClientSession client, CloseReason reason)
        {
            if (client.Client != null)
            {
                // Cleanup GameClients References.
                byte dummy;
                GameClients.TryRemove(client.Client, out dummy);
                
                GameClient dummyClient;
                var udpEndPoint = client.Client.UDPEndPoint;
                
                if (udpEndPoint != null)
                    UdpGameClients.TryRemove(udpEndPoint, out dummyClient);
                
                // Client Removed Event
                OnGameClientRemoved(client.Client);
                
                client.Client.Quit();
            }
        }
        
        void UDPClose(OfficialClientSession client, CloseReason reason)
        {
            if (client.Client != null)
            {
                // Cleanup GameClient UDP Session
                GameClient dummyClient;
                var udpEndPoint = client.Client.UDPEndPoint;
                
                if (udpEndPoint != null)
                    UdpGameClients.TryRemove(client.Client.UDPEndPoint, out dummyClient);
                
                client.UnsetClient();
            }
        }
        
        public bool Start()
        {
            return TCPServer.Start() && UDPServer.Start();
        }
        
        public void Stop()
        {
            TCPServer.Stop();
            UDPServer.Stop();
            TCPServer.NewRequestReceived -= TCPReceive;
            UDPServer.NewRequestReceived -= TCPReceive;
        }
    }
}

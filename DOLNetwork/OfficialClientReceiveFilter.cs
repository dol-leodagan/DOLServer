﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;

using SuperSocket.Facility.Protocol;
using SuperSocket.SocketBase.Protocol;

namespace DOL.Network
{
    /// <summary>
    /// Receive Filter Implementation for Official Client.
    /// </summary>
    public class OfficialClientReceiveFilter : FixedHeaderReceiveFilter<BinaryRequestInfo>
    {
        /// <summary>
        /// Create a new Instance of <see cref="OfficialClientReceiveFilter"/>
        /// </summary>
        public OfficialClientReceiveFilter()
            : base(2)
        {
        }
        
        protected override int GetBodyLengthFromHeader(byte[] header, int offset, int length)
        {
            return (header[offset] << 8 | header[offset + 1]) + 10;
        }
        
        protected override BinaryRequestInfo ResolveRequestInfo(ArraySegment<byte> header, byte[] bodyBuffer, int offset, int length)
        {
            return new BinaryRequestInfo(string.Empty, header.Concat(new ArraySegment<byte>(bodyBuffer, offset, length)).ToArray());
        }
    }
}

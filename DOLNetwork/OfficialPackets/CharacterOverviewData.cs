﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace DOL.Network.OfficialPackets
{
    /// <summary>
    /// ICharacterOverviewData Implementation.
    /// </summary>
    public class CharacterOverviewData : ICharacterOverviewData
    {
        public string CharacterName { get; set; }
        
        // Customization
        public byte Customized { get; set; }
        public byte EyeSize { get; set; }
        public byte LipSize { get; set; }
        public byte EyeColor { get; set; }
        public byte HairColor { get; set; }
        public byte FaceType { get; set; }
        public byte HairStyle { get; set; }
        public byte ExtensionBootsGloves { get; set; }
        public byte ExtensionTorsoCloakHood { get; set; }
        public byte CustomizationStep { get; set; }
        public byte MoodType { get; set; }
        public byte Emblem { get; set; }
        
        public eArmorExtension ExtensionBoots
        {
            get
            {
                var ext = (eArmorExtension)((ExtensionBootsGloves & 0xF0) >> 4);
                return Enum.IsDefined(typeof(eArmorExtension), ext) ? ext : eArmorExtension.None;
            }
            set
            {
                ExtensionBootsGloves = (byte)((ExtensionBootsGloves & 0x0F) | ((byte)value << 4));
            }
        }
        
        public eArmorExtension ExtensionGloves
        {
            get
            {
                var ext = (eArmorExtension)(ExtensionBootsGloves & 0x0F);
                return Enum.IsDefined(typeof(eArmorExtension), ext) ? ext : eArmorExtension.None;
            }
            set
            {
                ExtensionBootsGloves = (byte)((ExtensionBootsGloves & 0xF0) | ((byte)value));
            }
        }
        
        public eArmorExtension ExtensionTorso
        {
            get
            {
                var ext = (eArmorExtension)((ExtensionTorsoCloakHood & 0xF0) >> 4);
                return Enum.IsDefined(typeof(eArmorExtension), ext) ? ext : eArmorExtension.None;
            }
            set
            {
                ExtensionBootsGloves = (byte)((ExtensionTorsoCloakHood & 0x0F) | ((byte)value << 4));
            }
        }
        
        public bool CloakHoodUp
        {
            get
            {
                return (ExtensionTorsoCloakHood & 0x1) == 0x1;
            }
            set
            {
                ExtensionTorsoCloakHood = (byte)((ExtensionTorsoCloakHood & 0xF0) | (value ? 0x1 : 0x0));
            }
        }
        
        public byte[] Unknown0 { get; set; }
        
        public string ZoneDescription { get; set; }
        
        public string ClassName { get; set; }
        
        public string RaceName { get; set; }
        
        public byte Level { get; set; }
        
        public byte ClassId { get; set; }
        
        public eCharacterClass Class
        {
            get
            {
                var eclass = (eCharacterClass)ClassId;
                return Enum.IsDefined(typeof(eCharacterClass), eclass) ? eclass : eCharacterClass.Unknown;
            }
            set
            {
                ClassId = (byte)value;
            }
        }
        
        public byte RealmCode { get; set; }
        
        public eRealm Realm
        {
            get
            {
                var realm = (eRealm)RealmCode;
                return Enum.IsDefined(typeof(eRealm), realm) ? realm : eRealm.None;
            }
            set
            {
                RealmCode = (byte)value;
            }
        }
        
        public byte GenderRace { get; set; }
        
        
        public eGender Gender
        {
            get
            {
                return (eGender)(GenderRace & 0x10);
            }
            set
            {
                GenderRace = (byte)(value == eGender.Male
                                    ? GenderRace & ~(byte)eGender.Female
                                    : GenderRace | (byte)eGender.Female);
            }
        }
        
        public eRace Race
        {
            get
            {
                var race = (eRace)(GenderRace & 0x4F);
                return Enum.IsDefined(typeof(eRace), race) ? race : eRace.Unknown;
            }
            set
            {
                GenderRace = (byte)((GenderRace & ~0x4F) | (byte)value);
            }
        }
        
        public bool ShroudedIslesStartLocation
        {
            get
            {
                return (GenderRace & 0x80) == 0x80;
            }
            set
            {
                GenderRace = (byte)(value ? GenderRace | 0x80 : GenderRace & ~0x80);
            }
        }
        
        
        public ushort Model { get; set; }
        
        public eCharacterSize Size
        {
            get
            {
                var size = (eCharacterSize)(Model & (3 << 11));
                return Enum.IsDefined(typeof(eCharacterSize), size) ? size : eCharacterSize.None;
            }
            set
            {
                Model = (ushort)((Model & ~(3 << 11)) | (ushort)value);
            }
        }
        
        public byte RegionId { get; set; }
        
        public byte RegionId2 { get; set; }
        
        public uint DatabaseId { get; set; }
        
        
        public byte Strength { get; set; }
        
        public byte Dexterity { get; set; }
        
        public byte Constitution { get; set; }
        
        public byte Quickness { get; set; }
        
        public byte Intelligence { get; set; }
        
        public byte Piety { get; set; }
        
        public byte Empathy { get; set; }
        
        public byte Charisma { get; set; }
        
        
        public IList<ushort> ArmorModelBySlot { get; set; }
        
        public IList<ushort> ArmorColorBySlot { get; set; }
        
        public IList<ushort> WeaponModelBySlot { get; set; }
        
        
        public byte ActiveRightSlot { get; set; }
        
        public byte ActiveLeftSlot { get; set; }
        
        public byte ShroudedIslesZone { get; set; }
        
        public byte RealConstitution { get; set; }
        
        public uint Unknown1 { get; set; }
        
        public eCreateOperation Operation { get; set; }

        public CharacterOverviewData()
        {
            CustomizationStep = 1;
            Unknown0 = new byte[12];
            ArmorModelBySlot = new ushort[8];
            ArmorColorBySlot = new ushort[8];
            WeaponModelBySlot = new ushort[4];
        }
    }
}

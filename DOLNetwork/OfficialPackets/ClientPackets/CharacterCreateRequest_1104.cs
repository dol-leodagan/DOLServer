﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ClientPackets
{
    /// <summary>
    /// Character Create Request. From version 1104.
    /// </summary>
    [ClientPacket(FromVersion = 1104, PacketCode = 0xFF)]
    public class CharacterCreateRequest_1104 : CharacterCreateRequest_199
    {
        protected override void LoadCharacterFromPacket(PacketIn packet, ICharacterOverviewData character)
        {
            character.Unknown1 = packet.ReadUIntLowEndian();

            character.CharacterName = packet.ReadString(24);
            
            // Customization
            character.Customized = (byte)packet.ReadByte();
            character.EyeSize = (byte)packet.ReadByte();
            character.LipSize = (byte)packet.ReadByte();
            character.EyeColor = (byte)packet.ReadByte();
            character.HairColor = (byte)packet.ReadByte();
            character.FaceType = (byte)packet.ReadByte();
            character.HairStyle = (byte)packet.ReadByte();
            character.ExtensionBootsGloves = (byte)packet.ReadByte();
            character.ExtensionTorsoCloakHood = (byte)packet.ReadByte();
            character.CustomizationStep = (byte)packet.ReadByte();
            character.MoodType = (byte)packet.ReadByte();
            character.Emblem = (byte)packet.ReadByte();
            
            for (int count = 0 ; count < character.Unknown0.Length ; count++) // Should be 12
                character.Unknown0[count] = (byte)packet.ReadByte();
            
            character.ZoneDescription = packet.ReadString(24);
            
            character.ClassName = packet.ReadString(24);
            
            character.RaceName = packet.ReadString(24);
            
            character.Level = (byte)packet.ReadByte();
            
            character.ClassId = (byte)packet.ReadByte();
            
            character.RealmCode = (byte)packet.ReadByte();
            
            character.GenderRace = (byte)packet.ReadByte();
            
            character.Model = packet.ReadUShortLowEndian();
            
            character.RegionId = (byte)packet.ReadByte();
            
            character.RegionId2 = (byte)packet.ReadByte();
            
            character.DatabaseId = packet.ReadUIntLowEndian();
            
            
            character.Strength = (byte)packet.ReadByte();
            
            character.Dexterity = (byte)packet.ReadByte();
            
            character.Constitution = (byte)packet.ReadByte();
            
            character.Quickness = (byte)packet.ReadByte();
            
            character.Intelligence = (byte)packet.ReadByte();
            
            character.Piety = (byte)packet.ReadByte();
            
            character.Empathy = (byte)packet.ReadByte();
            
            character.Charisma = (byte)packet.ReadByte();
            
            
            for (int count = 0 ; count < character.ArmorModelBySlot.Count ; count++) // Should be 8
                character.ArmorModelBySlot[count] = packet.ReadUShortLowEndian();
            
            for (int count = 0 ; count < character.ArmorColorBySlot.Count ; count++) // Should be 8
                character.ArmorColorBySlot[count] = packet.ReadUShortLowEndian();
            
            for (int count = 0 ; count < character.WeaponModelBySlot.Count ; count++) // Should be 4
                character.WeaponModelBySlot[count] = packet.ReadUShortLowEndian();
            
            
            character.ActiveRightSlot = (byte)packet.ReadByte();
            
            character.ActiveLeftSlot = (byte)packet.ReadByte();
            
            character.ShroudedIslesZone = (byte)packet.ReadByte();
            
            character.RealConstitution = (byte)packet.ReadByte();
        }
    }
}

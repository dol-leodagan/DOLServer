﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;

using DOL.Network.Packets;
using DOL.Utils.Binary;

namespace DOL.Network.OfficialPackets.ClientPackets
{
    /// <summary>
    /// Character Create Request. From version 173.
    /// </summary>
    [ClientPacket(FromVersion = 173, PacketCode = 0xFF)]
    public class CharacterCreateRequest_173 : CharacterCreateRequest
    {
        public CharacterCreateRequest_173()
        {
            Characters = Enumerable.Repeat(true, 10).Select(i => new CharacterOverviewData()).ToArray();
        }
        
        public override void LoadFrom(PacketIn packet)
        {
            AccountNameWithRealm = packet.ReadString(24);
            
            foreach (var character in Characters)
            {
                LoadCharacterFromPacket(packet, character);
                character.Operation = (eCreateOperation)character.Unknown0.ReadUint(7);
                
                if (!Enum.IsDefined(typeof(eCreateOperation), character.Operation))
                    character.Operation = eCreateOperation.Unknown;
            }
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ClientPackets
{
    /// <summary>
    /// Character Overview Request.
    /// </summary>
    [ClientPacket(FromVersion = 168, PacketCode = 0xFC)]
    public class CharacterOverviewRequest : ICharacterOverviewRequest
    {
        public string AccountName { get; set; }
        
        public eRealm RealmRequest { get; set; }
        
        public uint Unknown0 { get; set; }
        
        public uint Unknown1 { get; set; }
        
        protected virtual string AccountNameWithRealm
        {
            set
            {
                var realmSuffix = value.Substring(value.Length - 2, 2);
                
                AccountName = value.Substring(0, value.Length - 2);
                
                switch(realmSuffix)
                {
                    case "-S":
                        RealmRequest = eRealm.Albion;
                        break;
                    case "-N":
                        RealmRequest = eRealm.Midgard;
                        break;
                    case "-H":
                        RealmRequest = eRealm.Hibernia;
                        break;
                    case "-X":
                        RealmRequest = eRealm.None;
                        break;
                    default:
                        RealmRequest = eRealm.None;
                        AccountName = value;
                        break;
                }
            }
        }
        
        public virtual void LoadFrom(PacketIn packet)
        {
            AccountNameWithRealm = packet.ReadString(20);
            
            Unknown0 = packet.ReadUIntLowEndian();
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ClientPackets
{
    /// <summary>
    /// Character Reset Request Client Packet.
    /// </summary>
    [ClientPacket(FromVersion = 168, PacketCode = 0xAC)]
    public class CharacterResetRequest : ICharacterResetRequest
    {
        public ushort SessionId { get; set; }
        public ushort Unknown0 { get; set; }
        
        public virtual void LoadFrom(PacketIn packet)
        {
            SessionId = packet.ReadUShort();
            
            Unknown0 = packet.ReadUShortLowEndian();
        }
    }
}

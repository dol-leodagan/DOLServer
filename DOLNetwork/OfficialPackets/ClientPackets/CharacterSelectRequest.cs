﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ClientPackets
{
    /// <summary>
    /// Character Select Request.
    /// </summary>
    [ClientPacket(FromVersion = 168, PacketCode = 0x10)]
    public class CharacterSelectRequest : ICharacterSelectRequest
    {
        public ushort SessionId { get; set; }
        
        public byte RegionIndex { get; set; }
        
        public byte Unknown0 { get; set; }
        
        public byte ServerId { get; set; }
        
        public string CharacterName { get; set; }
        
        public ushort Unknown3 { get; set; }
        
        public byte Unknown4 { get; set; }
        
        public uint Unknown1 { get; set; }
        
        public string AccountName { get; set; }
        
        public uint ClientUnknown0 { get; set; }
        
        public uint ClientUnknown1 { get; set; }
        
        public uint ClientUnknown2 { get; set; }
        
        public uint ClientUnknown3 { get; set; }
        
        public uint ClientUnknown4 { get; set; }
        
        public uint ClientUnknown5 { get; set; }
        
        public uint ClientUnknown6 { get; set; }
        
        public uint ClientUnknown7 { get; set; }
        
        public uint ClientUnknown8 { get; set; }
        
        public ushort Port { get; set; }
        
        public ushort Unknown2 { get; set; }
        
        public string Language { get; set; }
        
        public virtual void LoadFrom(PacketIn packet)
        {
            SessionId = packet.ReadUShort();
            
            RegionIndex = (byte)packet.ReadByte();

            Unknown0 = (byte)packet.ReadByte();
            
            CharacterName = packet.ReadString(24);
            
            Unknown1 = packet.ReadUInt();
            
            AccountName = packet.ReadString(20);
            
            ClientUnknown0 = packet.ReadUIntLowEndian();
        
            ClientUnknown1 = packet.ReadUIntLowEndian();
            
            ClientUnknown2 = packet.ReadUIntLowEndian();
            
            ClientUnknown3 = packet.ReadUIntLowEndian();
            
            ClientUnknown4 = packet.ReadUIntLowEndian();
            
            ClientUnknown5 = packet.ReadUIntLowEndian();
            
            ClientUnknown6 = packet.ReadUIntLowEndian();
            
            ClientUnknown7 = packet.ReadUIntLowEndian();
            
            Port = packet.ReadUShort();
            
            Unknown2 = packet.ReadUShort();
        }
    }
}

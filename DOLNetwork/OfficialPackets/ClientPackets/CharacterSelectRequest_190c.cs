﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ClientPackets
{
    /// <summary>
    /// Character Select Request from version 1.90c.
    /// </summary>
    [ClientPacket(FromVersion = 190, FromRevision = "c", PacketCode = 0x10)]
    public class CharacterSelectRequest_190c : CharacterSelectRequest_174
    {
        public override void LoadFrom(PacketIn packet)
        {
            SessionId = packet.ReadUShort();
            
            RegionIndex = (byte)packet.ReadByte();
            
            Unknown0 = (byte)packet.ReadByte();
            
            ServerId = (byte)packet.ReadByte();
            
            CharacterName = packet.ReadString(24);
            
            Unknown3 = packet.ReadUShort();
            
            Unknown4 = (byte)packet.ReadByte();
            
            Unknown1 = packet.ReadUIntLowEndian();
            
            AccountName = packet.ReadString(20);
            
            ClientUnknown0 = packet.ReadUIntLowEndian();
            
            ClientUnknown1 = packet.ReadUIntLowEndian();
            
            ClientUnknown2 = packet.ReadUIntLowEndian();
            
            ClientUnknown3 = packet.ReadUIntLowEndian();
            
            ClientUnknown4 = packet.ReadUIntLowEndian();
            
            ClientUnknown5 = packet.ReadUIntLowEndian();
            
            ClientUnknown6 = packet.ReadUIntLowEndian();
            
            ClientUnknown7 = packet.ReadUIntLowEndian();
            
            Port = packet.ReadUShort();
            
            Unknown2 = packet.ReadUShort();
            
            Language = packet.ReadString(8);
        }
    }
}

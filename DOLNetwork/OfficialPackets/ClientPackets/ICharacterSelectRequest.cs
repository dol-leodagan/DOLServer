﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ClientPackets
{
    /// <summary>
    /// 0x10 Character Select Request.
    /// </summary>
    public interface ICharacterSelectRequest : IClientPacket
    {
        ushort SessionId { get; set; }
        
        byte RegionIndex { get; set; }
        
        byte Unknown0 { get; set; }
        
        byte ServerId { get; set; }
        
        string CharacterName { get; set; }
        
        ushort Unknown3 { get; set; }
        
        byte Unknown4 { get; set; }
        
        uint Unknown1 { get; set; }
        
        string AccountName { get; set; }
        
        uint ClientUnknown0 { get; set; }
        
        uint ClientUnknown1 { get; set; }
        
        uint ClientUnknown2 { get; set; }
        
        uint ClientUnknown3 { get; set; }
        
        uint ClientUnknown4 { get; set; }
        
        uint ClientUnknown5 { get; set; }
        
        uint ClientUnknown6 { get; set; }
        
        uint ClientUnknown7 { get; set; }
        
        uint ClientUnknown8 { get; set; }
        
        ushort Port { get; set; }
        
        ushort Unknown2 { get; set; }
        
        string Language { get; set; }
    }
}

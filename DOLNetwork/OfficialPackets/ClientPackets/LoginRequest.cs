﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ClientPackets
{
    /// <summary>
    /// Client Login Request Packet.
    /// </summary>
    [ClientPacket(FromVersion = 168, PacketCode = 0xA7)]
    public class LoginRequest : ILoginRequest
    {
        public byte UnknownHeader { get; set; }
        
        public byte ClientType { get; set; }
        
        public byte ClientAddons { get; set; }
        
        public int ClientVersion { get; set; }
        
        public string ClientRevision { get; set; }
        
        public string Password { get; set; }
        
        public byte[] UnknownFlags { get; set; }
        
        public string AccountName { get; set; }

        public virtual void LoadFrom(PacketIn packet)
        {
            UnknownHeader = (byte)packet.ReadByte();
            
            var clientTypeAndAddons = packet.ReadByte();
            ClientType = (byte)(clientTypeAndAddons & 0x0F);
            ClientAddons = (byte)(clientTypeAndAddons & 0xF0);
            
            var major = (byte)packet.ReadByte();
            var minor = (byte)packet.ReadByte();
            var build = (byte)packet.ReadByte();
            ClientVersion = minor < 10 ? major * 100 + minor * 10 + build : major * 1000 + minor * 10 + build;
            
            ClientRevision = string.Empty;
            
            Password = packet.ReadString(19);
            
            UnknownFlags = new byte[51];
            
            for(var c = 0 ; c < UnknownFlags.Length ; c++)
                UnknownFlags[c] = (byte)packet.ReadByte();
            
            AccountName = packet.ReadString(20);
        }
    }
}

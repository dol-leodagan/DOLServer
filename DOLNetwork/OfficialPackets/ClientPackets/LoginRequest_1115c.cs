﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ClientPackets
{
    /// <summary>
    /// Client Login Request Packet. From 1.115c.
    /// </summary>
    [ClientPacket(FromVersion = 1115, FromRevision = "c", PacketCode = 0xA7)]
    public class LoginRequest_1115c : LoginRequest_1104
    {
        public override void LoadFrom(PacketIn packet)
        {
            UnknownHeader = 0;
            
            var clientTypeAndAddons = packet.ReadByte();
            ClientType = (byte)(clientTypeAndAddons & 0x0F);
            ClientAddons = (byte)(clientTypeAndAddons & 0xF0);
            
            var major = (byte)packet.ReadByte();
            var minor = (byte)packet.ReadByte();
            var build = (byte)packet.ReadByte();
            ClientVersion = major * 1000 + minor * 100 + build;
            
            ClientRevision = string.Format("{0}{1:X4}", (char)packet.ReadByte(), packet.ReadUShortLowEndian());
            
            UnknownFlags = new byte[0];
            
            AccountName = packet.ReadLowEndianShortPascalString();
            Password = packet.ReadLowEndianShortPascalString();
        }
    }
}

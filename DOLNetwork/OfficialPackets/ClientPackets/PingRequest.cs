﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ClientPackets
{
    /// <summary>
    /// Client Ping Request Packet.
    /// </summary>
    [ClientPacket(FromVersion = 168, PacketCode = 0xA3)]
    public class PingRequest : IPingRequest
    {
        public ushort Unknown1 { get; set; }
        public ushort Unknown2 { get; set; }
        public uint TimeStamp { get; set; }
        public uint Unknown3 { get; set; }
        public ushort? Sequence { get; protected set; }

        public virtual void LoadFrom(PacketIn packet)
        {
            Unknown1 = packet.ReadUShort();
            Unknown2 = packet.ReadUShort();
            TimeStamp = packet.ReadUInt();
            Unknown3 = packet.ReadUInt();
            
            var offPacket = packet as OfficialClientPacketIn;
            if (packet != null)
                Sequence = offPacket.Sequence;
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;

namespace DOL.Network.OfficialPackets
{
    /// <summary>
    /// Character Overview Data Exchanged for Character Selection and Creation.
    /// </summary>
    public interface ICharacterOverviewData
    {
        string CharacterName { get; set; }
        
        // Customization
        byte Customized { get; set; }
        byte EyeSize { get; set; }
        byte LipSize { get; set; }
        byte EyeColor { get; set; }
        byte HairColor { get; set; }
        byte FaceType { get; set; }
        byte HairStyle { get; set; }
        byte ExtensionBootsGloves { get; set; }
        byte ExtensionTorsoCloakHood { get; set; }
        byte CustomizationStep { get; set; }
        byte MoodType { get; set; }
        byte Emblem { get; set; }
        
        byte[] Unknown0 { get; set; }
        
        string ZoneDescription { get; set; }
        
        string ClassName { get; set; }
        
        string RaceName { get; set; }
        
        byte Level { get; set; }
        
        byte ClassId { get; set; }
        
        eCharacterClass Class { get; set; }
        
        byte RealmCode { get; set; }
        
        eRealm Realm { get; set; }
        
        byte GenderRace { get; set; }
                
        eGender Gender { get; set; }
        
        eRace Race { get; set; }
        
        bool ShroudedIslesStartLocation { get; set; }
        
        
        ushort Model { get; set; }
        
        byte RegionId { get; set; }
        
        byte RegionId2 { get; set; }
        
        uint DatabaseId { get; set; }
        
        
        byte Strength { get; set; }
        
        byte Dexterity { get; set; }
        
        byte Constitution { get; set; }
        
        byte Quickness { get; set; }
        
        byte Intelligence { get; set; }
        
        byte Piety { get; set; }
        
        byte Empathy { get; set; }
        
        byte Charisma { get; set; }
        
        
        IList<ushort> ArmorModelBySlot { get; set; }
        
        IList<ushort> ArmorColorBySlot { get; set; }
        
        IList<ushort> WeaponModelBySlot { get; set; }
        
        
        byte ActiveRightSlot { get; set; }
        
        byte ActiveLeftSlot { get; set; }
        
        byte ShroudedIslesZone { get; set; }
        
        byte RealConstitution { get; set; }
        
        uint Unknown1 { get; set; }
        
        eCreateOperation Operation { get; set; }
    }
}

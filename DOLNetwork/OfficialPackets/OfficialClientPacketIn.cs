﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets
{
    /// <summary>
    /// Inbound Game Packet from Official Client.
    /// </summary>
    public class OfficialClientPacketIn : PacketIn
    {
        public const int HEADER_SIZE = 10;
        public const int CHECKSUM_SIZE = 2;
        
        public override bool IsEndOfStream { get { return Position >= Length - HEADER_SIZE - CHECKSUM_SIZE; } }

        public ushort BodySize { get; private set; }
        
        public ushort Sequence { get; private set; }
        
        public ushort SessionId { get; private set; }
        
        public ushort Parameter { get; private set; }
        
        public ushort PacketCode { get; private set; }
        
        public ushort CheckSum { get; private set; }
        
        public bool IsCheckSumValidated {
            get
            {
                var pak = GetBuffer();
                byte val1 = 0x7E;
                byte val2 = 0x7E;
                var i = 0;
                var len = Length - CHECKSUM_SIZE;
    
                while (i < len)
                {
                    unchecked
                    {
                        val1 += pak[i++];
                        val2 += val1;
                    }
                }
                
                return CheckSum == unchecked((ushort)(val2 - ((val1 + val2) << 8)));
            }
        }
        
        public override long Position {
            get {
                return base.Position - HEADER_SIZE;
            }
            set {
                if (value > Length - HEADER_SIZE - CHECKSUM_SIZE)
                    throw new ArgumentOutOfRangeException("value", value, "Position is set above BodySize");
                
                base.Position = value + HEADER_SIZE;
            }
        }
        
        public OfficialClientPacketIn(byte[] buffer)
            : base(buffer)
        {
            if (buffer.Length < HEADER_SIZE + CHECKSUM_SIZE)
                throw new ArgumentException(string.Format("Byte array is smaller than Header Size ({0})", HEADER_SIZE + CHECKSUM_SIZE), "buffer");
            
            BodySize = base.ReadUShort();
            Sequence = base.ReadUShort();
            SessionId = base.ReadUShort();
            Parameter = base.ReadUShort();
            PacketCode = base.ReadUShort();
            
            base.Position = Length - CHECKSUM_SIZE;
            CheckSum = base.ReadUShort();
            
            base.Position = HEADER_SIZE;
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets
{
    /// <summary>
    /// Outgoing Game Packet to Official Client using UDP channel.
    /// </summary>
    public class OfficialClientUDPPacketOut : PacketOut
    {
        public const int HEADER_SIZE = 5;
        
        public byte PacketCode { get; set; }
        
        public ushort Sequence { get; set; }
        
        public ushort BodySize { get { return Convert.ToUInt16(Length - HEADER_SIZE); } }
        
        public override long Position {
            get {
                return base.Position - HEADER_SIZE;
            }
            set {
                base.Position = value + HEADER_SIZE;
            }
        }
        
        public OfficialClientUDPPacketOut(byte PacketCode, ushort Sequence)
        {
            this.PacketCode = PacketCode;
            this.Sequence = Sequence;
            base.Position = HEADER_SIZE;
        }
        
        public override byte[] GetBuffer()
        {
            base.Position = 0;
            
            WriteUShort(BodySize);
            WriteUShort(Sequence);
            WriteByte(PacketCode);
            
            Capacity = Convert.ToInt32(Length);
            
            return base.GetBuffer();
        }
    }
}

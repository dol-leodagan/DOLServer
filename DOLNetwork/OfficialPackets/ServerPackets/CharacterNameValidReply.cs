﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ServerPackets
{
    /// <summary>
    /// Character Name Valid Reply Server Packet.
    /// </summary>
    [ServerPacket(FromVersion = 168, PacketCode = 0xC3)]
    public class CharacterNameValidReply : ICharacterNameValidReply
    {
        public string CharacterName { get; set; }
        public string AccountName { get; set; }
        public byte Code { get; set; }
        public byte[] Unknown0 { get; set; }
        
        public virtual bool Valid
        {
            get
            {
                return Code == 0x01;
            }
            set
            {
                Code = value ? (byte)0x01 : (byte)0x00;
            }
        }
        
        public CharacterNameValidReply()
        {
            Unknown0 = new byte[3];
            Code = 0x00;
        }
        
        public virtual void ExportTo(PacketOut packet)
        {
            packet.FillString(CharacterName, 30);
            packet.FillString(AccountName, 20);
            packet.WriteByte(Code);
            
            foreach (var content in Unknown0)
                packet.WriteByte(content);
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ServerPackets
{
    /// <summary>
    /// Character Overview Server Packet.
    /// </summary>
    [ServerPacket(FromVersion = 168, PacketCode = 0xFD)]
    public class CharacterOverview : ICharacterOverview
    {
        public string AccountName { get; set; }
        
        public byte[] Unknown0 { get; set; }
        
        public IList<ICharacterOverviewData> Characters { get; set; }
        
        public CharacterOverview()
        {
            Unknown0 = new byte[104];
            Characters = new ICharacterOverviewData[8];
        }
        
        protected virtual void WriteCharacterTo(PacketOut packet, ICharacterOverviewData character)
        {
            if (character == null)
            {
                packet.Fill(0x00, 184);
            }
            else
            {
                packet.FillString(character.CharacterName, 24);
                
                // Customization
                packet.WriteByte(character.Customized);
                packet.WriteByte(character.EyeSize);
                packet.WriteByte(character.LipSize);
                packet.WriteByte(character.EyeColor);
                packet.WriteByte(character.HairColor);
                packet.WriteByte(character.FaceType);
                packet.WriteByte(character.HairStyle);
                packet.WriteByte(character.ExtensionBootsGloves);
                packet.WriteByte(character.ExtensionTorsoCloakHood);
                packet.WriteByte(character.CustomizationStep);
                packet.WriteByte(character.MoodType);
                packet.WriteByte(character.Emblem);
                foreach (var unknown in character.Unknown0) // Should be 12
                    packet.WriteByte(unknown);
                
                packet.FillString(character.ZoneDescription, 24);
                packet.FillString(character.RaceName, 24);
                packet.FillString(character.ClassName, 24);
                
                packet.WriteByte(character.Level);
                packet.WriteByte(character.ClassId);
                packet.WriteByte(character.RealmCode);
                packet.WriteByte(character.GenderRace);
                packet.WriteUShortLowEndian(character.Model);
                packet.WriteByte(character.RegionId);
                packet.WriteByte(character.RegionId2);
                packet.WriteUIntLowEndian(character.DatabaseId);
                
                packet.WriteByte(character.Strength);
                packet.WriteByte(character.Dexterity);
                packet.WriteByte(character.Constitution);
                packet.WriteByte(character.Quickness);
                packet.WriteByte(character.Intelligence);
                packet.WriteByte(character.Piety);
                packet.WriteByte(character.Empathy);
                packet.WriteByte(character.Charisma);
                
                foreach (var armor in character.ArmorModelBySlot) // Should be 8 count
                    packet.WriteUShortLowEndian(armor);
                
                foreach (var color in character.ArmorColorBySlot) // Should be 8 count
                    packet.WriteUShortLowEndian(color);
                
                foreach (var weapon in character.WeaponModelBySlot) // Should be 4 count
                    packet.WriteUShortLowEndian(weapon);
                
                packet.WriteByte(character.ActiveRightSlot);
                packet.WriteByte(character.ActiveLeftSlot);
                packet.WriteByte(character.ShroudedIslesZone);
                packet.WriteByte(character.RealConstitution);
            }
        }
        
        public virtual void ExportTo(PacketOut packet)
        {
            packet.FillString(AccountName, 24);
            
            foreach (var character in Characters)
            {
                WriteCharacterTo(packet, character);
            }
            
            foreach (byte content in Unknown0)
                packet.WriteByte(content);
        }
    }
}

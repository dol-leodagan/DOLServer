﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ServerPackets
{
    /// <summary>
    /// Character Overview Server Packet. From version 199.
    /// </summary>
    [ServerPacket(FromVersion = 199, PacketCode = 0xFD)]
    public class CharacterOverview_199 : CharacterOverview_173
    {
        public CharacterOverview_199()
        {
            Unknown0 = new byte[90];
        }
        
        public override void ExportTo(PacketOut packet)
        {
            packet.FillString(AccountName, 24);
            
            foreach (var character in Characters)
            {
                WriteCharacterTo(packet, character);
                packet.WriteUIntLowEndian(character != null ? character.Unknown1 : 0);
            }
            
            foreach (byte content in Unknown0)
                packet.WriteByte(content);
        }
    }
}

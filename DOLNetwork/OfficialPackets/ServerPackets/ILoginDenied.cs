﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ServerPackets
{
    public enum LoginDeniedType : byte
    {
        WrongPassword = 0x01, // "Your Password is Incorrect!"
        AccountInvalid = 0x02, // "Your Account is Invalid!"
        AuthorizationServerUnavailable = 0x03, // "The Authorization Server is Unavailable!"
        Unknown0 = 0x04,
        ClientVersionTooLow = 0x05, // "You do not have the latest version!"
        CannotAccessUserAccount = 0x06, // "Error Accessing User Account!"
        AccountNotFound = 0x07, // "No Record for User Found!"
        AccountNoAccessAnyGame = 0x08, // "Your account has no access to any games!"
        AccountNoAccessThisGame = 0x09, // "Your account has no access to this game!"
        AccountClosed = 0x0a, // "Your account has been closed!"
        AccountAlreadyLoggedIn = 0x0b, // "Your account is already logged in!"
        TooManyPlayersLoggedIn = 0x0c, // "Too many Players are logged in. Please try later."
        GameCurrentlyClosed = 0x0d, // "The game is currently closed. Please try later."
        Unknown1 = 0x0e,
        Unknown2 = 0x0f,
        AccountAlreadyLoggedIntoOtherServer = 0x10, // "Your account is already logged in to a different server!"
        AccountIsInLogoutProcedure = 0x11, // "Your account is being logged out! Try back in 1 minute."
        ExpansionPacketNotAllowed = 0x12, // "You have not been invited to join this server type."
        AccountIsBannedFromThisServerType = 0x13, // "Your account is banned from this server type."
        CafeIsOutOfPlayingTime = 0x14, // "The Cafe you are connecting through is out of playing time."
        PersonalAccountIsOutOfTime = 0x15, // "Your personal account is out of time."
        CafesAccountIsSuspended = 0x16, // "The Cafe's account is suspended."
        NotAuthorizedToUseExpansionVersion = 0x17, // "You are not authorized to use the expansion version!"
        Unknown3 = 0x18,
        ServiceNotAvailable = 0xaa
    }
    
    /// <summary>
    /// 0x22 Login Denied Server Packet.
    /// </summary>
    public interface ILoginDenied : IServerPacket
    {
        
        byte ClientType { get; set; }
        
        byte ClientAddons { get; set; }
        
        int ClientVersion { get; set; }
        
        string ClientRevision { get; set; }
        
        LoginDeniedType Reason { get; set; }
    }
}

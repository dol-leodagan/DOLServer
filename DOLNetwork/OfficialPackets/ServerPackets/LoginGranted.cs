﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ServerPackets
{
    /// <summary>
    /// Login Granted Server Packet.
    /// </summary>
    [ServerPacket(FromVersion = 168, PacketCode = 0x2A)]
    public class LoginGranted : ILoginGranted
    {
        public byte ClientType { get; set; }
        
        public byte ClientAddons { get; set; }
        
        public int ClientVersion { get; set; }
        
        public string AccountName { get; set; }
        
        public string ServerName { get; set; }
        
        public byte ServerId { get; set; }
        
        public byte ColorHandling { get; set; }
        
        public byte IsTrial { get; set; }

        public virtual void ExportTo(PacketOut packet)
        {
            // Is Shrouded Isles
            packet.WriteByte(1);
            
            packet.WriteByte((byte)(ClientVersion > 199 ? ClientVersion / 1000 : ClientVersion / 100));
            
            packet.WriteByte((byte)(ClientVersion > 199 ? (ClientVersion % 1000) / 10 : (ClientVersion % 100) / 10 ));
            
            packet.WriteByte((byte)(ClientVersion % 10));
            
            packet.WritePascalString(AccountName);
            
            packet.WritePascalString(ServerName);
            
            packet.WriteByte(ServerId);
            
            packet.WriteByte(ColorHandling);
            
            packet.WriteByte(IsTrial);
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.Packets;

namespace DOL.Network.OfficialPackets.ServerPackets
{
    /// <summary>
    /// Version And CryptKey Server Packet From 1.115c.
    /// </summary>
    [ServerPacket(FromVersion = 1115, FromRevision = "c", PacketCode = 0x22)]
    public class VersionAndCryptKey_1115c : VersionAndCryptKey
    {
        public override void ExportTo(PacketOut packet)
        {
            // Only send ClientType...
            packet.WriteByte((byte)(ClientType & 0x0F));
            // Disable Encryption Flag
            packet.WriteByte(0);
            // Version (max 5 char)
            packet.WriteRawString(string.Format("{0}.{1}", ClientVersion / 1000, ClientVersion % 1000), 5);
            // Build
            packet.WriteByte(!string.IsNullOrEmpty(ClientRevision) ? (byte)ClientRevision[0] : (byte)0);
            // Serial
            packet.WriteUShortLowEndian(!string.IsNullOrEmpty(ClientRevision) && ClientRevision.Length > 4 ? ushort.Parse(string.Format("{0}{1}{2}{3}", ClientRevision[1], ClientRevision[2], ClientRevision[3], ClientRevision[4])) : (ushort)0);
        }
    }
}

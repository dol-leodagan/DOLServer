﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

using DOL.Network.Packets;

namespace DOL.Network
{
    /// <summary>
    /// Packet Processor Caches Client Version/Revision Packet Resolver.
    /// </summary>
    public class PacketProcessor
    {
        public const int CLIENTPACKET_PACKETCODE_MAXVALUE = 256;
        public const int SERVERPACKET_PACKETCODE_MAXVALUE = 256;
        
        readonly Dictionary<Tuple<int, string>, WeakReference<Type[]>> ClientPacketProcessorCache = new Dictionary<Tuple<int, string>, WeakReference<Type[]>>();
        readonly Dictionary<Tuple<int, string>, WeakReference<Type[]>> ServerPacketProcessorCache = new Dictionary<Tuple<int, string>, WeakReference<Type[]>>();
        
        readonly object ClientLockObject = new object();
        readonly object ServerLockObject = new object();
        
        public Type[] GetClientProcessorFor(int ClientVersion, string ClientRevision)
        {
            lock(ClientLockObject)
            {
                WeakReference<Type[]> result;
                var key = new Tuple<int, string>(ClientVersion, ClientRevision);
                if (ClientPacketProcessorCache.TryGetValue(key, out result))
                {
                    Type[] processor;
                    if (result.TryGetTarget(out processor) && processor != null)
                        return processor;
                    
                    ClientPacketProcessorCache.Remove(key);
                }
                
                var packetHandlers = AppDomain.CurrentDomain.GetAssemblies()
                    .Select(asm => asm.GetTypes()
                            .Where(type => type.IsClass && !type.IsAbstract && typeof(IClientPacket).IsAssignableFrom(type)))
                    .SelectMany(types => types)
                    .Select(types => new { Type = types, Attributes = types.GetCustomAttributes<ClientPacketAttribute>() })
                    .Where(objs => objs.Attributes.Any())
                    .SelectMany(objs => objs.Attributes.Select(attr => new { Type = objs.Type, Attribute = attr }))
                    .GroupBy(attrs => attrs.Attribute.PacketCode)
                    .Select(grp => grp
                            .OrderBy(attrs => typeof(PacketProcessor).Assembly.Equals(attrs.Type.Assembly) ? 1 : 0)
                            .ThenByDescending(attrs => attrs.Attribute.FromVersion)
                            .ThenByDescending(attrs => attrs.Attribute.FromRevision)
                            .First(attrs => attrs.Attribute.FromVersion < ClientVersion
                                   || (attrs.Attribute.FromVersion == ClientVersion && string.CompareOrdinal(attrs.Attribute.FromRevision, ClientRevision) <= 0)))
                    .ToLookup(attrs => attrs.Attribute.PacketCode, attrs => attrs.Type);
                
                var res = new Type[CLIENTPACKET_PACKETCODE_MAXVALUE];
                
                for (var c = 0 ; c < res.Length ; c++)
                    res[c] = packetHandlers[c].SingleOrDefault();
                
                ClientPacketProcessorCache.Add(key, new WeakReference<Type[]>(res));
                return res;
            }
        }
        
        public Type[] GetServerProcessorFor(int ClientVersion, string ClientRevision)
        {
            lock(ServerLockObject)
            {
                WeakReference<Type[]> result;
                var key = new Tuple<int, string>(ClientVersion, ClientRevision);
                if (ServerPacketProcessorCache.TryGetValue(key, out result))
                {
                    Type[] processor;
                    if (result.TryGetTarget(out processor) && processor != null)
                        return processor;
                    
                    ServerPacketProcessorCache.Remove(key);
                }
                
                var packetHandlers = AppDomain.CurrentDomain.GetAssemblies()
                    .Select(asm => asm.GetTypes()
                            .Where(type => type.IsClass && !type.IsAbstract && typeof(IServerPacket).IsAssignableFrom(type)))
                    .SelectMany(types => types)
                    .Select(types => new { Type = types, Attributes = types.GetCustomAttributes<ServerPacketAttribute>() })
                    .Where(objs => objs.Attributes.Any())
                    .SelectMany(objs => objs.Attributes.Select(attr => new { Type = objs.Type, Attribute = attr }))
                    .GroupBy(attrs => attrs.Attribute.PacketCode)
                    .Select(grp => grp
                            .OrderBy(attrs => typeof(PacketProcessor).Assembly.Equals(attrs.Type.Assembly) ? 1 : 0)
                            .ThenByDescending(attrs => attrs.Attribute.FromVersion)
                            .ThenByDescending(attrs => attrs.Attribute.FromRevision)
                            .First(attrs => attrs.Attribute.FromVersion < ClientVersion
                                   || (attrs.Attribute.FromVersion == ClientVersion && string.CompareOrdinal(attrs.Attribute.FromRevision, ClientRevision) <= 0)))
                    .ToLookup(attrs => attrs.Attribute.PacketCode, attrs => attrs.Type);
                
                var res = new Type[SERVERPACKET_PACKETCODE_MAXVALUE];
                
                for (var c = 0 ; c < res.Length ; c++)
                    res[c] = packetHandlers[c].SingleOrDefault();
                
                ServerPacketProcessorCache.Add(key, new WeakReference<Type[]>(res));
                return res;
            }
        }
    }
}

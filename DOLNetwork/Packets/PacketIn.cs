﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Text;

namespace DOL.Network.Packets
{
    /// <summary>
    /// Inbound Game Packet.
    /// </summary>
    public class PacketIn : System.IO.MemoryStream
    {
        public Encoding Encoding { get; set; }
        
        public virtual bool IsEndOfStream { get { return Position >= Length; } }
        
        public PacketIn(byte[] buffer, Encoding Encoding)
            : base(buffer, 0, buffer.Length, false, true)
        {
            this.Encoding = Encoding;
        }
        
        public PacketIn(byte[] buffer)
            : this(buffer, Encoding.GetEncoding("Windows-1252"))
        {
        }
        
        public virtual void Skip(int count)
        {
            var position = Position;
            Position = position + count;
        }
        
        public virtual ushort ReadUShort()
        {
            var bytes = new []{ (byte)ReadByte(), (byte)ReadByte() };
            
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            
            return BitConverter.ToUInt16(bytes, 0);
        }
        
        public virtual ushort ReadUShortLowEndian()
        {
            var bytes = new []{ (byte)ReadByte(), (byte)ReadByte() };
            
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            
            return BitConverter.ToUInt16(bytes, 0);
        }
        
        public virtual short ReadShort()
        {
            var bytes = new []{ (byte)ReadByte(), (byte)ReadByte() };
            
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            
            return BitConverter.ToInt16(bytes, 0);
        }
        
        public virtual short ReadShortLowEndian()
        {
            var bytes = new []{ (byte)ReadByte(), (byte)ReadByte() };
            
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            
            return BitConverter.ToInt16(bytes, 0);
        }
        
        public virtual uint ReadUInt()
        {
            var bytes = new []{ (byte)ReadByte(), (byte)ReadByte(), (byte)ReadByte(), (byte)ReadByte() };
            
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            
            return BitConverter.ToUInt32(bytes, 0);
        }

        public virtual uint ReadUIntLowEndian()
        {
            var bytes = new []{ (byte)ReadByte(), (byte)ReadByte(), (byte)ReadByte(), (byte)ReadByte() };
            
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            
            return BitConverter.ToUInt32(bytes, 0);
        }
        
        public virtual int ReadInt()
        {
            var bytes = new []{ (byte)ReadByte(), (byte)ReadByte(), (byte)ReadByte(), (byte)ReadByte() };
            
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            
            return BitConverter.ToInt32(bytes, 0);
        }

        public virtual int ReadIntLowEndian()
        {
            var bytes = new []{ (byte)ReadByte(), (byte)ReadByte(), (byte)ReadByte(), (byte)ReadByte() };
            
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            
            return BitConverter.ToInt32(bytes, 0);
        }
        
        public virtual string ReadString(int maxlen)
        {
            var buf = new byte[maxlen];
            Read(buf, 0, maxlen);
            
            for (var c = 0 ; c < maxlen ; c++)
                if (buf[c] == 0)
                    return Encoding.GetString(buf, 0, c);
            
            return Encoding.GetString(buf);
        }
        
        public virtual string ReadPascalString()
        {
            return ReadString(ReadByte());
        }
        
        public virtual string ReadLowEndianShortPascalString()
        {
            return ReadString(ReadUShortLowEndian());
        }
    }
}

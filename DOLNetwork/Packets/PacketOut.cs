﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Text;

namespace DOL.Network.Packets
{
    /// <summary>
    /// Outgoing Game Packet.
    /// </summary>
    public class PacketOut : System.IO.MemoryStream
    {
        public Encoding Encoding { get; set; }

        public PacketOut(Encoding Encoding)
        {
            this.Encoding = Encoding;
        }
        
        public PacketOut()
            : this(Encoding.GetEncoding("Windows-1252"))
        {
        }
        
        public virtual void WriteShort(short val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
        }
        
        public virtual void WriteShortLowEndian(short val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
        }
        
        public virtual void WriteUShort(ushort val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
        }
        
        public virtual void WriteUShortLowEndian(ushort val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
        }
        
        public virtual void WriteInt(int val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
            WriteByte(buff[2]);
            WriteByte(buff[3]);
        }
        
        public virtual void WriteIntLowEndian(int val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
            WriteByte(buff[2]);
            WriteByte(buff[3]);
        }
        
        public virtual void WriteUInt(uint val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
            WriteByte(buff[2]);
            WriteByte(buff[3]);
        }
        
        public virtual void WriteUIntLowEndian(uint val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
            WriteByte(buff[2]);
            WriteByte(buff[3]);
        }
        
        public virtual void WriteLong(long val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
            WriteByte(buff[2]);
            WriteByte(buff[3]);
            WriteByte(buff[4]);
            WriteByte(buff[5]);
            WriteByte(buff[6]);
            WriteByte(buff[7]);
        }
        
        public virtual void WriteLongLowEndian(long val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
            WriteByte(buff[2]);
            WriteByte(buff[3]);
            WriteByte(buff[4]);
            WriteByte(buff[5]);
            WriteByte(buff[6]);
            WriteByte(buff[7]);
        }
        
        public virtual void WriteULong(ulong val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
            WriteByte(buff[2]);
            WriteByte(buff[3]);
            WriteByte(buff[4]);
            WriteByte(buff[5]);
            WriteByte(buff[6]);
            WriteByte(buff[7]);
        }
        
        public virtual void WriteULongLowEndian(ulong val)
        {
            var buff = BitConverter.GetBytes(val);
            
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buff);
            
            WriteByte(buff[0]);
            WriteByte(buff[1]);
            WriteByte(buff[2]);
            WriteByte(buff[3]);
            WriteByte(buff[4]);
            WriteByte(buff[5]);
            WriteByte(buff[6]);
            WriteByte(buff[7]);
        }
        
        public virtual void Fill(byte val, int count)
        {
            for (var c = 0 ; c < count ; c++)
                WriteByte(val);
        }
        
        public virtual void WriteRawString(string val, int maxlen)
        {
            if (maxlen < 1)
                return;
            
            var str = val ?? string.Empty;
            var buf = Encoding.GetBytes(str);
            
            Write(buf, 0, buf.Length < maxlen ? buf.Length : maxlen);
        }
        
        public virtual void WriteRawString(string val)
        {
            var str = val ?? string.Empty;
            WriteRawString(str, Encoding.GetByteCount(val));
        }
        
        public virtual void WriteString(string val)
        {
            WriteRawString(val);
            WriteByte(0);
        }
        
        public virtual void WritePascalString(string val)
        {
            var str = val ?? string.Empty;
            var length = Encoding.GetByteCount(str);
            
            if (length > byte.MaxValue)
                throw new ArgumentOutOfRangeException("val", length, "Argument is too long for a Byte-Indexed Pascal String");
            
            WriteByte((byte)length);
            WriteRawString(str, length);
        }
        
        public virtual void WriteLowEndianShortPascalString(string val)
        {
            var str = val ?? string.Empty;
            var length = Encoding.GetByteCount(str);
            
            if (length > ushort.MaxValue)
                throw new ArgumentOutOfRangeException("val", length, "Argument is too long for a Short-Indexed Pascal String");
            
            WriteUShortLowEndian((ushort)length);
            WriteRawString(str, length);
        }
        
        /// <summary>
        /// Write string to Packet up to maxlen and fill with zero up to maxlen.
        /// </summary>
        /// <param name="val">string to write to packet.</param>
        /// <param name="maxlen">maxlen for string or fill with zero.</param>
        public virtual void FillString(string val, int maxlen)
        {
            var currentPosition = Position;
            WriteRawString(val, maxlen);
            
            var remaining = maxlen - (Position - currentPosition);
            
            if (remaining > 0)
                Fill(0, (int)remaining);
        }
        
        public virtual void WriteByte(bool value)
        {
            base.WriteByte((byte)(value ? 0x01 : 0x00));
        }
    }
}

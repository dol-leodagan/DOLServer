#region Using directives
using System;
using System.Reflection;
using System.Runtime.InteropServices;

#endregion
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle ("Dawn of Light - DOLNetwork")]
[assembly: AssemblyDescription ("Network Library for Dawn of Light server")]
[assembly: AssemblyConfiguration ("")]
[assembly: AssemblyCompany ("")]
[assembly: AssemblyProduct ("DAoC Server Side Emulation Package - Dawn of Light")]
[assembly: AssemblyCopyright ("Dawn of Light Development Team")]
[assembly: AssemblyTrademark ("Dawn of Light Development Team")]
[assembly: AssemblyCulture ("")]
// This sets the default COM visibility of types in the assembly to invisible.
// If you need to expose a type to COM, use [ComVisible(true)] on that type.
[assembly: ComVisible (false)]
// The assembly version has following format :
//
// Major.Minor.Build.Revision
//
// You can specify all the values or you can use the default the Revision and 
// Build Numbers by using the '*' as shown below:
[assembly: AssemblyVersion ("3.0.0.*")]

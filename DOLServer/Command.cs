﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.IO;
using System.Reflection;
using System.Threading;

using CommandLine;
using log4net;

using DOL.Commands;
using DOL.GS;

namespace DOL.Server
{
    /// <summary>
    /// Commands Handler for DOL Server Startup.
    /// </summary>
    public class Command : AbstractCommandHandler
    {
        protected override string DefaultVerb {
            get {
                return "start";
            }
        }
            
        /// <summary>
        /// Default Config Path, relative to Program Assembly.
        /// </summary>
        private const string DefaultConfigPath = "config/server.yml";
        
        /// <summary>
        /// Handle Config Update Command.
        /// </summary>
        [Verb("updateconfig", HelpText = "Update an existing configuration")]
        public class UpdateConfigVerb : ICommandVerb
        {
            /// <summary>
            /// Defines a logger for this class.
            /// </summary>
            private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

            /// <summary>
            /// Path to Alternate Config File.
            /// </summary>
            [Option('c', "config", HelpText = "Use alternate config File")]
            public string Config { get; set; }
            
            public void OnCommand()
            {
                var configFile = string.IsNullOrEmpty(Config)
                    ? new FileInfo(Path.Combine(new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName, DefaultConfigPath))
                    : new FileInfo(Config);
                    
                if (!configFile.Exists)
                {
                    if (log.IsErrorEnabled)
                        log.ErrorFormat("Specified Config File '{0}' does not exists!", configFile.FullName);
                    
                    return;
                }
                
                // Read and rewrite...
                var configuration = GameServerConfiguration.CreateConfigurationFromFile(configFile);
                configuration.WriteConfigurationToFile(configFile);
            }
        }

        /// <summary>
        /// Handle GameServer Start Command.
        /// </summary>
        [Verb("start", HelpText = "Start DOL Server")]
        public class StartVerb : ICommandVerb
        {
            /// <summary>
            /// Defines a logger for this class.
            /// </summary>
            private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            
            /// <summary>
            /// Exit Event to wait for Server Stop.
            /// </summary>
            private readonly ManualResetEvent ExitEvent = new ManualResetEvent(false);
            
            /// <summary>
            /// Path to Alternate Config File.
            /// </summary>
            [Option('c', "config", HelpText = "Use alternate config File")]
            public string Config { get; set; }
            
            /// <summary>
            /// Set Log Debug Output.
            /// </summary>
            [Option('d', "debug", HelpText = "Enable debug output for startup")]
            public bool Debug { get; set; }
            
            /// <summary>
            /// Enable Console Input.
            /// </summary>
            [Option('C', "console", HelpText = "Enable Console while running Server")]
            public bool EnableConsole { get; set; }
            
            /// <summary>
            /// Restart Initial Log4Net Manager with Debug Enabled.
            /// </summary>
            private static void SetLog4NetDebug()
            {
                LogManager.Shutdown();
                log4net.Config.BasicConfigurator.Configure(
                    new log4net.Appender.ConsoleAppender {
                        Layout = new log4net.Layout.SimpleLayout(),
                    });
            }
            
            /// <summary>
            /// Receive Program Exit Event.
            /// Terminate Server before Exiting.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="eventArgs"></param>
            private void ReceiveExitEvent(object sender, ConsoleCancelEventArgs eventArgs)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Console Exit Event Received - Terminating Server.");
                eventArgs.Cancel = true;
                ExitEvent.Set();
            }
            
            /// <summary>
            /// Receive Server Quit Event.
            /// Close Program when server is Stopped. 
            /// </summary>
            /// <param name="sender"></param>
            private void ReceiveQuitEvent(GameServer sender)
            {
                if (log.IsDebugEnabled)
                    log.Debug("Server Quit Event Received - Server Offline, Exiting.");
                ExitEvent.Set();
            }
            
            /// <summary>
            /// Start an Interactive Console for the Server
            /// </summary>
            /// <param name="gameserver"></param>
            private void RunConsole(GameServer gameserver)
            {
                var run = true;
                                
                while (run)
                {
                    try
                    {
                        Console.Write("> ");
                        string line = Console.ReadLine();
                        
                        if (!string.IsNullOrEmpty(line))
                        {
                            switch (line.ToLower())
                            {
                                case "exit":
                                    run = false;
                                    gameserver.Stop();
                                    break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (log.IsErrorEnabled)
                            log.ErrorFormat("Error in Interactive Console : {0}", ex);
                        
                        Console.WriteLine();
                    }
                }
                
                // Quit Console
                if (gameserver.Started)
                    gameserver.Stop();
            }
            
            /// <summary>
            /// Start Command Handler
            /// Run Game Server then wait for Break or Server Quit.
            /// </summary>
            public void OnCommand()
            {
                if (Debug)
                    SetLog4NetDebug();
                
                if (log.IsDebugEnabled)
                    log.DebugFormat("Starting Server with Arguments : Config '{0}' (Default '{1}'), Debug '{2}'", Config, DefaultConfigPath, Debug);
                
                FileInfo configFile;
                // Config Option specified
                if (!string.IsNullOrEmpty(Config))
                {
                    configFile = new FileInfo(Config);
                    
                    if (!configFile.Exists)
                    {
                        if (log.IsErrorEnabled)
                            log.ErrorFormat("Specified Config File '{0}' does not exists!", configFile.FullName);
                        
                        return;
                    }
                }
                else
                {
                    configFile = new FileInfo(Path.Combine(new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName, DefaultConfigPath));
                    
                    if (!configFile.Exists)
                    {
                        if (!configFile.Directory.Exists)
                        {
                            if (log.IsDebugEnabled)
                                log.DebugFormat("Config Directory '{0}' does not exists, creating it...");
                            
                            configFile.Directory.Create();
                        }
                        
                        if (log.IsInfoEnabled)
                            log.InfoFormat("Config File does not exists, creating default in '{0}'", configFile.FullName);
                        
                        new GameServerConfiguration().WriteConfigurationToFile(configFile);
                    }
                }
                
                if (log.IsInfoEnabled)
                    log.InfoFormat("Loading Config File from '{0}'", configFile.FullName);
                
                var configuration = GameServerConfiguration.CreateConfigurationFromFile(configFile);
                
                if (log.IsDebugEnabled)
                {
                    log.DebugFormat("Game Server Configuration Initialized : {0}", configuration);
                }
                
                var gameserver = new GameServer(configuration);
                
                gameserver.Quit += ReceiveQuitEvent;
                Console.CancelKeyPress += ReceiveExitEvent;
                
                if (!gameserver.Start())
                {
                    if (log.IsErrorEnabled)
                        log.ErrorFormat("Could not Start Game Server, exiting!");
                }
                else
                {
                    
                    if (EnableConsole)
                        RunConsole(gameserver);
                    
                    ExitEvent.WaitOne();
                }
                
                gameserver.Quit -= ReceiveQuitEvent;
                Console.CancelKeyPress -= ReceiveExitEvent;
                
                if (gameserver.Started)
                    gameserver.Stop();
            }
        }
    }
}

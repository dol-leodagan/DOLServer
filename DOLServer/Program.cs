﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Reflection;

using log4net;

namespace DOL.Server
{
    class Program
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        /// <summary>
        /// Init DOL Server.
        /// </summary>
        static Program()
        {
            log4net.Config.BasicConfigurator.Configure(
                new log4net.Appender.ConsoleAppender {
                    Layout = new log4net.Layout.SimpleLayout(),
                    #if !DEBUG
                    Threshold = log4net.Core.Level.Info,
                    #endif
                });
        }
        
        /// <summary>
        /// Program Entry Point
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            if (log.IsDebugEnabled)
                log.Debug("Executing DOLServer Main Entry Point.");
            
            // Parse Command Line
            var command = new Command();
            
            var result = command.ParseCommand(args);
            
            if (log.IsInfoEnabled && !string.IsNullOrEmpty(result))
                log.Info(result);
        }
    }
}
﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using NUnit.Framework;

using DOL.Network;
using DOL.Network.OfficialPackets;
using DOL.Network.OfficialPackets.ServerPackets;
using DOL.Network.OfficialPackets.ClientPackets;

namespace DOL.UnitTests.Network.Handlers
{
    /// <summary>
    /// Test of Packet Processor Resolutions
    /// </summary>
    [TestFixture]
    public class PacketProcessorTests
    {
        PacketProcessor processor;
        
        public PacketProcessorTests()
        {
            processor = new PacketProcessor();
        }
        
        [Test]
        public void ProcessorClient_For168_BaseHandler()
        {
            var types = processor.GetClientProcessorFor(168, string.Empty);
            
            Assert.AreEqual(typeof(FirstPacketAndCryptKey), types[0xF4]);
        }
        
        [Test]
        public void ProcessorClient_For168e8f5e_BaseHandler()
        {
            var types = processor.GetClientProcessorFor(168, "e8f5e");
            
            Assert.AreEqual(typeof(FirstPacketAndCryptKey), types[0xF4]);
        }
        
        [Test]
        public void ProcessorClient_For186_Handler186()
        {
            var types = processor.GetClientProcessorFor(186, string.Empty);
            
            Assert.AreEqual(typeof(FirstPacketAndCryptKey_186), types[0xF4]);
        }
        
        [Test]
        public void ProcessorClient_For186d9871_Handler186()
        {
            var types = processor.GetClientProcessorFor(186, "d9871");
            
            Assert.AreEqual(typeof(FirstPacketAndCryptKey_186), types[0xF4]);
        }
        
        [Test]
        public void ProcessorClient_For1115c_Handler1115c()
        {
            var types = processor.GetClientProcessorFor(1115, "c0000");
            
            Assert.AreEqual(typeof(FirstPacketAndCryptKey_1115c), types[0xF4]);
        }
        
        [Test]
        public void ProcessorClient_For1115b6574_Handler186()
        {
            var types = processor.GetClientProcessorFor(1115, "b6574");
            
            Assert.AreEqual(typeof(FirstPacketAndCryptKey_186), types[0xF4]);
        }
        
        [Test]
        public void ProcessorClient_For1115c6895_Handler1115c()
        {
            var types = processor.GetClientProcessorFor(1115, "c6895");
            
            Assert.AreEqual(typeof(FirstPacketAndCryptKey_1115c), types[0xF4]);
        }
        
        [Test]
        public void ProcessorClient_For1116_Handler1115c()
        {
            var types = processor.GetClientProcessorFor(1116, string.Empty);
            
            Assert.AreEqual(typeof(FirstPacketAndCryptKey_1115c), types[0xF4]);
        }
        
        [Test]
        public void ProcessorClient_For1122b1587_Handler1115c()
        {
            var types = processor.GetClientProcessorFor(1122, "b1587");
            
            Assert.AreEqual(typeof(FirstPacketAndCryptKey_1115c), types[0xF4]);
        }
        
        [Test]
        public void ProcessorServer_For168_BaseHandler()
        {
            var types = processor.GetServerProcessorFor(168, string.Empty);
            
            Assert.AreEqual(typeof(VersionAndCryptKey), types[0x22]);
        }
        
        [Test]
        public void ProcessorServer_For168e8f5e_BaseHandler()
        {
            var types = processor.GetServerProcessorFor(168, "e8f5e");
            
            Assert.AreEqual(typeof(VersionAndCryptKey), types[0x22]);
        }
        
        [Test]
        public void ProcessorServer_For186_Handler186()
        {
            var types = processor.GetServerProcessorFor(186, string.Empty);
            
            Assert.AreEqual(typeof(VersionAndCryptKey), types[0x22]);
        }
        
        [Test]
        public void ProcessorServer_For186d9871_Handler186()
        {
            var types = processor.GetServerProcessorFor(186, "d9871");
            
            Assert.AreEqual(typeof(VersionAndCryptKey), types[0x22]);
        }
        
        [Test]
        public void ProcessorServer_For1115c_Handler1115c()
        {
            var types = processor.GetServerProcessorFor(1115, "c0000");
            
            Assert.AreEqual(typeof(VersionAndCryptKey_1115c), types[0x22]);
        }
        
        [Test]
        public void ProcessorServer_For1115b6574_Handler186()
        {
            var types = processor.GetServerProcessorFor(1115, "b6574");
            
            Assert.AreEqual(typeof(VersionAndCryptKey), types[0x22]);
        }
        
        [Test]
        public void ProcessorServer_For1115c6895_Handler1115c()
        {
            var types = processor.GetServerProcessorFor(1115, "c6895");
            
            Assert.AreEqual(typeof(VersionAndCryptKey_1115c), types[0x22]);
        }
        
        [Test]
        public void ProcessorServer_For1116_Handler1115c()
        {
            var types = processor.GetServerProcessorFor(1116, string.Empty);
            
            Assert.AreEqual(typeof(VersionAndCryptKey_1115c), types[0x22]);
        }
        
        [Test]
        public void ProcessorServer_For1122b1587_Handler1115c()
        {
            var types = processor.GetServerProcessorFor(1122, "b1587");
            
            Assert.AreEqual(typeof(VersionAndCryptKey_1115c), types[0x22]);
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using NUnit.Framework;

using DOL.Network.OfficialPackets;

namespace DOL.UnitTests.Network.Packets
{
    /// <summary>
    /// Tests of Packet In/Out Mechanisms
    /// </summary>
    [TestFixture]
    public class PacketStreamTests
    {
        public PacketStreamTests()
        {
        }
        
        [Test]
        public void ValidateCheckSum_OnInboundValidPacket_True()
        {
            var buffer = new byte[] { 
                0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF4, 0x00, 0xF6, 0x01, 0x0A, 0x09, 0xA9,
                0xD6,
            };
            var packet = new OfficialClientPacketIn(buffer);
            
            Assert.AreEqual(0xA9 << 8 | 0xD6, packet.CheckSum);
            Assert.IsTrue(packet.IsCheckSumValidated);
        }
        
        [Test]
        public void ValidateCheckSum_OnInboundInValidPacket_False()
        {
            var buffer = new byte[] { 
                0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF4, 0x00, 0xF6, 0x01, 0x0A, 0x09, 0xFF,
                0xFF,
            };
            var packet = new OfficialClientPacketIn(buffer);
            
            Assert.IsFalse(packet.IsCheckSumValidated);
        }
    }
}

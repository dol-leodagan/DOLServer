﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Utils.Text;

using NUnit.Framework;

namespace DOL.UnitTests.Utils
{
    /// <summary>
    /// Test of Password Hash Extensions
    /// </summary>
    [TestFixture]
    public class PasswordHash
    {
        public PasswordHash()
        {
        }
        
        [Test]
        public void CheckHashAgainstPassword()
        {
            var password = "foobar";
            var hash = "{SHA512}{NqOIO2Kge3Ao8cVxtArWpA==}2UpbMoxzhxK6pOEFo4nvSpRrJ2AwXyeh5U+RSMXEwfz3HwZ16AhInIukXg1vFfPvOKVy3C/0ym1JuB1h4MFpuw==";
            
            Assert.IsTrue(password.EqualsPasswordHash(hash));
        }
        
        [Test]
        public void RoundTripPasswordHashCompare()
        {
            var password = "123456";
            
            Assert.IsTrue(password.EqualsPasswordHash(password.GeneratePasswordHash()));
        }
    }
}

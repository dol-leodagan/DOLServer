﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Collections.Generic;

using log4net;

using DOL.Network;
using DOL.GS.ClientObjects;
using DOL.Utils.ThreadSafeCollections;

namespace DOL.GS
{
    /// <summary>
    /// ClientManager handle notifications of Connection and Disconnection from Network
    /// </summary>
    public class ClientManager : DisposableComponent
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        GameServer Server { get; set; }
        
        readonly ReaderWriterDictionary<GameClient, GameClientHandler> GameClientHandlers = new ReaderWriterDictionary<GameClient, GameClientHandler>();
        
        public event DOLEventHandler<ClientManager, GameClientHandler> ClientConnecting;
        
        void OnClientCreated(GameClientHandler client)
        {
            var handler = ClientConnecting;
            
            if (handler != null)
                handler(this, client);
        }
        
        public event DOLEventHandler<ClientManager, GameClientHandler> ClientDispose;
        
        void OnClientDisposed(GameClientHandler client)
        {
            var handler = ClientDispose;
            
            if (handler != null)
                handler(this, client);
        }
        
        /// <summary>
        /// Get Client Count in this Instance.
        /// </summary>
        public int ClientCount { get { return GameClientHandlers.Count; } }
        
        /// <summary>
        /// Get All Clients.
        /// </summary>
        public ICollection<GameClientHandler> Clients { get { return GameClientHandlers.Values; } }
        
        /// <summary>
        /// Create a new Instance of <see cref="ClientManager"/>
        /// </summary>
        /// <param name="Server">GameServer Object to Listen to.</param>
        public ClientManager(GameServer Server)
        {
            this.Server = Server;
        }
        
        protected override void OnInit()
        {
            Server.Listener.GameClientCreated += ClientConnected;
            Server.Listener.GameClientRemoved += ClientDisconnected;
        }
        
        protected override void OnDispose()
        {
            Server.Listener.GameClientCreated -= ClientConnected;
            Server.Listener.GameClientRemoved += ClientDisconnected;
        }
        
        void ClientConnected(GameServerListener listener, GameClient client)
        {
            GameClientHandler handler = null;
            GameClientHandlers.FreezeWhile(clients => {
                                               if (clients.ContainsKey(client))
                                               {
                                                   if (log.IsErrorEnabled)
                                                       log.ErrorFormat("Trying to Register an existing GameClient : {0}", client);
                                                   
                                                   return;
                                               }
                                               
                                               handler = new GameClientHandler(client, Server);
                                               handler.StateChanged += EnforceClientUniqueAccountWhenIdentified;
                                               
                                               clients.Add(client, handler);
                                           });
            
            if (handler != null)
            {
                handler.Init();
                OnClientCreated(handler);
                
                if (log.IsInfoEnabled)
                    log.InfoFormat("New Client Connected with Version {0} - Revision {1}", client.Version, client.Revision);
            }
        }
        
        void ClientDisconnected(GameServerListener listener, GameClient client)
        {
            GameClientHandler handler;
            if (GameClientHandlers.TryRemove(client, out handler))
            {
                OnClientDisposed(handler);
                handler.StateChanged -= EnforceClientUniqueAccountWhenIdentified;
                handler.Dispose();
                
                if (log.IsInfoEnabled)
                    log.InfoFormat("Client {0} Disconnected", client);
            }
            else
            {
                if (log.IsErrorEnabled)
                    log.ErrorFormat("Tried to Remove an Unregistered Client : {0}", client);
            }
        }
        
        void EnforceClientUniqueAccountWhenIdentified(GameClientHandler client, eClientState state)
        {
            if (state != eClientState.Identified)
                return;
            
            GameClientHandlers.FreezeWhile(clients => {
                                               if (clients.Values.Any(handler => handler != client
                                                                      && handler.ClientState >= eClientState.Identified
                                                                      && handler.Account.Id == client.Account.Id))
                                                   client.Account.DeniedReason = eConnectionDeniedReason.AlreadyLoggedIn;
                                           });
        }
    }
}

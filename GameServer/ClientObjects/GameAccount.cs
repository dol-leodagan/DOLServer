﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Collections.Generic;

using DOL.Database.DataObjects;

using log4net;

namespace DOL.GS.ClientObjects
{
    /// <summary>
    /// Reason For Denying Connection to GameServer.
    /// </summary>
    public enum eConnectionDeniedReason
    {
        Undefined,
        Error,
        WrongLogin,
        WrongPassword,
        Banned,
        Throttled,
        InvalidCredentials,
        AlreadyLoggedIn,
        None,
    }
    
    /// <summary>
    /// GameAccount Hold the current state of a Client's Account.
    /// </summary>
    public class GameAccount
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        readonly object _AccountLock = new object();
        
        /// <summary>
        /// Data Object Primary Key for Reference.
        /// </summary>
        public int? Id { get; protected set; }
        
        /// <summary>
        /// Account Login.
        /// </summary>
        public string Login { get; set; }
        
        /// <summary>
        /// Account Password.
        /// </summary>
        public string Password { get; set; }
        
        /// <summary>
        /// Is Account Successfully Authenticated ?
        /// </summary>
        public bool isAuthenticated { get { return Id.HasValue; } }
        
        /// <summary>
        /// Is Account Allowed to Connect ?
        /// </summary>
        public bool isAllowedToConnect { get { return DeniedReason == eConnectionDeniedReason.None; } }
        
        /// <summary>
        /// Reason for not allowing Connection.
        /// </summary>
        public eConnectionDeniedReason DeniedReason { get; set; }
        
        /// <summary>
        /// Internal Characters Collection.
        /// </summary>
        protected Dictionary<string, Dictionary<int, GameCharacter>> Characters { get; set; }
        
        /// <summary>
        /// Load Account values from Database Object.
        /// </summary>
        /// <param name="account"></param>
        public void LoadFromDatabase(Account account)
        {
            lock (_AccountLock)
            {
                Id = account.AccountId;
                Login = account.Name;
                Password = account.Password;
                
                // Load Characters
                Characters.Clear();
                foreach (var character in account.Characters)
                {
                    Dictionary<int, GameCharacter> realmChars;
                    if (!Characters.TryGetValue(character.Realm, out realmChars))
                    {
                        realmChars = new Dictionary<int, GameCharacter>();
                        Characters.Add(character.Realm, realmChars);
                    }
                    
                    var gameCharacter = new GameCharacter(character.Name);
                    gameCharacter.LoadFromDatabase(character);
                    realmChars.Add(character.Slot, gameCharacter);
                }
            }
        }
        
        public GameCharacter GetCharacterByRealmSlot(string realm, int slot)
        {
            lock (_AccountLock)
            {
                Dictionary<int, GameCharacter> realmChars;
                if (!Characters.TryGetValue(realm, out realmChars))
                    return null;
                
                GameCharacter result;
                return realmChars.TryGetValue(slot, out result) ? result : null;
            }
        }
        
        public GameCharacter GetCharacterByName(string name)
        {
            lock (_AccountLock)
            {
                return Characters.SelectMany(characters => characters.Value)
                    .Select(character => character.Value)
                    .SingleOrDefault(chr => chr.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            }
        }
        
        public bool TryAddCharacter(GameCharacter character, int slot)
        {
            if (slot < 0)
                return false;
            
            lock (_AccountLock)
            {
                Dictionary<int, GameCharacter> realmChars;
                if (!Characters.TryGetValue(character.Realm, out realmChars))
                {
                    realmChars = new Dictionary<int, GameCharacter>();
                    Characters.Add(character.Realm, realmChars);
                }
                
                if (realmChars.Any(chr => chr.Value.Name.Equals(character.Name, StringComparison.OrdinalIgnoreCase)))
                    return false;
                
                try
                {
                    realmChars.Add(slot, character);
                }
                catch (Exception e)
                {
                    if (log.IsWarnEnabled)
                        log.WarnFormat("Error while creating Character {0}{1}{2}", character.Name, Environment.NewLine, e);
                    return false;
                }
                
                return true;
            }
        }
        
        public bool TryUpdateCharacter(GameCharacter character, int slot)
        {
            lock (_AccountLock)
            {
                Dictionary<int, GameCharacter> realmChars;
                
                if (!Characters.TryGetValue(character.Realm, out realmChars))
                    return false;
                
                GameCharacter gameCharacter;
                if (!realmChars.TryGetValue(slot, out gameCharacter))
                    return false;
                
                if (!gameCharacter.Name.Equals(character.Name, StringComparison.OrdinalIgnoreCase))
                    return false;
                
                gameCharacter.UpdateFrom(character);
                
                return true;
            }
        }
        
        public bool TryDeleteCharacter(string name)
        {
            lock (_AccountLock)
            {
                var toDelete = Characters.SelectMany(realms => realms.Value, (r, c) => new { Realm = r.Key, Char = c.Value, Slot = c.Key })
                    .SingleOrDefault(rc => rc.Char.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
                
                if (toDelete == null)
                    return false;
                
                try
                {
                    return Characters[toDelete.Realm].Remove(toDelete.Slot);
                }
                catch (Exception e)
                {
                    if (log.IsWarnEnabled)
                        log.WarnFormat("Error while Deleting Character {0}{1}{2}", name, Environment.NewLine, e);
                    return false;
                }
            }
        }
        
        /// <summary>
        /// Create a new Instance of <see cref="GameAccount"/>
        /// </summary>
        public GameAccount()
        {
            DeniedReason = eConnectionDeniedReason.Undefined;
            Characters = new Dictionary<string, Dictionary<int, GameCharacter>>();
        }
    }
}

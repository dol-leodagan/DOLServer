﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Collections.Generic;

using log4net;

using DOL.Network;
using DOL.Network.Packets;
using DOL.Network.OfficialPackets;
using DOL.Network.OfficialPackets.ClientPackets;
using DOL.Network.OfficialPackets.ServerPackets;

using DOL.GS.ClientObjects;

namespace DOL.GS
{
    /// <summary>
    /// Client State Flag.
    /// </summary>
    public enum eClientState
    {
        Disposed = -3,
        Disconnected = -2,
        LinkDead = -1,
        
        Instanced = 1,
        Connected = 2,
        Identified = 3,
        LoggedIn = 4,
        LoginScreen = 5,
        EnteringGame = 6,
        InGame = 7,
    }
    
    /// <summary>
    /// Game Client Handler Translate Game Server Events into Network Event and Trigger Event on Incoming Network Packets.
    /// </summary>
    public partial class GameClientHandler : DisposableComponent
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        /// <summary>
        /// The client addons Flag.
        /// </summary>
        public enum eClientAddons : byte
        {
            bit4 = 0x10,
            NewNewFrontiers = 0x20,
            Foundations = 0x40,
            NewFrontiers = 0x80,
        }

        /// <summary>
        /// The client software type Flag.
        /// </summary>
        public enum eClientType : byte
        {
            Unknown = 0,
            Classic = 1,
            ShroudedIsles = 2,
            TrialsOfAtlantis = 3,
            Catacombs = 4,
            DarknessRising = 5,
            LabyrinthOfTheMinotaur = 6,
        }

        protected readonly object _LockObject = new object();
        
        GameClient Client { get; set; }
        GameServer Server { get; set; }
        
        
        eClientAddons? Addons { get; set; }
        eClientType? Type { get; set; }
        
        readonly Func<IClientPacket, GameClient.PacketProtocol, bool>[] ClientFunctionRouter;
        
        protected virtual Dictionary<Type, Func<IClientPacket, GameClient.PacketProtocol, bool>> ClientFunctionRouting {
            get {
                return new Dictionary<Type, Func<IClientPacket, GameClient.PacketProtocol, bool>> {
                    { typeof(IFirstPacketAndCryptKey), OnFirstPacketAndCryptKey },
                    { typeof(ILoginRequest), OnLoginRequest },
                    { typeof(IPingRequest), OnPingRequest },
                    { typeof(ICharacterSelectRequest), OnCharacterSelectRequest },
                    { typeof(ICharacterOverviewRequest), OnCharacterOverviewRequest },
                    { typeof(IDisconnectPacket), OnDisconnect },
                    { typeof(ICharacterNameDuplicateRequest), OnCharacterNameDuplicateRequest },
                    { typeof(ICharacterNameValidRequest), OnCharacterNameValidRequest },
                    { typeof(ICharacterCreateRequest), OnCharacterCreateRequest },
                    
                    { typeof(ICharacterResetRequest), Ignore },
                    { typeof(ICharacterDeletionRequest), Ignore },
                    { typeof(ICharacterCreationConfirm), Ignore },
                };
            }
        }
        
        protected virtual Dictionary<Type, Action<int>> ServerFunctionRouting {
            get {
                return new Dictionary<Type, Action<int>> {
                    { typeof(IVersionAndCryptKey), code => VersionAndCryptKeyPacketCode = code },
                    { typeof(ILoginDenied), code => LoginDeniedPacketCode = code },
                    { typeof(ILoginGranted), code => LoginGrantedPacketCode = code },
                    { typeof(IPingReply), code => PingReplyPacketCode = code },
                    { typeof(ISetSessionId), code => SetSessionIdPacketCode = code },
                    { typeof(ISetRealm), code => SetRealmPacketCode = code },
                    { typeof(ICharacterOverview), code => CharacterOverviewPacketCode = code },
                    { typeof(ICharacterNameDuplicateReply), code => CharacterNameDuplicateReplyPacketCode = code },
                    { typeof(ICharacterNameValidReply), code => CharacterNameValidReplyPacketCode = code },
                };
            }
        }
        
        /// <summary>
        /// Create a new Instance of <see cref="GameClientHandler"/>
        /// </summary>
        /// <param name="Client">Network Client Object to Listen to.</param>
        /// <param name="Server">Game Server to Listen to.</param>
        public GameClientHandler(GameClient Client, GameServer Server)
        {
            ClientState = eClientState.Instanced;
            
            this.Client = Client;
            this.Server = Server;
            
            var clientRouter = Client.ClientPacketRouting;
            var serverRouter = Client.ServerPacketRouting;
            var clientRouting = ClientFunctionRouting;
            var serverRouting = ServerFunctionRouting;
            
            ClientFunctionRouter = new Func<IClientPacket, GameClient.PacketProtocol, bool>[clientRouter.Length];
            
            for (var c = 0 ; c < clientRouter.Length ; c++)
            {
                ClientFunctionRouter[c] = clientRouting.Where(kv => kv.Key.IsAssignableFrom(clientRouter[c])).Select(kv => kv.Value).SingleOrDefault();
                
                if (log.IsWarnEnabled && ClientFunctionRouter[c] == null && clientRouter[c] != null)
                    log.WarnFormat("Could not Register Client Packet Type '{0}', no implementation found...", clientRouter[c]);
            }
            
            for (var c = 0 ; c < serverRouter.Length ; c++)
            {
                var initRoute = serverRouting.Where(kv => kv.Key.IsAssignableFrom(serverRouter[c])).Select(kv => kv.Value).SingleOrDefault();
                
                if (initRoute != null)
                    initRoute(c);
                
                if (log.IsWarnEnabled && initRoute == null && serverRouter[c] != null)
                    log.WarnFormat("Could not Register Server Packet Type '{0}', no implementation found...", serverRouter[c]);
            }
        }
        
        protected override void OnInit()
        {
            Client.ReceivedDecodedPacket += OnReceived;
        }
        
        protected override void OnDispose()
        {
            Client.ReceivedDecodedPacket -= OnReceived;
            ClientState = eClientState.Disposed;
        }
        
        void OnReceived(GameClient client, int packetCode, IClientPacket packet, GameClient.PacketProtocol protocol)
        {
            var receiveHandler = ClientFunctionRouter[packetCode];
            
            if (log.IsDebugEnabled)
                log.DebugFormat("Handle Incoming Client Packet <{0}(0x{1:X2})> Procotol:{2}{3}{4}",
                                packet.GetType().Name, packetCode, protocol, Environment.NewLine, packet);
            
            if (receiveHandler == null)
            {
                if (log.IsDebugEnabled)
                    log.DebugFormat("No Function Handler found for Client Packet Type {0} - Client {1}", packet.GetType().Name, Client);
                return;
            }
            
            if (!receiveHandler(packet, protocol))
                if (log.IsWarnEnabled)
                    log.WarnFormat("Function Handler refused to Handle Client Packet Type {0} - Client {1}", packet.GetType().Name, Client);
        }
        
        eClientState _State;
        
        /// <summary>
        /// Client Current State
        /// </summary>
        public eClientState ClientState
        {
            get { return _State; }
            protected set { _State = value; OnStateChanged(value); }
        }
        
        /// <summary>
        /// Client State Changed.
        /// </summary>
        public event DOLEventHandler<GameClientHandler, eClientState> StateChanged;
        
        void OnStateChanged(eClientState state)
        {
            var handler = StateChanged;
            
            if (handler != null)
                handler(this, state);
        }

        GameAccount _Account;
        
        public GameAccount Account
        {
            get { return _Account; }
            protected set
            {
                lock (_LockObject)
                {
                    if (_Account != null)
                        throw new NotSupportedException("Can't Change GameClientHandler's Account after Initialization.");
                    
                    _Account = value;
                    OnAccountCreated(value);
                }
            }
        }
        
        /// <summary>
        /// Client Account Created
        /// </summary>
        public event DOLEventHandler<GameClientHandler, GameAccount> AccountCreated;
        
        void OnAccountCreated(GameAccount account)
        {
            var handler = AccountCreated;
            
            if (handler != null)
                handler(this, account);
        }

        uint _PingTimestamp;
        
        public DateTime LastPingReceived { get; protected set; }
        
        uint PingTimestamp {
            get { return _PingTimestamp; }
            set { LastPingReceived = DateTime.Now; _PingTimestamp = value; }
        }
        
        protected virtual bool Ignore(IClientPacket packet, GameClient.PacketProtocol protocol)
        {
            return true;
        }
    }
}

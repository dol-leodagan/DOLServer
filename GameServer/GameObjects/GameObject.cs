﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using log4net;

using DOL.GS.World;

namespace DOL.GS.GameObjects
{
    /// <summary>
    /// Base Class for Every Object in Regions.
    /// </summary>
    public abstract class GameObject
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Object State Flag.
        /// </summary>
        public enum eObjectState : byte
        {
            /// <summary>
            /// Active, visibly in world
            /// </summary>
            Active,
            /// <summary>
            /// Inactive, currently being moved or stuff
            /// </summary>
            Inactive,
            /// <summary>
            /// Deleted, waiting to be cleaned up
            /// </summary>
            Deleted
        }
        
        /// <summary>
        /// Current Object Id in Region.
        /// </summary>
        public uint? ObjectId { get; protected set; }
        
        /// <summary>
        /// Object Lock For Location.
        /// </summary>
        protected readonly object LocationLockObject = new object();
        
        /// <summary>
        /// Region Backend Field.
        /// </summary>
        protected Region _Region;
        
        /// <summary>
        /// Get Object Current Region.
        /// </summary>
        public Region Region
        {
            get
            {
                lock (LocationLockObject)
                    return _Region;
            }
        }
        
        /// <summary>
        /// Object State Backend Field.
        /// </summary>
        volatile eObjectState _ObjectState;
        
        /// <summary>
        /// Current Game Object State.
        /// </summary>
        public eObjectState ObjectState
        {
            get
            {
                return _ObjectState;
            }
            protected set
            {
                _ObjectState = value;
                OnStateChanged(value);
            }
        }
        
        RegionLocation _Location;
        /// <summary>
        /// Current Game Object Location.
        /// </summary>
        public virtual RegionLocation Location
        {
            get
            {
                lock (LocationLockObject)
                    return _Location;
            }
            protected set
            {
                lock (LocationLockObject)
                    _Location = value;
                OnLocationChanged(value);
            }
        }
        
        /// <summary>
        /// Game Object State Changed.
        /// </summary>
        public event DOLEventHandler<GameObject, eObjectState> StateChanged;
        
        void OnStateChanged(eObjectState state)
        {
            var handler = StateChanged;
            
            if (handler != null)
                handler(this, state);
        }
        
        /// <summary>
        /// Game Object Location Changed.
        /// </summary>
        public event DOLEventHandler<GameObject, RegionLocation> LocationChanged;
        
        void OnLocationChanged(RegionLocation location)
        {
            var handler = LocationChanged;
            
            if (handler != null)
                handler(this, location);
        }
        
        /// <summary>
        /// Create a new instance of <see cref="GameObject"/>
        /// </summary>
        protected GameObject()
        {
            _ObjectState = eObjectState.Inactive;
        }
        
        public virtual bool AddToWorld()
        {
            if (ObjectState == eObjectState.Active)
                return false;
            
            ObjectState = eObjectState.Active;
            
            lock (LocationLockObject)
            {
                try
                {
                    if (Region == null)
                        return false;
                    
                    ObjectId = Region.AddObject(this);
                }
                catch (Exception ex)
                {
                    if (log.IsErrorEnabled)
                        log.ErrorFormat("Could not add GameObject ({0}) to Region's World ({1}){2}{3}", this, Region, Environment.NewLine, ex);
                    return false;
                }
            }
            
            return true;
        }
        
        public virtual bool RemoveFromWorld()
        {
            if (ObjectState != eObjectState.Active)
                return false;
                
            ObjectState = eObjectState.Inactive;
            
            lock (LocationLockObject)
            {
                try
                {
                    if (Region == null)
                        return false;
                    
                    Region.RemoveObject(this);
                }
                catch (Exception ex)
                {
                    if (log.IsErrorEnabled)
                        log.ErrorFormat("Could not remove GameObject ({0}) from Region's World ({1}){2}{3}", this, Region, Environment.NewLine, ex);
                    return false;
                }
            }
            
            return true;
        }
        
        public void MoveTo(RegionLocation location)
        {
        }
    }
}

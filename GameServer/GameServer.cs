﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.IO;
using System.Linq;

using log4net;
using log4net.Config;

using DOL.Network;
using DOL.Utils;
using DOL.Database;

using DOL.GS.Schedulers;
using DOL.GS.Managers;
using DOL.GS.Managers.World;
using DOL.GS.Managers.Login;

namespace DOL.GS
{
    /// <summary>
    /// Game Server Main Class.
    /// </summary>
    public class GameServer
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        /// <summary>
        /// Server State Flag.
        /// </summary>
        public enum eServerState
        {
            Instanced,
            Starting,
            Started,
            Opened,
            Closed,
            Stopped,
        }
        
        /// <summary>
        /// This GameServer Configuration.
        /// </summary>
        public GameServerConfiguration Configuration { get; protected set; }
        
        /// <summary>
        /// Network Listener.
        /// </summary>
        public GameServerListener Listener { get; protected set; }
        
        /// <summary>
        /// Server Scheduler.
        /// </summary>
        public ServerScheduler ServerScheduler { get; protected set; }
        
        /// <summary>
        /// Database Connection.
        /// </summary>
        public IObjectDatabase Database { get; protected set; }
        
        /// <summary>
        /// Dynamic Script Manager.
        /// </summary>
        public Scripts.ScriptManager ScriptManager { get; protected set; }
        
        /// <summary>
        /// Networked Client Manager.
        /// </summary>
        public ClientManager ClientManager { get; protected set; }
        
        /// <summary>
        /// Client Login Manager.
        /// </summary>
        public LoginManager LoginManager { get; protected set; }
        
        /// <summary>
        /// World Manager.
        /// </summary>
        public WorldManager WorldManager { get; protected set; }
        
        /// <summary>
        /// Global Events Manager
        /// </summary>
        public GlobalEventManager GlobalEvents { get; protected set; }
        
        /// <summary>
        /// ServerProperties Manager
        /// </summary>
        public ServerPropertiesManager Properties { get; protected set; }
        
        /// <summary>
        /// Statistics Manager
        /// </summary>
        public StatisticsManager StatisticsManager { get; protected set; }
        
        /// <summary>
        /// Is this Server Started ?
        /// </summary>
        public bool Started { get; protected set; }
        
        eServerState _ServerState;
        /// <summary>
        /// Current Server State.
        /// </summary>
        public eServerState ServerState
        {
            get { return _ServerState; }
            protected set { _ServerState = value; OnStateChanged(value); }
        }
        
        /// <summary>
        /// Quit Event Handler when the Server is fully Stopped.
        /// </summary>
        public event DOLEventHandler<GameServer> Quit;
        
        void OnQuitEvent()
        {
            var handler = Quit;
            
            if (handler != null)
                handler(this);
        }
        
        /// <summary>
        /// State Change Event Handler.
        /// </summary>
        public event DOLEventHandler<GameServer, eServerState> StateChanged;
        
        void OnStateChanged(eServerState state)
        {
            var handler = StateChanged;
            
            if (handler != null)
                handler(this, state);
        }
        
        /// <summary>
        /// Create a new instance of <see cref="GameServer"/> using given Configuration.
        /// </summary>
        /// <param name="Configuration">Configuration for the created GameServer.</param>
        public GameServer(GameServerConfiguration Configuration)
        {
            ServerState = eServerState.Instanced;
            
            this.Configuration = Configuration;

            InitializeLog4Net();
            
            Listener = new GameServerListener(Configuration.TCPListenIP, Configuration.TCPListenPort, Configuration.UDPListenIP, Configuration.UDPListenPort, Configuration.MaxConnections);
            
            Database = new ObjectDatabase(Configuration.DatabaseType, Configuration.DatabaseConnectionString, Configuration.RealRootPath.FullName);
            
            ScriptManager = new Scripts.ScriptManager(Configuration.RealScriptsPath, Configuration.RealScriptsCompilationTargetPath, Configuration.AllScriptsCompilationAssemblies, Configuration.EnableScriptsCompilation, Configuration.RealAdditionalLibrariesPaths, Configuration.RealRootPath);
            
            ClientManager = new ClientManager(this);
            
            LoginManager = new LoginManager(this);
            
            WorldManager = new WorldManager(this);
            
            GlobalEvents = new GlobalEventManager(this);
            
            Properties = new ServerPropertiesManager(this);
            
            if (Configuration.DelayBetweenStatisticsDisplay > 0)
                StatisticsManager = new StatisticsManager(this);
        }
        
        /// <summary>
        /// Start the GameServer.
        /// </summary>
        /// <returns>True if everything went fine, False otherwise.</returns>
        public bool Start()
        {
            if (Started)
            {
                if (log.IsFatalEnabled)
                    log.FatalFormat("Tried to start GameServer '{0}' when already started!", this);
                
                return false;
            }
            
            ServerState = eServerState.Starting;
            
            Started = true;
            
            if (log.IsInfoEnabled)
                log.InfoFormat("Starting Server, Memory is {0:0.##}MB", Memory.GetCurrentUsedMemory());
            
            // Gather All Scripts before beginning
            if (!InitComponent(() => ScriptManager.Init(), "Dynamic Script Manager"))
                return false;
            
            // Init Global Event Manager
            if (!InitComponent(GlobalEvents, "Global Events Manager"))
                return false;
            
            // Init Database with available scripts assemblies
            if (!InitComponent(() => Database.Init(), "Database and Tables"))
                return false;
            
            if (!InitComponent(Properties, "Server Properties Manager"))
                return false;
            
            // Init Server Scheduler
            if (!InitComponent(() => ServerScheduler = new ServerScheduler(), "Server Timer-based Scheduler"))
                return false;
            
            // Once Database is loaded we can Start Init Scripts Event
            if (!InitComponent(() => ScriptManager.Start(this), "Dynamic Script Manager Init Scripts Event"))
                return false;
            
            // Init World Manager
            if (!InitComponent(WorldManager, "World Manager"))
                return false;
            
            // Init Game Clients Manager to handle Network Event
            if (!InitComponent(ClientManager, "Game Clients Manager"))
                return false;
            
            // Init Login Manager to handle Client Authentication
            if (!InitComponent(LoginManager, "Login Manager"))
                return false;
            
            // Start Network
            if (!InitComponent(() => Listener.Start(), "Server Network Listener"))
                return false;

            // TODO check components...
            
            ServerState = eServerState.Started;
            
            // Enable Statistics Manager if needed
            if (StatisticsManager != null)
            {
                if (!InitComponent(StatisticsManager, "Server Statistics Manager"))
                    return false;
            }
            
            ServerState = eServerState.Opened;
            
            return true;
        }
        
        /// <summary>
        /// Stop the GameServer.
        /// </summary>
        public void Stop()
        {
            if (!Started)
            {
                if (log.IsFatalEnabled)
                    log.FatalFormat("Tried to stop GameServer '{0}' when not started!", this);
                
                return;
            }
            
            ServerState = eServerState.Closed;
            
            Started = false;
            
            if (log.IsInfoEnabled)
                log.InfoFormat("Stopping Server, Memory is {0:0.##}MB", Memory.GetCurrentUsedMemory());

            // Stopping Network
            DisposeComponent(() => Listener.Stop(), "Server Network Listener");
            
            // Disable Components
            
            // Disable Statistics Manager if needed
            if (StatisticsManager != null)
                DisposeComponent(StatisticsManager, "Server Statistics Manager");
            
            // Disable World Manager
            DisposeComponent(WorldManager, "World Manager");
            
            // Disable Login Manager
            DisposeComponent(LoginManager, "Login Manager");
            
            // Disable Client Manager
            DisposeComponent(ClientManager, "Game Clients Manager");
            
            // Disable Scheduler
            DisposeComponent(() => { ServerScheduler.Shutdown(); ServerScheduler = null; }, "Server Timer-based Scheduler");
            
            // Disable Global Event Manager
            DisposeComponent(GlobalEvents, "Global Events Manager");
            
            // Disable Server Properties Manager
            DisposeComponent(Properties, "Server Properties Manager");
            
            if (log.IsInfoEnabled)
                log.InfoFormat("Server Stopped, sending Quit event, Memory is {0:0.##}MB", Memory.GetCurrentUsedMemory());

            OnQuitEvent();
            
            ServerState = eServerState.Stopped;
        }
        
        #region Initialization Helpers
        static bool InitComponent(DisposableComponent Component, string Description)
        {
            return InitComponent(() => Component.Init(), Description);
        }
        
        static bool InitComponent(Action ComponentInitMethod, string Description)
        {
            return InitComponent(() => { ComponentInitMethod(); return true; }, Description);
        }
        
        static bool InitComponent(Func<bool> ComponentInitFunc, string Description)
        {
            if (log.IsDebugEnabled)
                log.DebugFormat("Starting {0}, Memory is {1:0.##}MB", Description, Memory.GetCurrentUsedMemory());
            
            bool componentInitState = false;
            try
            {
                componentInitState = ComponentInitFunc();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.ErrorFormat("{0}: Error during Initialization{2}{1}", Description, ex, Environment.NewLine);
            }

            if (log.IsInfoEnabled)
                log.InfoFormat("{0} Started: {1}", Description, componentInitState);
            
            if (log.IsDebugEnabled)
                log.DebugFormat("Finished {0} Initialization, Memory is {1:0.##}MB", Description, Memory.GetCurrentUsedMemory());
            
            return componentInitState;
        }
        
        static bool DisposeComponent(IDisposable Component, string Description)
        {
            return DisposeComponent(() => Component.Dispose(), Description);
        }
        
        static bool DisposeComponent(Action ComponentDisposeMethod, string Description)
        {
            return DisposeComponent(() => { ComponentDisposeMethod(); return true; }, Description);
        }
        
        static bool DisposeComponent(Func<bool> ComponentDisposeFunc, string Description)
        {
            if (log.IsDebugEnabled)
                log.DebugFormat("Stopping {0}, Memory is {1:0.##}MB", Description, Memory.GetCurrentUsedMemory());
            
            bool componentStopState = false;
            try
            {
                componentStopState = ComponentDisposeFunc();
            }
            catch (Exception ex)
            {
                if (log.IsErrorEnabled)
                    log.ErrorFormat("{0}: Error during Dispose{2}{1}", Description, ex, Environment.NewLine);
            }

            if (log.IsInfoEnabled)
                log.InfoFormat("{0} Stopped: {1}", Description, componentStopState);
            
            if (log.IsDebugEnabled)
                log.DebugFormat("Finished {0} Dispose, Memory is {1:0.##}MB", Description, Memory.GetCurrentUsedMemory());
            
            return componentStopState;
        }
        #endregion
        
        #region Initialization
        /// <summary>
        /// Setup the Log4Net Manager according to GameServer Configuration
        /// </summary>
        void InitializeLog4Net()
        {
            var logConfigFile = Configuration.RealLogConfigPath;
            
            if (!logConfigFile.Exists && log.IsFatalEnabled)
                log.FatalFormat("Could not find Log Config File '{0}', Server will run without output log to file ! Check your server.yml Configuration!", logConfigFile.FullName);
            
            //Configure and watch the config file
            XmlConfigurator.ConfigureAndWatch(logConfigFile);
            
            if (log.IsInfoEnabled)
                log.InfoFormat("Log4Net Configured with File '{0}'", logConfigFile.FullName);
        }
        #endregion
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using YamlDotNet.Serialization;

using DOL.Database;

namespace DOL.GS
{
    /// <summary>
    /// GameServerConfiguration Hold Basic paramters to start a GameServer Instance.
    /// </summary>
    public class GameServerConfiguration
    {
        #region Server Paths
        public string RootPath { get; private set; }
        
        public string ServerName { get; private set; }
        
        public string ServerShortName { get; private set; }
        
        public int ServerId { get; private set; }
        #endregion
        
        #region Network Params
        public string TCPListenIP { get; private set; }
        
        public string UDPListenIP { get; private set; }
        
        public ushort TCPListenPort { get; private set; }
        
        public ushort UDPListenPort { get; private set; }
        
        public int MaxConnections { get; private set; }
        #endregion
        
        #region Database Config
        public eDatabaseType DatabaseType { get; private set; }
        
        public string DatabaseConnectionString { get; private set; }
        #endregion

        public string LogConfig { get; private set; }
        
        #region Scripts Config
        public string ScriptsPath { get; private set; }
        
        public string ScriptsCompilationTargetPath { get; private set; }
        
        public IEnumerable<string> ScriptsCompilationAssemblies { get; private set; }
        
        public bool EnableScriptsCompilation { get; private set; }
        
        public IEnumerable<string> AdditionalLibraries { get; private set; }
        #endregion
        
        #region Server Config
        public int DelayBetweenStatisticsDisplay { get; private set; }
        
        public int DelayBetweenWorldSave { get; private set; }
        
        public int RegionThreads { get; private set; }
        
        public IEnumerable<ushort> RegionOwnThread { get; private set; }
        #endregion
        
        [YamlIgnore]
        public DirectoryInfo RealRootPath
        {
            get
            {
                return Path.IsPathRooted(RootPath)
                    ? new DirectoryInfo(RootPath)
                    : new DirectoryInfo(Path.Combine(new FileInfo(Assembly.GetEntryAssembly().Location).DirectoryName, RootPath));
            }
        }
        
        [YamlIgnore]
        public FileInfo RealLogConfigPath { get { return GetRealPath(LogConfig); } }
        
        [YamlIgnore]
        public DirectoryInfo RealScriptsPath { get { return GetRealDirectoryPath(ScriptsPath); } }
        
        [YamlIgnore]
        public FileInfo RealScriptsCompilationTargetPath { get { return GetRealPath(ScriptsCompilationTargetPath); } }
        
        [YamlIgnore]
        public FileInfo[] AllScriptsCompilationAssemblies
        {
            get
            {
                return ScriptsCompilationAssemblies.Select(name => new FileInfo(name))
                    .Union(GetRealDirectoryPath("lib")
                           .EnumerateFiles("*", SearchOption.TopDirectoryOnly)
                           .Where(f => f.Extension.Equals(".dll", StringComparison.OrdinalIgnoreCase))
                           .Where(f => !f.Name.Equals(RealScriptsCompilationTargetPath.Name, StringComparison.OrdinalIgnoreCase)))
                    .ToArray();
            }
        }
        
        [YamlIgnore]
        public FileInfo[] RealAdditionalLibrariesPaths { get { return AdditionalLibraries.Select(path => GetRealPath(path)).ToArray(); } }
        
        /// <summary>
        /// Create a new Instance of <see cref="GameServerConfiguration"/>
        /// </summary>
        public GameServerConfiguration()
        {
            RootPath = ".";
            ServerName = "Dawn of Light Default Test Shard";
            ServerShortName = "DOLDEFTESTSHARD";
            ServerId = 1;
            
            TCPListenIP = "0.0.0.0";
            UDPListenIP = "0.0.0.0";
            TCPListenPort = 10300;
            UDPListenPort = 10400;
            MaxConnections = 500;
            
            DatabaseType = eDatabaseType.SQLite;
            DatabaseConnectionString = @"Data Source=dol3.sqlite3.db;Version=3;Pooling=True;Cache Size=1073741824;Journal Mode=WAL;Synchronous=Off;Foreign Keys=True;Default Timeout=60;";
            
            LogConfig = "config/logconfig.xml";
            
            ScriptsPath = "scripts";
            ScriptsCompilationTargetPath = "lib/GameServerScripts.dll";
            ScriptsCompilationAssemblies = new [] { "System.Core.dll", "System.dll", "System.ComponentModel.DataAnnotations.dll" };
            EnableScriptsCompilation = true;
            AdditionalLibraries = Enumerable.Empty<string>();
            
            DelayBetweenStatisticsDisplay = 600;
            DelayBetweenWorldSave = 600;
            
            RegionThreads = 4;
            // Default Own Thread : New Frontiers, Albion, Midgard, Hibernia
            RegionOwnThread = new [] { (ushort)163, (ushort)1, (ushort)100, (ushort)200 };
        }
        
        /// <summary>
        /// Get FileInfo from Path relative to Configuration Root Path.
        /// </summary>
        /// <param name="path">Path</param>
        /// <returns>FileInfo from Path Combined to Root Path.</returns>
        public FileInfo GetRealPath(string path)
        {
            return Path.IsPathRooted(path)
                ? new FileInfo(path)
                : new FileInfo(Path.Combine(RealRootPath.FullName, path));
        }
        
        /// <summary>
        /// Get DirectoryInfo from Path relative to Configuration Root Path.
        /// </summary>
        /// <param name="path">Path</param>
        /// <returns>DirectoryInfo from Path Combined to Root Path.</returns>
        public DirectoryInfo GetRealDirectoryPath(string path)
        {
            return Path.IsPathRooted(path)
                ? new DirectoryInfo(path)
                : new DirectoryInfo(Path.Combine(RealRootPath.FullName, path));
        }
        
        public void WriteConfigurationToFile(FileInfo file)
        {
            using (TextWriter writer = file.CreateText())
            {
                new SerializerBuilder().Build().Serialize(writer, new [] { this }.AsEnumerable());
            }
        }
        
        public static GameServerConfiguration CreateConfigurationFromFile(FileInfo file)
        {
            using (TextReader reader = file.OpenText())
            {
                return new DeserializerBuilder().IgnoreUnmatchedProperties().Build()
                    .Deserialize<IEnumerable<GameServerConfiguration>>(reader).First();
            }
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.GS.ClientObjects;

namespace DOL.GS.Managers
{
    /// <summary>
    /// Manager that broadcast Local Event at Server Level.
    /// </summary>
    public class GlobalEventManager : DisposableComponent
    {
        readonly GameServer Server;
        
        /// <summary>
        /// Create a new Instance of <see cref="GlobalEventManager"/>
        /// </summary>
        /// <param name="Server">Game Server to listen to.</param>
        public GlobalEventManager(GameServer Server)
        {
            this.Server = Server;
        }
        
        protected override void OnInit()
        {
            Server.ClientManager.ClientConnecting += OnClientConnecting;
            Server.ClientManager.ClientDispose += OnClientDispose;
        }
        
        protected override void OnDispose()
        {
            Server.ClientManager.ClientConnecting -= OnClientConnecting;
            Server.ClientManager.ClientDispose -= OnClientDispose;
        }
        
        void OnClientConnecting(ClientManager manager, GameClientHandler client)
        {
            client.AccountCreated += OnClientAccountCreated;
            client.StateChanged += OnClientStateChanged;
        }

        void OnClientDispose(ClientManager manager, GameClientHandler client)
        {
            client.AccountCreated -= OnClientAccountCreated;
            client.StateChanged -= OnClientStateChanged;
        }
        
        public event DOLEventHandler<GameServer, GameClientHandler, GameAccount> ClientAccountCreated;
        
        void OnClientAccountCreated(GameClientHandler client, GameAccount account)
        {
            var handler = ClientAccountCreated;
            if (handler != null)
                handler(Server, client, account);
        }
        
        public event DOLEventHandler<GameServer, GameClientHandler, eClientState> ClientStateChanged;
        
        void OnClientStateChanged(GameClientHandler client, eClientState state)
        {
            var handler = ClientStateChanged;
            if (handler != null)
                handler(Server, client, state);
        }
    }
}

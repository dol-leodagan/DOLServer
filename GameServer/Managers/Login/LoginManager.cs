﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;

using DOL.GS.ClientObjects;
using DOL.Database;
using DOL.Utils.Text;

using System.Data.Entity;

namespace DOL.GS.Managers.Login
{
    /// <summary>
    /// Login Manager Handles Game Account Authentication.
    /// </summary>
    public class LoginManager : DisposableComponent
    {
        GameServer Server;
        
        /// <summary>
        /// Creates a new instance of <see cref="LoginManager"/>
        /// </summary>
        /// <param name="Server">GameServer to listen to.</param>
        public LoginManager(GameServer Server)
        {
            this.Server = Server;
        }
        
        protected override void OnInit()
        {
            Server.GlobalEvents.ClientAccountCreated += OnAccountCreated;
        }
        
        protected override void OnDispose()
        {
            Server.GlobalEvents.ClientAccountCreated -= OnAccountCreated;
        }
        
        /// <summary>
        /// Authenticate the given GameAccount
        /// </summary>
        /// <param name="client">Client Handler.</param>
        /// <param name="account">GameAccount Object.</param>
        public virtual void Authenticate(GameClientHandler client, GameAccount account)
        {
            if (account.isAuthenticated
                || (account.DeniedReason != eConnectionDeniedReason.Undefined
                    && account.DeniedReason != eConnectionDeniedReason.InvalidCredentials))
                return;
            
            using (var db = Server.Database.GetContext<DOLDatabaseContext>())
            {
                var DbAccount = db.Accounts.SingleOrDefault(acc => acc.Name == account.Login);
                
                if (DbAccount != null)
                {
                    if (account.Password.EqualsPasswordHash(DbAccount.Password))
                    {
                        DbAccount = db.Accounts
                            .Include(acc => acc.Characters.Select(chr => chr.Inventory))
                            .SingleOrDefault(acc => acc.AccountId == DbAccount.AccountId);
                        
                        account.LoadFromDatabase(DbAccount);
                        account.DeniedReason = eConnectionDeniedReason.None;
                        return;
                    }
                    
                    account.DeniedReason = eConnectionDeniedReason.WrongPassword;
                }
                
                if (account.DeniedReason == eConnectionDeniedReason.Undefined)
                    account.DeniedReason = eConnectionDeniedReason.WrongLogin;
            }
        }

        void OnAccountCreated(GameServer sender, GameClientHandler client, GameAccount account)
        {
            Authenticate(client, account);
        }
        
        /// <summary>
        /// Check if Character Name is Allowed in this Server.
        /// </summary>
        /// <param name="name">Character Name to check.</param>
        /// <returns>True if Valid, False if Invalid.</returns>
        public bool IsValidCharacterName(string name)
        {
            if (name == null)
                return false;
            
            /* TODO */
            return true;
        }
        
        /// <summary>
        /// Check if Character Name already Exists in this Server.
        /// </summary>
        /// <param name="name">Character Name to check.</param>
        /// <returns>True if Exists, False otherwise.</returns>
        public bool IsDuplicatedCharacterName(string name)
        {
            if (name == null)
                return true;
            
            using (var db = Server.Database.GetContext<DOLDatabaseContext>())
            {
                return db.Characters.Any(character => character.Name == name);
            }
        }
    }
}

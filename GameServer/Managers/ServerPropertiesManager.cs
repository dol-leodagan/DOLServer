﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;

using DOL.GS.Scripts;
using DOL.Database;

namespace DOL.GS.Managers
{
    /// <summary>
    /// Manage Instances of Server Properties Containers.
    /// </summary>
    public sealed class ServerPropertiesManager : DisposableComponent
    {
        GameServer Server { get; set; }
        
        Dictionary<Type, AbstractServerProperties> ServerProperties { get; set; }
        
        IFormatProvider FormatProvider { get; set; }
        
        public ServerPropertiesManager(GameServer Server)
        {
            this.Server = Server;
            ServerProperties = new Dictionary<Type, AbstractServerProperties>();
            
            //we do this because we need "1.0" to be considered double sometimes its "1,0" in other countries
            var myCIintl = new CultureInfo("en-US", false);
            FormatProvider = myCIintl.NumberFormat;
        }
        
        protected override void OnInit()
        {
            ServerProperties = Server.ScriptManager.ScriptsAssemblies
                .GetAllNonAbstractClasses<AbstractServerProperties>()
                .Select(type => new { Type = type, Instance = (AbstractServerProperties)Activator.CreateInstance(type) })
                .ToDictionary(k => k.Type, v => v.Instance);
            
            using (var context = Server.Database.GetContext<DOLDatabaseContext>())
            {
                var allProperties = context.ServerProperties.ToArray();
                
                foreach (var propertyContainer in ServerProperties)
                {
                    var type = propertyContainer.Key;
                    var container = propertyContainer.Value;
                    
                    foreach (var propAttr in type
                             .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                             .Select(property => new { Property = property, Attribute = property.GetCustomAttribute<ServerPropertyAttribute>() })
                             .Where(property => property.Attribute != null))
                    {
                        var existing = allProperties.SingleOrDefault(prop => prop.KeyName.Equals(propAttr.Property.Name, StringComparison.OrdinalIgnoreCase));
                        
                        if (existing != null)
                        {
                            propAttr.Property.SetValue(container, Convert.ChangeType(existing.Value, propAttr.Attribute.Default.GetType(), FormatProvider));
                            
                            if (!string.IsNullOrEmpty(propAttr.Attribute.Description) && propAttr.Attribute.Description != existing.Description)
                                existing.Description = propAttr.Attribute.Description;
                            
                            if (propAttr.Attribute.Default != Convert.ChangeType(existing.DefaultValue, propAttr.Attribute.Default.GetType(), FormatProvider))
                                existing.DefaultValue = string.Format(FormatProvider, "{0}", propAttr.Attribute.Default);
                            
                            if (propAttr.Property.DeclaringType.Name.Replace("Properties", string.Empty) != existing.Category)
                                existing.Category = propAttr.Property.DeclaringType.Name.Replace("Properties", string.Empty);
                        }
                        else
                        {
                            propAttr.Property.SetValue(container, propAttr.Attribute.Default);
                            
                            var newProp = context.ServerProperties.Create();
                            newProp.KeyName = propAttr.Property.Name;
                            newProp.Category = propAttr.Property.DeclaringType.Name.Replace("Properties", string.Empty);
                            newProp.Description = propAttr.Attribute.Description;
                            newProp.DefaultValue = string.Format(FormatProvider, "{0}", propAttr.Attribute.Default);
                            newProp.Value = newProp.DefaultValue;
                            context.ServerProperties.Add(newProp);
                        }
                    }
                }
                
                context.SaveChanges();
            }
        }
        
        protected override void OnDispose()
        {
            using (var context = Server.Database.GetContext<DOLDatabaseContext>())
            {
                SaveChanges(context);
                context.SaveChanges();
            }
            
            ServerProperties.Clear();
        }
        
        public void SaveChanges(DOLDatabaseContext context)
        {
            var allProperties = context.ServerProperties.ToArray();
            
            foreach (var propertyContainer in ServerProperties)
            {
                var type = propertyContainer.Key;
                var container = propertyContainer.Value;
                
                foreach (var prop in type
                         .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                         .Where(property => property.GetCustomAttribute<ServerPropertyAttribute>() != null))
                {
                    var dataProp = allProperties.Single(dp => dp.KeyName.Equals(prop.Name));
                    dataProp.Value =  string.Format(FormatProvider, "{0}", prop.GetValue(container));
                }
            }
        }
        
        /// <summary>
        /// Retrieve Server Properties Container.
        /// </summary>
        /// <returns></returns>
        public T Get<T>() where T : AbstractServerProperties
        {
            return ServerProperties[typeof(T)] as T;
        }
    }
}

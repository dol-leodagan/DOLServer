﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Text;

using DOL.Network;
using DOL.Network.OfficialPackets;

using DOL.GS.Schedulers;

using log4net;

namespace DOL.GS.Managers
{
    /// <summary>
    /// Keep Tracks of Statistics during Server Runtime.
    /// </summary>
    public class StatisticsManager : DisposableComponent
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        GameServer Server;
        
        long LastRun;
        
        long InPacketCount;
        long BandwidthDown;
        
        long OutPacketCount;
        long BandwidthUp;
        
        long TimerTriggerCount;
        long TimerDurationTotal;
        
        readonly ConcurrentDictionary<GameTimer.TimeManager, long> TimeManagerTriggerCount = new ConcurrentDictionary<GameTimer.TimeManager, long>();
        
        double CpuPreviousTotalTime;
        
        /// <summary>
        /// Create a new Instance of <see cref="StatisticsManager"/>
        /// </summary>
        /// <param name="Server">Server to gather Statistics from.</param>
        public StatisticsManager(GameServer Server)
        {
            this.Server = Server;
        }
        
        protected override void OnInit()
        {
            Interlocked.Exchange(ref LastRun, ServerScheduler.Ticks);
            Server.ServerScheduler.Start(OnTick, Server.Configuration.DelayBetweenStatisticsDisplay * 1000);
            Server.Listener.GameClientCreated += OnClientConnected;
            Server.Listener.GameClientRemoved += OnClientRemoved;
            Server.ServerScheduler.TaskTriggered += OnSchedulerTriggered;
            Server.ServerScheduler.TaskFinished += OnSchedulerFinished;
            
            foreach (var timeManager in Server.WorldManager.TimeManagers)
            {
                TimeManagerTriggerCount.TryAdd(timeManager, 0);
                timeManager.GameTimerTriggered += OnTimeManagerTriggered;
            }
        }
        
        protected override void OnDispose()
        {
            Server.Listener.GameClientCreated -= OnClientConnected;
            Server.Listener.GameClientRemoved -= OnClientRemoved;
            Server.ServerScheduler.TaskTriggered -= OnSchedulerTriggered;
            Server.ServerScheduler.TaskFinished -= OnSchedulerFinished;
            
            foreach (var timeManager in TimeManagerTriggerCount.Keys)
                timeManager.GameTimerTriggered -= OnTimeManagerTriggered;
            
            TimeManagerTriggerCount.Clear();
        }
        
        /// <summary>
        /// Display Statistics each Timer Tick.
        /// </summary>
        /// <returns></returns>
        int OnTick()
        {
            if (Disposed)
                return 0;
            
            var lastRun = Interlocked.Exchange(ref LastRun, ServerScheduler.Ticks);
            var elapsed = Math.Max(1, ServerScheduler.Ticks - lastRun);
            
            // Network Stats
            var inPacketCount = Interlocked.Exchange(ref InPacketCount, 0);
            var bandwidthDown = Interlocked.Exchange(ref BandwidthDown, 0);
            
            var outPacketCount = Interlocked.Exchange(ref OutPacketCount, 0);
            var bandwidthUp = Interlocked.Exchange(ref BandwidthUp, 0);
            
            var clients = Server.ClientManager.ClientCount;
            
            var activeTimer = Server.ServerScheduler.RunningTimerCount;
            var totalTimer = Server.ServerScheduler.TimerCount;
            var rateTimerTrigger = Interlocked.Exchange(ref TimerTriggerCount, 0);
            var durationTimerTrigger = Interlocked.Exchange(ref TimerDurationTotal, 0);
            
            // List Time Managers Stats
            var timeManagerStats = new StringBuilder();
            
            foreach (var timeManager in TimeManagerTriggerCount.Keys)
            {
                long currentValue = 0;
                TimeManagerTriggerCount.AddOrUpdate(timeManager, 0, (key, value) => { currentValue = value; return 0; });
                timeManagerStats.AppendFormat(", {0}={1:0.##}t/s ({2})", timeManager.Name, currentValue * 1000.0 / elapsed, timeManager.ActiveTimers);
            }
            
            // System Stats Gathering
            var currentCpuTime = System.Diagnostics.Process.GetCurrentProcess().TotalProcessorTime.TotalMilliseconds;
            var systemStats = string.Format(", CPU:{0:0.##}%", (currentCpuTime - CpuPreviousTotalTime) / elapsed * 100);
            CpuPreviousTotalTime = currentCpuTime;
            
            log.InfoFormat("Mem={0:0.##}MB{11}, Clients={1}, Down={2:0.##}kb/s, Up={3:0.##}kb/s, In={4:0.##}pk/s, Out={5:0.##}pk/s, Scheduler={6}/{7} @ {8:0.##}t/s; {9:0.##}ms{10}",
                           DOL.Utils.Memory.GetCurrentUsedMemory(),
                           clients,
                           bandwidthDown * 1000.0 / 1024.0 / elapsed,
                           bandwidthUp * 1000.0 / 1024.0 / elapsed,
                           inPacketCount * 1000.0 / elapsed,
                           outPacketCount * 1000.0 / elapsed,
                           activeTimer,
                           totalTimer,
                           rateTimerTrigger * 1000.0 / elapsed,
                           durationTimerTrigger / Math.Max(1.0, rateTimerTrigger),
                           timeManagerStats,
                           systemStats);
            
            // Restart Timer
            return Initialized ? Server.Configuration.DelayBetweenStatisticsDisplay * 1000 : 0;
        }
        
        void OnClientConnected(GameServerListener listener, GameClient client)
        {
            client.ReceivedRawPacket += OnClientReceivedPacket;
            client.SentRawPacketTCP += OnClientSentPacketTCP;
            client.SentRawPacketUDP += OnClientSentPacketUDP;
        }
        
        void OnClientRemoved(GameServerListener listener, GameClient client)
        {
            client.ReceivedRawPacket -= OnClientReceivedPacket;
            client.SentRawPacketTCP -= OnClientSentPacketTCP;
            client.SentRawPacketUDP -= OnClientSentPacketUDP;
        }
        
        void OnClientReceivedPacket(GameClient client, OfficialClientPacketIn packet)
        {
            Interlocked.Increment(ref InPacketCount);
            Interlocked.Add(ref BandwidthDown, packet.Length);
        }
        
        void OnClientSentPacketTCP(GameClient client, OfficialClientTCPPacketOut packet)
        {
            Interlocked.Increment(ref OutPacketCount);
            Interlocked.Add(ref BandwidthUp, packet.Length);
        }

        void OnClientSentPacketUDP(GameClient client, OfficialClientUDPPacketOut packet)
        {
            Interlocked.Increment(ref OutPacketCount);
            Interlocked.Add(ref BandwidthUp, packet.Length);
        }
        
        void OnSchedulerTriggered(ServerScheduler scheduler, ScheduledTask task)
        {
            Interlocked.Increment(ref TimerTriggerCount);
        }
        
        void OnSchedulerFinished(ServerScheduler scheduler, ScheduledTask task, long elapsed)
        {
            Interlocked.Add(ref TimerDurationTotal, elapsed);
        }
        
        void OnTimeManagerTriggered(GameTimer.TimeManager manager, GameTimer timer)
        {
            TimeManagerTriggerCount.AddOrUpdate(manager, 1, (key, value) => value + 1);
        }
    }
}

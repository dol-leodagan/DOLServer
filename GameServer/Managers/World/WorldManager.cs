﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;

using DOL.GS.Schedulers;

namespace DOL.GS.Managers.World
{
    /// <summary>
    /// WorldManager is the entry point for World Objects and Handlers.
    /// </summary>
    public class WorldManager : DisposableComponent
    {
        GameServer Server;
        
        GameTimer.TimeManager[] _TimeManagers { get; set; }
        
        public IEnumerable<GameTimer.TimeManager> TimeManagers
        {
            get
            {
                return _TimeManagers.ToArray();
            }
        }
        
        /// <summary>
        /// Create a new Instance of <see cref="WorldManager"/>
        /// </summary>
        /// <param name="Server">Game Server to listen to.</param>
        public WorldManager(GameServer Server)
        {
            this.Server = Server;
            
            _TimeManagers = Enumerable.Range(0, Math.Max(1, Server.Configuration.RegionThreads))
                .Select(i => new GameTimer.TimeManager(string.Format("RegTimer{0}", i)))
                .Concat(Server.Configuration.RegionOwnThread
                        .Distinct()
                        .Select(i => new GameTimer.TimeManager(string.Format("OwnTimer{0}", i))))
                .ToArray();
        }
        
        protected override void OnInit()
        {
            foreach (var timeManager in _TimeManagers)
                timeManager.Start();
        }
        
        protected override void OnDispose()
        {
            foreach (var timeManager in _TimeManagers)
                timeManager.Stop();
            
            _TimeManagers = null;
            Server = null;
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network;
using DOL.Network.Packets;
using DOL.Network.OfficialPackets;
using DOL.Network.OfficialPackets.ClientPackets;

using DOL.GS.ClientObjects;

namespace DOL.GS
{
    /// <summary>
    /// Handler for Character Create Request.
    /// </summary>
    public partial class GameClientHandler
    {
        #region CharacterCreateRequest
        public event DOLEventHandler<GameClientHandler, ICharacterCreateRequest> CharacterCreateRequestReceived;
        
        void OnCharacterCreateRequestReceived(ICharacterCreateRequest packet)
        {
            var handler = CharacterCreateRequestReceived;
            if (handler != null)
                handler(this, packet);
        }
        
        protected virtual bool OnCharacterCreateRequest(IClientPacket packet, GameClient.PacketProtocol protocol)
        {
            if (ClientState != eClientState.LoginScreen)
                return false;
            
            var characterCreate = packet as ICharacterCreateRequest;
            
            if (characterCreate == null)
                return false;
            
            OnCharacterCreateRequestReceived(characterCreate);
            /** TODO - check character against server rules - **/
            /** TODO - Insert in database before creating to enforce unique constraint **/
            var changed = false;
            for (int count = 0 ; count < characterCreate.Characters.Count ; count++)
            {
                var character = characterCreate.Characters[count];
                
                if (character.Operation == eCreateOperation.Create
                    && Account.GetCharacterByName(character.CharacterName) == null)
                {
                    var gameCharacter = new GameCharacter(character.CharacterName)
                    {
                        Realm = character.RealmCode == 1 ? "Albion" : character.RealmCode == 2 ? "Midgard" : "Hibernia",
                    };
                    changed |= Account.TryAddCharacter(gameCharacter, count);
                }
                else if (character.Operation == eCreateOperation.Customize)
                {
                    var existingCharacter = Account.GetCharacterByName(character.CharacterName);
                    if (existingCharacter != null)
                    {
                        var gameCharacter = new GameCharacter(character.CharacterName)
                        {
                            Realm = character.RealmCode == 1 ? "Albion" : character.RealmCode == 2 ? "Midgard" : "Hibernia",
                        };
                        changed |= Account.TryUpdateCharacter(gameCharacter, count);
                    }
                }
                else if (character.Operation == eCreateOperation.Delete)
                {
                    changed |= Account.TryDeleteCharacter(character.CharacterName);
                }
            }
            
            if (changed)
            {
                /** TODO - Save to Database - **/
            }
            
            SendCharacterOverview(characterCreate.RealmRequest.ToString());

            return true;
        }
        #endregion
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network;
using DOL.Network.Packets;
using DOL.Network.OfficialPackets.ClientPackets;
using DOL.Network.OfficialPackets;

namespace DOL.GS
{
    /// <summary>
    /// Handler for Character Overview Request.
    /// </summary>
    public partial class GameClientHandler
    {
        #region CharacterOverviewRequest
        public event DOLEventHandler<GameClientHandler, ICharacterOverviewRequest> CharacterOverviewRequestReceived;
        
        void OnCharacterOverviewRequestReceived(ICharacterOverviewRequest packet)
        {
            var handler = CharacterOverviewRequestReceived;
            if (handler != null)
                handler(this, packet);
        }
        
        protected virtual bool OnCharacterOverviewRequest(IClientPacket packet, GameClient.PacketProtocol protocol)
        {
            if (ClientState < eClientState.LoggedIn)
                return false;
            
            var characterOverviewRequest = packet as ICharacterOverviewRequest;
            
            if (characterOverviewRequest == null)
                return false;
            
            OnCharacterOverviewRequestReceived(characterOverviewRequest);
            
            ClientState = eClientState.LoginScreen;
            
            if (characterOverviewRequest.RealmRequest == eRealm.None)
                SendSetRealm();
            else
                SendCharacterOverview(characterOverviewRequest.RealmRequest.ToString());
            
            return true;
        }
        #endregion
    }
}

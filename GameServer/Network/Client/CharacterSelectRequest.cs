﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network;
using DOL.Network.Packets;
using DOL.Network.OfficialPackets.ClientPackets;

namespace DOL.GS
{
    /// <summary>
    /// Handler for Character Select Request.
    /// </summary>
    public partial class GameClientHandler
    {
        #region CharacterSelectRequest
        public event DOLEventHandler<GameClientHandler, ICharacterSelectRequest> CharacterSelectRequestReceived;
        
        void OnCharacterSelectRequestReceived(ICharacterSelectRequest packet)
        {
            var handler = CharacterSelectRequestReceived;
            if (handler != null)
                handler(this, packet);
        }
        
        protected virtual bool OnCharacterSelectRequest(IClientPacket packet, GameClient.PacketProtocol protocol)
        {
            if (ClientState < eClientState.LoggedIn)
                return false;
            
            var characterSelectRequest = packet as ICharacterSelectRequest;
            
            if (characterSelectRequest == null)
                return false;
            
            OnCharacterSelectRequestReceived(characterSelectRequest);
            
            /* TODO */
            
            SendSetSessionId();
            
            return true;
        }
        #endregion
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network;
using DOL.Network.Packets;
using DOL.Network.OfficialPackets.ClientPackets;

using DOL.GS.ClientObjects;

namespace DOL.GS
{
    /// <summary>
    /// Handler for Login Request.
    /// </summary>
    public partial class GameClientHandler
    {
        #region LoginRequest
        public event DOLEventHandler<GameClientHandler, ILoginRequest> LoginRequestReceived;
        
        void OnLoginRequestReceived(ILoginRequest packet)
        {
            var handler = LoginRequestReceived;
            if (handler != null)
                handler(this, packet);
        }
        
        protected virtual bool OnLoginRequest(IClientPacket packet, GameClient.PacketProtocol protocol)
        {
            if (Account != null || ClientState != eClientState.Connected)
                return false;
            
            var loginRequest = packet as ILoginRequest;
            
            if (loginRequest == null)
                return false;
            
            log.InfoFormat("Client Authenticating with Login: {0} Password: Length {1}", loginRequest.AccountName, loginRequest.Password.Length);
            
            OnLoginRequestReceived(loginRequest);
            
            // Create base Account Object for Authentication
            Account = new GameAccount { Login = loginRequest.AccountName, Password = loginRequest.Password };
            
            if (Account.isAuthenticated)
                ClientState = eClientState.Identified;
            
            if (!Account.isAllowedToConnect)
            {
                ClientState = eClientState.Disconnected;
                SendLoginDenied();
            }
            else
            {
                ClientState = eClientState.LoggedIn;
                SendLoginGranted();
            }
            
            return true;
        }
        #endregion
    }
}

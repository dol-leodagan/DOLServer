﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network;
using DOL.Network.Packets;
using DOL.Network.OfficialPackets.ClientPackets;

namespace DOL.GS
{
    /// <summary>
    /// Handler for Ping Request.
    /// </summary>
    public partial class GameClientHandler
    {
        #region PingRequest
        public event DOLEventHandler<GameClientHandler, IPingRequest> PingRequestReceived;
        
        void OnPingRequestReceived(IPingRequest packet)
        {
            var handler = PingRequestReceived;
            if (handler != null)
                handler(this, packet);
        }
        
        protected virtual bool OnPingRequest(IClientPacket packet, GameClient.PacketProtocol protocol)
        {
            if (ClientState < eClientState.LoggedIn)
                return false;
            
            var pingRequest = packet as IPingRequest;
            
            if (pingRequest == null)
                return false;
            
            PingTimestamp = pingRequest.TimeStamp;
            
            OnPingRequestReceived(pingRequest);
            SendPingReply(pingRequest.TimeStamp, pingRequest.Sequence.HasValue ? unchecked((ushort)(pingRequest.Sequence.Value + 1)) : (ushort)0);
            
            return true;
        }
        #endregion
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.OfficialPackets.ServerPackets;

namespace DOL.GS
{
    /// <summary>
    /// Handler for Character Name Valid Reply.
    /// </summary>
    public partial class GameClientHandler
    {
        #region CharacterNameValidReply
        protected virtual int? CharacterNameValidReplyPacketCode { get; set; }
        
        protected virtual bool SendCharacterNameValidReply(string name, bool valid)
        {
            if (!CharacterNameValidReplyPacketCode.HasValue)
                return false;
            
            return Client.SendTCP<ICharacterNameValidReply>(
                CharacterNameValidReplyPacketCode.Value,
                packet => {
                    packet.AccountName = Account.Login;
                    packet.CharacterName = name;
                    packet.Valid = valid;
                });
        }
        #endregion
    }
}

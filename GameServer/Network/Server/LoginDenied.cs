﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using DOL.Network.OfficialPackets.ServerPackets;

using DOL.GS.ClientObjects;

namespace DOL.GS
{
    /// <summary>
    /// Handler for Login Denied.
    /// </summary>
    public partial class GameClientHandler
    {
        #region LoginDenied
        protected virtual int? LoginDeniedPacketCode { get; set; }
        
        protected virtual bool SendLoginDenied()
        {
            if (!LoginDeniedPacketCode.HasValue)
                return false;
            
            return Client.SendTCP<ILoginDenied>(
                LoginDeniedPacketCode.Value,
                packet => {
                    if (Type.HasValue) packet.ClientType = (byte)Type.Value;
                    if (Addons.HasValue) packet.ClientAddons = (byte)Addons.Value;
                    packet.ClientVersion = Client.Version;
                    packet.ClientRevision = Client.Revision;
                    switch (Account.DeniedReason)
                    {
                        case eConnectionDeniedReason.WrongPassword:
                            packet.Reason = LoginDeniedType.WrongPassword;
                            break;
                        case eConnectionDeniedReason.WrongLogin:
                            packet.Reason = LoginDeniedType.AccountNotFound;
                            break;
                        case eConnectionDeniedReason.Throttled:
                            packet.Reason = LoginDeniedType.AuthorizationServerUnavailable;
                            break;
                        case eConnectionDeniedReason.InvalidCredentials:
                            packet.Reason = LoginDeniedType.AccountInvalid;
                            break;
                        case eConnectionDeniedReason.Error:
                            packet.Reason = LoginDeniedType.CannotAccessUserAccount;
                            break;
                        case eConnectionDeniedReason.AlreadyLoggedIn:
                            packet.Reason = LoginDeniedType.AccountAlreadyLoggedIn;
                            break;
                        case eConnectionDeniedReason.Banned:
                            packet.Reason = LoginDeniedType.AccountIsBannedFromThisServerType;
                            break;
                        default:
                            packet.Reason = LoginDeniedType.ServiceNotAvailable;
                            break;
                    }
                });
        }
        #endregion
    }
}

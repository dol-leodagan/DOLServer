﻿//#define MonitorCallbacks
//#define CollectStatistic
/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;

using log4net;

using ThreadState = System.Threading.ThreadState;

namespace DOL.GS.Schedulers
{
    /// <summary>
    /// The GameTimer class invokes OnTick() method after
    /// certain intervals which are defined in milliseconds
    /// </summary>
    public abstract class GameTimer
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Milliseconds Frequency Constant
        /// </summary>
        static readonly long StopwatchFrequencyMilliseconds = Stopwatch.Frequency / 1000;
        /// <summary>
        /// Retrieve Current Timestamp used by TimerManager
        /// </summary>
        public static long Ticks { get { return Stopwatch.GetTimestamp() / StopwatchFrequencyMilliseconds; } }

        /// <summary>
        /// Stores the reference to the next timer in the chain
        /// </summary>
        GameTimer m_nextTimer;
        /// <summary>
        /// Stores the next execution tick and flags
        /// </summary>
        long m_tick = TIMER_DISABLED;
        /// <summary>
        /// Stores the timer intervals
        /// </summary>
        int m_interval;
        /// <summary>
        /// Stores the time where the timer was inserted
        /// </summary>
        long m_targetTime = -1;
        /// <summary>
        /// Stores the time manager used for this timer
        /// </summary>
        readonly TimeManager m_time;

        /// <summary>
        /// Flags the timer as disabled
        /// </summary>
        public const long TIMER_DISABLED = long.MinValue;
        /// <summary>
        /// Flags the current tick timers as rescheduled
        /// </summary>
        public const long TIMER_RESCHEDULED = 0x40000000;

        /// <summary>
        /// Constructs a new GameTimer
        /// </summary>
        /// <param name="time">The time manager for this timer</param>
        protected GameTimer(TimeManager time)
        {
            if (time == null)
                throw new ArgumentNullException("time");
            m_time = time;
        }

        /// <summary>
        /// Gets the time left until this timer fires, in milliseconds.
        /// </summary>
        public long TimeUntilElapsed
        {
            get
            {
                long ins = m_targetTime;
                return ins < 0 ? -1 : ins - m_time.CurrentTime;
            }
        }

        /// <summary>
        /// Gets or sets the timer intervals in milliseconds
        /// </summary>
        public virtual int Interval
        {
            get
            {
                return m_interval;
            }
            set
            {
                if (value < 0 || value > TimeManager.MaxInterval)
                    throw new ArgumentOutOfRangeException("value", value, string.Format("Interval value must be in 1 .. {0} range.", TimeManager.MaxInterval));
                m_interval = value;
            }
        }

        /// <summary>
        /// Checks whether this timer is disabled
        /// </summary>
        public bool IsAlive { get { return (m_tick & TIMER_DISABLED) == 0; } }

        /// <summary>
        /// Returns short information about the timer
        /// </summary>
        /// <returns>Short info about the timer</returns>
        public override string ToString()
        {
            return string.Format("{0} tick:0x{1:X8} interval:{2} manager:'{3}'", GetType().FullName, m_tick, m_interval, m_time != null ? m_time.Name : "null");
        }

        /// <summary>
        /// Starts the timer with defined initial delay
        /// </summary>
        /// <param name="initialDelay">The initial timer delay. Must be more than 0 and less than MaxInterval</param>
        public virtual void Start(int initialDelay)
        {
            m_time.InsertTimer(this, initialDelay);
        }

        /// <summary>
        /// Stops the timer
        /// </summary>
        public virtual void Stop()
        {
            m_time.RemoveTimer(this);
        }

        /// <summary>
        /// Called on every timer tick
        /// </summary>
        protected abstract void OnTick();
        
        #region TimeManager
        /// <summary>
        /// This class manages all the GameTimers. It is started from
        /// within the GameServer.Start() method and stopped from
        /// within the GameServer.Stop() method. It runs an own thread
        /// when it is started, that cylces through all GameTimers and
        /// executes them at the right moment.
        /// </summary>
        public sealed class TimeManager
        {
            /// <summary>
            /// Time Manager Lock Object
            /// </summary>
            private readonly object m_lockObject = new object();
            
            /// <summary>
            /// Defines a logger for this class.
            /// </summary>
            private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

            /// <summary>
            /// The size of cache array with 1ms granularity, in bits
            /// Must be more than or equal to one bucket size
            /// </summary>
            public const int CACHE_BITS = 14;
            /// <summary>
            /// The bucket size in bits. All timers with same high bits get into same bucket.
            /// </summary>
            public const int BUCKET_BITS = 4;
            /// <summary>
            /// The table arrays size in bits.
            /// Table size in milliseconds is (1 less than less than BUCKET_BITS + TABLE_BITS).
            /// </summary>
            public const int TABLE_BITS = 17;
            /// <summary>
            /// Defines amount of bits that don't fit the fixed time table.
            /// Two highest bits are reserved for flags.
            /// </summary>
            public const int LONGTERM_BITS = 30 - BUCKET_BITS - TABLE_BITS;

            /// <summary>
            /// Defines the bitmask used to get timer cache bucket index
            /// </summary>
            public const int CACHE_MASK = (1 << CACHE_BITS) - 1;
            /// <summary>
            /// Defines the bitmask used for a check if new bucket should be sorted
            /// </summary>
            public const int BUCKET_MASK = (1 << BUCKET_BITS) - 1;
            /// <summary>
            /// Defines the bitmask used to get timer bucket index
            /// (after shifting it by BUCKET_BITS!)
            /// </summary>
            public const int TABLE_MASK = (1 << TABLE_BITS) - 1;
            /// <summary>
            /// Defines the bitmask used to check if timer should be delayed for another table loop
            /// </summary>
            public const int LONGTERM_MASK = ((1 << LONGTERM_BITS) - 1) << BUCKET_BITS + TABLE_BITS;
            /// <summary>
            /// Defines the bitmask used to "overflow" the current millisecond
            /// so that time table starts from 0 again.
            /// </summary>
            public const int TICK_MASK = (1 << TABLE_BITS + BUCKET_BITS) - 1;

            /// <summary>
            /// The time thread
            /// </summary>
            Thread m_timeThread;
            /// <summary>
            /// The time thread name
            /// </summary>
            readonly string m_name;
            /// <summary>
            /// The timer is running while this flag is true
            /// </summary>
            volatile bool m_running;
            /// <summary>
            /// The current virtual millisecond, overflows when it reach the time table end
            /// </summary>
            int m_tick;
            /// <summary>
            /// The current manager time in milliseconds
            /// </summary>
            long m_time;
            /// <summary>
            /// The cached bucket with 1ms granularity.
            /// All intervals that fit this array don't need sorting.
            /// </summary>
            readonly CacheBucket[] m_cachedBucket = new CacheBucket[1 << CACHE_BITS];
            /// <summary>
            /// Stores all timers. Array index = (TimerTick>>BUCKET_BITS)&TABLE_MASK
            /// </summary>
            readonly GameTimer[] m_buckets = new GameTimer[1 << TABLE_BITS];
            /// <summary>
            /// The count of active timers in the manager
            /// </summary>
            int m_activeTimers;

            /// <summary>
            /// Holds the first and the last timers in the chain
            /// </summary>
            private struct CacheBucket
            {
                /// <summary>
                /// The first timer in the chain
                /// </summary>
                public GameTimer FirstTimer;
                /// <summary>
                /// The last timer in the chain
                /// </summary>
                public GameTimer LastTimer;
                /// <summary>
                /// The empty bucket
                /// </summary>
                public static readonly CacheBucket EmptyBucket = new CacheBucket();
            }
            
            /// <summary>
            /// Event triggered when a GameTimer is about to Run.
            /// </summary>
            public event DOLEventHandler<TimeManager, GameTimer> GameTimerTriggered;
            
            /// <summary>
            /// Trigger the GameTimer Triggered Event.
            /// </summary>
            /// <param name="task"></param>
            void OnGameTimerTriggered(GameTimer task)
            {
                var handler = GameTimerTriggered;
                
                if (handler != null)
                    handler(this, task);
            }
            
            /// <summary>
            /// Event triggered when a GameTimer finished its execution.
            /// </summary>
            public event DOLEventHandler<TimeManager, GameTimer, long> GameTimerFinished;

            /// <summary>
            /// Trigger the GameTimer Finished Event.
            /// </summary>
            /// <param name="task"></param>
            /// <param name="elapsed"></param>
            void OnGameTimerFinished(GameTimer task, long elapsed)
            {
                var handler = GameTimerFinished;
                
                if (handler != null)
                    handler(this, task, elapsed);
            }

            /// <summary>
            /// Constructs a new time manager
            /// </summary>
            /// <param name="name">Thread name</param>
            public TimeManager(string name)
            {
                if (name == null)
                    throw new ArgumentNullException("name");
                m_name = name;
            }

            /// <summary>
            /// Gets the maximal allowed interval
            /// </summary>
            public readonly static int MaxInterval = (1 << LONGTERM_BITS + TABLE_BITS + BUCKET_BITS) - (1 << TABLE_BITS + BUCKET_BITS);

            /// <summary>
            /// Gets the current virtual millisecond which is reset after TICK_MASK ticks
            /// </summary>
            internal int CurrentTick
            {
                get { return m_tick; }
                set { m_tick = value; }
            }

            /// <summary>
            /// Gets the current manager time in milliseconds
            /// </summary>
            public long CurrentTime
            {
                get { return m_time; }
                set { m_time = value; }
            }

            /// <summary>
            /// True if manager is active
            /// </summary>
            public bool Running
            {
                get { return m_running; }
            }

            /// <summary>
            /// Gets the manager name
            /// </summary>
            public string Name
            {
                get { return m_name; }
            }

            /// <summary>
            /// Gets the current count of active timers
            /// </summary>
            public int ActiveTimers
            {
                get { return m_activeTimers; }
            }

            /// <summary>
            /// Returns short description of the time manager
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return string.Format("Timer Manager:'{0}' running:{1} currentTime:{2}", m_name, m_running, m_time);
            }

            /// <summary>
            /// Starts the time manager if not started already
            /// </summary>
            /// <returns>success</returns>
            public bool Start()
            {
                lock (m_lockObject)
                {
                    if (m_timeThread != null)
                        return false;
    
                    m_running = true;
                    m_timeThread = new Thread(new ThreadStart(TimeThread));
                    m_timeThread.Name = m_name;
                    m_timeThread.Priority = ThreadPriority.AboveNormal;
                    m_timeThread.IsBackground = true;
                    m_timeThread.Start();
                    return true;
                }
            }

            /// <summary>
            /// Stops the time manager if not stopped already
            /// </summary>
            /// <returns>success</returns>
            public bool Stop()
            {
                if (m_timeThread == null)
                    return false;

                lock (m_lockObject)
                {
                    m_running = false;

                    if (!m_timeThread.Join(10000))
                    {
                        try
                        {
                            if (log.IsErrorEnabled)
                            {
                                ThreadState state = m_timeThread.ThreadState;
                                log.ErrorFormat("failed to stop the Timer Thread ({0}) in 10 seconds (thread state={1}){2}", m_name, state, Environment.NewLine);
                            }
                        }
                        finally
                        {
                            m_timeThread.Abort();
                            try
                            {
                                while(!m_timeThread.Join(2000))
                                {
                                    log.ErrorFormat("Timer Thread ({0}) can't stop after abort... maybe remaining threads going... trying again !", m_name);
                                
                                    try
                                    {
                                        m_timeThread.Abort();
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                    
                    m_timeThread = null;

                    Array.Clear(m_buckets, 0, m_buckets.Length);
                    Array.Clear(m_cachedBucket, 0, m_cachedBucket.Length);
                    
                    return true;
                }
            }

            /// <summary>
            /// Inserts the timer into the table.
            /// </summary>
            /// <param name="t">The timer to insert</param>
            /// <param name="offsetTick">The offset from current tick. min value=1, max value&lt;MaxInterval</param>
            internal void InsertTimer(GameTimer t, int offsetTick)
            {
                if (offsetTick > MaxInterval || offsetTick < 1)
                    throw new ArgumentOutOfRangeException("offsetTick", offsetTick, string.Format("Offset must be in range from 1 to {0}", MaxInterval));

                GameTimer timer = t;

                lock (m_buckets)
                {
                    long timerTick = timer.m_tick;
                    long targetTick = m_tick + offsetTick;
                    
                    if (timerTick == m_tick || (timerTick & TIMER_RESCHEDULED) != 0)
                    {
                        timer.m_targetTime = (int) (CurrentTime + offsetTick);
                        timer.m_tick = targetTick | TIMER_RESCHEDULED;
                        return;
                    }

                    if ((timerTick & TIMER_DISABLED) == 0)
                    {
                        RemoveTimerUnsafe(timer);
                    }

                    timer.m_targetTime = (int) (CurrentTime + offsetTick);
                    m_activeTimers++;

                    if (offsetTick <= CACHE_MASK + 1)
                    {
                        timer.m_tick = targetTick & TICK_MASK;
                        targetTick &= CACHE_MASK;
                        CacheBucket bucket = m_cachedBucket[targetTick];
                        GameTimer prev = bucket.LastTimer;
                        if (prev != null)
                        {
                            prev.m_nextTimer = timer;
                            m_cachedBucket[targetTick].LastTimer = timer;
                        }
                        else
                        {
                            bucket.FirstTimer = timer;
                            bucket.LastTimer = timer;
                            m_cachedBucket[targetTick] = bucket;
                        }
                    }
                    else
                    {
                        if ((targetTick & TICK_MASK) > (m_tick & ~BUCKET_MASK) + BUCKET_MASK)
                            targetTick += TICK_MASK + 1; // extra pass if the timer is ahead of current tick
                        timer.m_tick = targetTick;
                        targetTick = (targetTick >> BUCKET_BITS) & TABLE_MASK;
                        GameTimer next = m_buckets[targetTick];
                        m_buckets[targetTick] = timer;
                        if (next != null)
                        {
                            timer.m_nextTimer = next;
                        }
                    }
                }
            }

            /// <summary>
            /// Removes the timer from the table.
            /// </summary>
            /// <param name="timer">The timer to remove</param>
            internal void RemoveTimer(GameTimer timer)
            {
                lock (m_buckets)
                {
                    RemoveTimerUnsafe(timer);
                }
            }

            /// <summary>
            /// Removes the timer from the table without locking the table
            /// </summary>
            /// <param name="timer">The timer to remove</param>
            private void RemoveTimerUnsafe(GameTimer timer)
            {
                GameTimer t = timer;
                long tick = t.m_tick;
                if ((tick & TIMER_DISABLED) != 0)
                    return;

                timer.m_targetTime = -1;
                // never change the active chain
                if (tick == m_tick || (tick & TIMER_RESCHEDULED) != 0)
                {
                    t.m_tick = TIMER_DISABLED | TIMER_RESCHEDULED;
                    return;
                }

                m_activeTimers--;

                // check the cache first
                long cachedIndex = tick & CACHE_MASK;
                CacheBucket bucket = m_cachedBucket[cachedIndex];
                if (bucket.FirstTimer == t)
                {
                    t.m_tick = TIMER_DISABLED;
                    bucket.FirstTimer = t.m_nextTimer;
                    if (bucket.LastTimer == t)
                        bucket.LastTimer = t.m_nextTimer;
                    t.m_nextTimer = null;
                    m_cachedBucket[cachedIndex] = bucket;
                    return;
                }

                GameTimer timerChain = bucket.FirstTimer;
                GameTimer prev;
                while (timerChain != null)
                {
                    prev = timerChain;
                    timerChain = timerChain.m_nextTimer;
                    if (timerChain == t)
                    {
                        prev.m_nextTimer = t.m_nextTimer;
                        t.m_nextTimer = null;
                        t.m_tick = TIMER_DISABLED;
                        if (bucket.LastTimer == t)
                        {
                            bucket.LastTimer = prev;
                            m_cachedBucket[cachedIndex] = bucket;
                        }
                        return;
                    }
                }

                // check the buckets
                tick = (tick >> BUCKET_BITS) & TABLE_MASK;
                timerChain = m_buckets[tick];
                if (timerChain == t)
                {
                    timerChain = timerChain.m_nextTimer;
                    m_buckets[tick] = timerChain;
                    t.m_nextTimer = null;
                    t.m_tick = TIMER_DISABLED;
                    return;
                }

                while (timerChain != null)
                {
                    prev = timerChain;
                    timerChain = timerChain.m_nextTimer;
                    if (timerChain == t)
                    {
                        prev.m_nextTimer = t.m_nextTimer;
                        break;
                    }
                }
                t.m_nextTimer = null;
                t.m_tick = TIMER_DISABLED;
            }

            /// <summary>
            /// The time thread loop
            /// </summary>
            private void TimeThread()
            {
                log.InfoFormat("Started Timer Manager thread {0} (ID:{1})", m_name, Thread.CurrentThread.ManagedThreadId);

                int timeBalance = 0;
                uint workStart, workEnd;
                GameTimer chain, next, bucketTimer;

                workStart = workEnd = (uint)Ticks;

                while (m_running)
                {
                    try
                    {
                        // fire timers
                        lock (m_buckets)
                        {
                            m_time++;
                            
                            int newTick = m_tick = (m_tick + 1) & TICK_MASK;
                            if ((newTick & BUCKET_MASK) == 0)
                            {
                                // cache next bucket
                                int index = newTick >> BUCKET_BITS;
                                next = m_buckets[index];
                                if (next != null)
                                {
                                    m_buckets[index] = null;
                                    // sort the new cached bucket
                                    do
                                    {
                                        GameTimer timer = next;
                                        next = next.m_nextTimer;
                                        long index2 = timer.m_tick;
                                        if ((index2 & LONGTERM_MASK) != 0
                                            && ((index2 -= (1 << TABLE_BITS + BUCKET_BITS)) & LONGTERM_MASK) != 0)
                                        {
                                            // reinsert longterm timers back
                                            timer.m_tick = index2;
                                            bucketTimer = m_buckets[index];
                                            m_buckets[index] = timer;
                                        }
                                        else
                                        {
                                            timer.m_tick = index2;
                                            index2 &= CACHE_MASK;
                                            bucketTimer = m_cachedBucket[index2].FirstTimer;
                                            m_cachedBucket[index2].FirstTimer = timer;
                                            if (m_cachedBucket[index2].LastTimer == null)
                                                m_cachedBucket[index2].LastTimer = timer;
                                        }

                                        if (bucketTimer == null)
                                        {
                                            timer.m_nextTimer = null;
                                        }
                                        else
                                        {
                                            timer.m_nextTimer = bucketTimer;
                                        }
                                    }
                                    while (next != null);
                                }
                            }

                            int cacheIndex = m_tick & CACHE_MASK;
                            chain = m_cachedBucket[cacheIndex].FirstTimer;
                            if (chain != null)
                            {
                                m_cachedBucket[cacheIndex] = CacheBucket.EmptyBucket;
                            }
                        }

                        GameTimer current = chain;
                        int curTick = m_tick;
                        int currentBucketMax = (curTick & ~BUCKET_MASK) + BUCKET_MASK;
                        
                        while (current != null)
                        {
                            if (current.m_tick == curTick || (current.m_tick & TIMER_RESCHEDULED) == 0)
                            {
                                try
                                {
                                    OnGameTimerTriggered(current);

                                    long callbackStart = Ticks;

                                    current.OnTick();
                                    
                                    var elapsed = Ticks - callbackStart;
                                    
                                    OnGameTimerFinished(current, elapsed);

                                    if (log.IsWarnEnabled && elapsed > 250)
                                        log.WarnFormat("GameTimer Callback took {0}ms! {1}", elapsed, current);
                                }
                                catch (Exception e)
                                {
                                    if (log.IsErrorEnabled)
                                        log.ErrorFormat("Error in GameTimer Callback ({0}){1}{2}", current, Environment.NewLine, e);
                                    current.m_tick = TIMER_DISABLED | TIMER_RESCHEDULED;
                                }
                            }

                            lock (m_buckets)
                            {
                                next = current.m_nextTimer;
                                long tick = current.m_tick;
                                long interval = current.m_interval;

                                if ((tick & TIMER_DISABLED) != 0 || (interval == 0 && (tick & TIMER_RESCHEDULED) == 0))
                                {
                                    m_activeTimers--;
                                    current.m_nextTimer = null;
                                    current.m_tick = TIMER_DISABLED;
                                    current.m_targetTime = -1;
                                }
                                else
                                {
                                    // REINSERT all including rescheduled timers
                                    if ((tick & TIMER_RESCHEDULED) != 0)
                                    {
                                        current.m_tick = tick &= ~TIMER_RESCHEDULED;
                                    }
                                    else
                                    {
                                        current.m_targetTime = (int) (CurrentTime + interval);
                                        current.m_tick = tick = curTick + interval;
                                    }

                                    if (tick - curTick <= CACHE_MASK + 1)
                                    {
                                        tick &= CACHE_MASK;
                                        current.m_tick &= TICK_MASK;
                                        CacheBucket bucket = m_cachedBucket[tick];
                                        GameTimer prev = bucket.LastTimer;
                                        current.m_nextTimer = null;
                                        if (prev != null)
                                        {
                                            prev.m_nextTimer = current;
                                            bucket.LastTimer = current;
                                        }
                                        else
                                        {
                                            bucket.FirstTimer = current;
                                            bucket.LastTimer = current;
                                        }
                                        m_cachedBucket[tick] = bucket;
                                    }
                                    else
                                    {
                                        if ((tick & TICK_MASK) > currentBucketMax)
                                            current.m_tick = tick += TICK_MASK + 1; // extra pass if the timer is ahead of current tick
                                        tick = (tick >> BUCKET_BITS) & TABLE_MASK;
                                        bucketTimer = m_buckets[tick];
                                        if (bucketTimer == null)
                                        {
                                            current.m_nextTimer = null;
                                        }
                                        else
                                        {
                                            current.m_nextTimer = bucketTimer;
                                        }
                                        m_buckets[tick] = current;
                                    }
                                }
                            }
                            current = next;
                        }

                        bucketTimer = null;

                        workEnd = (uint)Ticks;
                        timeBalance += 1 - (int)(workEnd - workStart);

                        if (timeBalance > 0)
                        {
                            Thread.Sleep(timeBalance);
                            workStart = (uint)Ticks;
                            timeBalance -= (int)(workStart - workEnd);
                        }
                        else
                        {
                            if (timeBalance < -1000)
                            {
                                //We can not increase forever if we get out of
                                //sync. At some point we have to print out a warning
                                //and catch up some time!
                                if (log.IsWarnEnabled && timeBalance < -2000)
                                {
                                    // Again, too much warning spam is meaningless.  Will warn if time sync is more than the typical 1 to 2 seconds
                                    // -tolakram
                                    log.WarnFormat("Timer Manager ({0}) out of sync, over 2000ms lost! {1}ms", m_name, timeBalance);
                                }
                                timeBalance += 1000;
                            }
                            workStart = workEnd;
                        }
                    }
                    catch (ThreadAbortException e)
                    {
                        if (log.IsWarnEnabled)
                            log.WarnFormat("Timer Manager thread ({0}) was aborted{1}{2}", m_name, Environment.NewLine, e);
                        m_running = false;
                        break;
                    }
                    catch (Exception e)
                    {
                        if (log.IsErrorEnabled)
                            log.ErrorFormat("Exception in Timer Manager ({0})!{1}{2}", m_name, Environment.NewLine, e);
                    }
                }

                log.InfoFormat("Stopped Timer Manager thread {0} (ID:{1})", m_name, Thread.CurrentThread.ManagedThreadId);
            }
        }

        #endregion
    }
}

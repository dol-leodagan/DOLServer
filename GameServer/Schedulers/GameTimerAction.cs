﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

namespace DOL.GS.Schedulers
{
    /// <summary>
    /// Lambda Action/Function to be run in a GameTimer Scheduler. 
    /// </summary>
    public sealed class GameTimerAction : GameTimer
    {
        /// <summary>
        /// Get or Set the On Tick Function, Returning Next Interval Value
        /// Return 0 to stop the callback
        /// </summary>
        public Func<int> OnTickEvent { get; private set; }
        
        /// <summary>
        /// Create a new Instance of <see cref="GameTimerAction"/>
        /// </summary>
        /// <param name="Manager">Timer Manager.</param>
        /// <param name="OnTickEvent">Event Function.</param>
        public GameTimerAction(GameTimer.TimeManager Manager, Func<int> OnTickEvent)
            : base(Manager)
        {
            this.OnTickEvent = OnTickEvent;
        }
        
        /// <summary>
        /// Create a new Instance of <see cref="GameTimerAction"/>
        /// </summary>
        /// <param name="Manager">Timer Manager.</param>
        /// <param name="OnTickEvent">Event Action.</param>
        public GameTimerAction(GameTimer.TimeManager Manager, Action OnTickEvent)
            : base(Manager)
        {
            this.OnTickEvent = () => { OnTickEvent(); return 0; };
        }
        
        protected override void OnTick()
        {
            Interval = OnTickEvent();
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.CodeDom.Compiler;
using Microsoft.CSharp;

using log4net;

using YamlDotNet.Serialization;

using DOL.Utils.Paths;

namespace DOL.GS.Scripts
{
    /// <summary>
    /// ScriptManager handle dynamicly compiled modules.
    /// </summary>
    public class ScriptManager
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        DirectoryInfo ScriptsPath { get; set; }
        FileInfo ScriptsCompilationTargetPath { get; set; }
        FileInfo[] ScriptsCompilationAssemblies { get; set; }
        bool EnableScriptsCompilation { get; set; }
        FileInfo[] AdditionalLibraries { get; set; }
        DirectoryInfo RootPath { get; set; }
        
        readonly HashSet<Assembly> ExternalAssemblies = new HashSet<Assembly>();
        
        public Assembly[] ScriptsAssembliesOnly
        {
            get { return ExternalAssemblies.ToArray(); }
        }
        
        public Assembly[] ScriptsAssemblies
        {
            get { return ExternalAssemblies.Concat(new [] { typeof(GameServer).Assembly }).ToArray(); }
        }

        /// <summary>
        /// Create a new Instance of <see cref="ScriptManager"/>
        /// </summary>
        /// <param name="ScriptsPath"></param>
        /// <param name="ScriptsCompilationTargetPath"></param>
        /// <param name="ScriptsCompilationAssemblies"></param>
        /// <param name="EnableScriptsCompilation"></param>
        /// <param name="AdditionalLibraries"></param>
        /// <param name="RootPath"></param>
        public ScriptManager(DirectoryInfo ScriptsPath, FileInfo ScriptsCompilationTargetPath, IEnumerable<FileInfo> ScriptsCompilationAssemblies, bool EnableScriptsCompilation, IEnumerable<FileInfo> AdditionalLibraries, DirectoryInfo RootPath)
        {
            this.ScriptsPath = ScriptsPath;
            this.ScriptsCompilationTargetPath = ScriptsCompilationTargetPath;
            this.ScriptsCompilationAssemblies = ScriptsCompilationAssemblies.ToArray();
            this.EnableScriptsCompilation = EnableScriptsCompilation;
            this.AdditionalLibraries = AdditionalLibraries.ToArray();
            this.RootPath = RootPath;
        }
        
        public bool Init()
        {
            // Load Additional Assemblies
            foreach (var lib in AdditionalLibraries)
                LoadExternalAssembly(lib);
            
            // Check if Compilation is Enabled
            if (EnableScriptsCompilation)
                return CheckAndCompileScriptAssembly();
            
            // or Load Scripts Assembly if available
            if (ScriptsCompilationTargetPath.Exists)
                LoadExternalAssembly(ScriptsCompilationTargetPath);
            else if (log.IsWarnEnabled)
                log.WarnFormat("Script Compilation Disabled, and Scripts Target '{0}' could not be found !", ScriptsCompilationTargetPath.FullName);
            
            return true;
        }
        
        public void Start(GameServer Server)
        {
            // Notify All Scripts Assemblies
            var scriptInitMethods = ScriptsAssemblies.Select(asm => asm.GetTypes()
                                                             .Where(type => type.IsClass))
                .SelectMany(types => types)
                .Select(type => type.GetMethods(BindingFlags.Public | BindingFlags.Static)
                        .Where(method => method.GetCustomAttribute<ScriptsInitEventAttribute>() != null))
                .SelectMany(methods => methods);
            
            foreach (var method in scriptInitMethods)
            {
                try
                {
                    method.Invoke(null, new object[] { Server });
                }
                catch (Exception ex)
                {
                    if (log.IsErrorEnabled)
                        log.ErrorFormat("Error while Initializing Script with InitEventAttribute - {0}.{1}{2}{3}",
                                        method.DeclaringType, method.Name, Environment.NewLine, ex);
                }
            }
        }
        
        public void Stop(GameServer Server)
        {
            // Notify All Scripts Assemblies
            var scriptUnloadMethods = ScriptsAssemblies.Select(asm => asm.GetTypes()
                                                             .Where(type => type.IsClass))
                .SelectMany(types => types)
                .Select(type => type.GetMethods(BindingFlags.Public | BindingFlags.Static)
                        .Where(method => method.GetCustomAttribute<ScriptsUnloadEventAttribute>() != null))
                .SelectMany(methods => methods);
            
            foreach (var method in scriptUnloadMethods)
            {
                try
                {
                    method.Invoke(null, new object[] { Server });
                }
                catch (Exception ex)
                {
                    if (log.IsErrorEnabled)
                        log.ErrorFormat("Error while Unloading Script with InitEventAttribute - {0}.{1}{2}{3}",
                                        method.DeclaringType, method.Name, Environment.NewLine, ex);
                }
            }
        }
        
        void LoadExternalAssembly(FileInfo lib)
        {
            var asm = lib.Exists ? Assembly.LoadFile(lib.FullName) : Assembly.LoadFile(lib.Name);
            
            // Register Assembly
            if (!ExternalAssemblies.Add(asm))
                throw new InvalidOperationException(string.Format("Assembly {0} is already loaded in Script Manager", asm));
        }
        
        bool CheckAndCompileScriptAssembly()
        {
            var hash = new SHA256Managed();
            
            var sources = ScriptsPath.EnumerateFiles("*", SearchOption.AllDirectories)
                .Where(f => f.Extension.Equals(".cs", StringComparison.OrdinalIgnoreCase))
                .Select(f => new { File = f, Relative = f.GetRelativePath(ScriptsPath), Hash = Convert.ToBase64String(hash.ComputeHash(File.ReadAllBytes(f.FullName))) })
                .ToArray();
            
            // check the config file
            var manifestFile = new FileInfo(string.Format("{0}.yml", ScriptsCompilationTargetPath.FullName));
            
            var compilationRequired = false;
            
            // Check if needed file exists...
            if (!ScriptsCompilationTargetPath.Exists)
            {
                if (log.IsDebugEnabled)
                    log.DebugFormat("Scripts Assembly '{0}' could not be found, recompilation will trigger...", ScriptsCompilationTargetPath);
                
                compilationRequired = true;
            }
            else if (!manifestFile.Exists)
            {
                if (log.IsDebugEnabled)
                    log.DebugFormat("Scripts Assembly Manifest '{0}' could not be found, recompilation will trigger...", manifestFile);
                
                compilationRequired = true;
            }
            else
            {
                // Compare Manifest Files
                using (TextReader reader = manifestFile.OpenText())
                {
                    var results = new Deserializer().Deserialize<IEnumerable<SourceElement>>(reader).ToList();
                    
                    foreach(var existing in sources)
                    {
                        var element = results.FirstOrDefault(el => el.Name.Equals(existing.Relative)
                                                              && existing.File.Length == el.Size
                                                              && existing.File.LastWriteTimeUtc.ToFileTimeUtc() == el.LastModified
                                                              && existing.Hash == el.Hash);
                        if (element == null)
                        {
                            if (log.IsDebugEnabled)
                                log.Debug("At Least one file differs between Scripts Assembly Manifest and Source Files, recompilation will trigger...");
                            
                            compilationRequired = true;
                            break;
                        }
                       
                       results.Remove(element);
                    }
                    
                    // Some source files were removed compared to manifest.
                    if (!compilationRequired && results.Any())
                    {
                        if (log.IsDebugEnabled)
                            log.Debug("Scripts Assembly Manifest lists files that don't exist anymore, recompilation will trigger...");
                        
                        compilationRequired = true;
                    }
                }
            }
            
            if (compilationRequired)
            {
                if (log.IsInfoEnabled)
                    log.Info("Scripts Assembly is being Compiled...");
                
                if (ScriptsCompilationTargetPath.Exists)
                    ScriptsCompilationTargetPath.Delete();
                
                var compiler = new CSharpCodeProvider(new Dictionary<string, string> { { "CompilerVersion", "v4.0" } });
                
                #if DEBUG
                var param = new CompilerParameters(ScriptsCompilationAssemblies.Select(f => f.Name).ToArray(), ScriptsCompilationTargetPath.FullName, true);
                #else
                var param = new CompilerParameters(ScriptsCompilationAssemblies.Select(f => f.Name).ToArray(), ScriptsCompilationTargetPath.FullName, false);
                #endif
                
                param.GenerateExecutable = false;
                param.GenerateInMemory = false;
                param.WarningLevel = 2;
                param.CompilerOptions = string.Format("/optimize /lib:{0}", Path.Combine(RootPath.FullName, "lib"));
                param.ReferencedAssemblies.Add("System.Core.dll");
                
                var compileResult = compiler.CompileAssemblyFromFile(param, sources.Select(elem => elem.File.FullName).ToArray());

                //After compiling, collect
                GC.Collect();
                
                
                // Check for Errors and exit if any.
                if (compileResult.Errors.HasErrors)
                {
                    foreach (CompilerError err in compileResult.Errors)
                    {
                        if (err.IsWarning)
                            continue;
                        
                        if (log.IsErrorEnabled)
                            log.ErrorFormat("Error while compiling Scripts Assembly: {0}{1}    {2} Line:{3} Col:{4}",
                                            err.ErrorText, Environment.NewLine,
                                            err.FileName, err.Line, err.Column);
                    }
                    
                    return false;
                }
                
                // Overwrite Manifest File.
                using (TextWriter writer = manifestFile.CreateText())
                {
                    new Serializer().Serialize(writer, sources.Select(elem => new SourceElement { Name = elem.Relative, Size = elem.File.Length, LastModified = elem.File.LastWriteTimeUtc.ToFileTimeUtc(), Hash = elem.Hash }));
                }
                
                // Register Assembly
                if (!ExternalAssemblies.Add(compileResult.CompiledAssembly))
                    throw new InvalidOperationException(string.Format("Assembly {0} is already loaded in Script Manager", compileResult.CompiledAssembly));
            }
            else
            {
                LoadExternalAssembly(ScriptsCompilationTargetPath);
            }

            return true;
        }
    }
    
    /// <summary>
    /// Source File Identifier.
    /// </summary>
    class SourceElement
    {
        public string Name { get; set; }
        public long Size { get; set; }
        public long LastModified { get; set; }
        public string Hash { get; set; }
    }
}

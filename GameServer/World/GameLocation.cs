﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

namespace DOL.GS.World
{
    /// <summary>
    /// Description of GameLocation.
    /// </summary>
    public class GameLocation : RegionLocation, IGameLocation
    {
        public const int OBJECT_IN_FRONT_DEFAULT_THRESHOLD = 32;
        
        public ushort Heading { get; protected set; }
        
        public bool IsObjectInFront(IRegionLocation Location, double ViewAngle)
        {
            return IsObjectInFront(Location, ViewAngle, OBJECT_IN_FRONT_DEFAULT_THRESHOLD);
        }
        
        public bool IsObjectInFront(IRegionLocation Location, double ViewAngle, double RangeThreshold)
        {
            if (RegionId == Location.RegionId)
            {
                var angleDiff = (GetHeadingTo(Location) & 0xFFF) - (Heading & 0xFFF);
                if (angleDiff < 0)
                    angleDiff += 4096;
                
                var angle = angleDiff * 360.0 / 4096.0;
                
                if (angle >= 360 - ViewAngle / 2 || angle < ViewAngle / 2)
                    return true;
                
                if (RangeThreshold > 0)
                    return IsWithinRadius(Location, RangeThreshold);
            }
            
            return false;
        }
        
        public GameLocation(int X, int Y, int Z, int RegionId, ushort Heading)
            : base(X, Y, Z, RegionId)
        {
            this.Heading = Heading;
        }
        
        public GameLocation()
        {
        }
    }
}

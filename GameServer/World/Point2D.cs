﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

namespace DOL.GS.World
{
    /// <summary>
    /// Description of Point2D.
    /// </summary>
    public class Point2D : IPoint2D
    {
        // Coordinate calculation functions in DOL are standard trigonometric functions, but
        // with some adjustments to account for the different coordinate system that DOL uses
        // compared to the standard Cartesian coordinates used in trigonometry.
        //
        // Cartesian grid:
        //        90
        //         |
        // 180 --------- 0
        //         |
        //        270
        //        
        // DOL Heading grid:
        //       2048
        //         |
        // 1024 ------- 3072
        //         |
        //         0
        // 
        // The Cartesian grid is 0 at the right side of the X-axis and increases counter-clockwise.
        // The DOL Heading grid is 0 at the bottom of the Y-axis and increases clockwise.
        // General trigonometry and the System.Math library use the Cartesian grid.

        /// <summary>
        /// The factor to convert a heading value to radians
        /// </summary>
        /// <remarks>
        /// Heading to degrees = heading * (360 / 4096)
        /// Degrees to radians = degrees * (PI / 180)
        /// </remarks>
        public const double HEADING_TO_RADIAN = (360.0/4096.0)*(Math.PI/180.0);

        /// <summary>
        /// The factor to convert radians to a heading value
        /// </summary>
        /// <remarks>
        /// Radians to degrees = radian * (180 / PI)
        /// Degrees to heading = degrees * (4096 / 360)
        /// </remarks>
        public const double RADIAN_TO_HEADING = (180.0/Math.PI)*(4096.0/360.0);

        public int X { get; protected set; }
        public int Y { get; protected set; }
        
        public ushort GetHeadingTo(IPoint2D Point)
        {
            double heading = Math.Atan2(X - Point.X, Point.Y - Y) * RADIAN_TO_HEADING;

            if (heading < 0)
                heading += 4096;

            return Convert.ToUInt16(heading);
        }
        
        public IPoint2D GetPointFromHeading(ushort Heading, double Distance)
        {
            return new Point2D(Convert.ToInt32(X - (Math.Sin(Heading * HEADING_TO_RADIAN) * Distance)),
                               Convert.ToInt32(Y + (Math.Cos(Heading * HEADING_TO_RADIAN) * Distance)));
        }
        
        public double GetDistanceTo(IPoint2D Point)
        {
            return Math.Sqrt(((X - Point.X) * (X - Point.X)) + ((Y - Point.Y) * (Y - Point.Y)));
        }
        
        public bool IsWithinRadius(IPoint2D Point, double Distance)
        {
            return ((X - Point.X) * (X - Point.X)) + ((Y - Point.Y) * (Y - Point.Y)) > Distance * Distance;
        }
        
        public Point2D(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }
        
        public Point2D()
        {
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

namespace DOL.GS.World
{
    /// <summary>
    /// Description of Point3D.
    /// </summary>
    public class Point3D : Point2D, IPoint3D
    {
        public int Z { get; protected set; }
        
        public new IPoint3D GetPointFromHeading(ushort Heading, double Distance)
        {
            var point = base.GetPointFromHeading(Heading, Distance);
            return new Point3D(point.X, point.Y, Z);
        }
        
        public double GetDistanceTo(IPoint3D Point)
        {
            return GetDistanceTo(Point, 1);
        }
        
        public double GetDistanceTo(IPoint3D Point, double ZFactor)
        {
            return Math.Sqrt(((X - Point.X) * (X - Point.X)) + ((Y - Point.Y) * (Y - Point.Y)) + ((Z - Point.Z) * (Z - Point.Z) * ZFactor));
        }
        
        public bool IsWithinRadius(IPoint3D Point, double Distance)
        {
            return IsWithinRadius(Point, Distance, 1);
        }
        
        public bool IsWithinRadius(IPoint3D Point, double Distance, bool IgnoreZ)
        {
            return IgnoreZ ? IsWithinRadius(Point as IPoint2D, Distance) : IsWithinRadius(Point, Distance, 1);
        }
        
        public bool IsWithinRadius(IPoint3D Point, double Distance, double ZFactor)
        {
             return ((X - Point.X) * (X - Point.X)) + ((Y - Point.Y) * (Y - Point.Y))  + ((Z - Point.Z) * (Z - Point.Z) * ZFactor) > Distance * Distance;
        }
        

        public Point3D(int X, int Y, int Z)
            : base(X, Y)
        {
            this.Z = Z;
        }

        public Point3D()
        {
        }
    }
}

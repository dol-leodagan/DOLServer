﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

namespace DOL.GS.World
{
    /// <summary>
    /// Realm.
    /// </summary>
    public sealed class Realm
    {
        public string Name { get; private set; }
        
        public Realm(string Name)
        {
            this.Name = Name;
        }
        
        #region Equals and GetHashCode implementation
        public override bool Equals(object obj)
        {
            var other = obj as Realm;
            if (other == null)
                return false;
            return this.Name == other.Name;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;
            unchecked {
                if (Name != null)
                    hashCode += 1000000007 * Name.GetHashCode();
            }
            return hashCode;
        }

        public static bool operator ==(Realm lhs, Realm rhs) {
            if (ReferenceEquals(lhs, rhs))
                return true;
            if (ReferenceEquals(lhs, null) || ReferenceEquals(rhs, null))
                return false;
            return lhs.Equals(rhs);
        }

        public static bool operator !=(Realm lhs, Realm rhs) {
            return !(lhs == rhs);
        }
        #endregion
    }
}

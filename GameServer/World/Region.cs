﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Collections.Generic;

using KDSharp.KDTree;
using log4net;

using DOL.GS.GameObjects;
using DOL.GS.Schedulers;
using DOL.GS.Scripts;

namespace DOL.GS.World
{
    public class RegionProperties : AbstractServerProperties
    {
        [ServerProperty(Default = 60000)]
        public int REGION_MAX_ALLOWED_OBJECTS { get; set; }

        [ServerProperty(Default = 256)]
        public int REGION_INTERNAL_ARRAY_INCREMENTS { get; set; }
    }
    /// <summary>
    /// Region holds all object interacting within bound of "Zone Loading"
    /// </summary>
    public class Region
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        readonly int REGION_MAX_ALLOWED_OBJECTS;
        readonly int REGION_INTERNAL_ARRAY_INCREMENTS;
        
        /// <summary>
        /// Lock object from Objects Array
        /// </summary>
        readonly object ObjectsSyncLock = new object();

        GameObject[] Objects;
        
        SortedList<int, uint> AvailableIndices { get; set; }
        uint ObjectsUpperLimit { get; set; }
        
        GameTimer.TimeManager TimeManager;
        
        KDTree<GameObject> MovingObjects { get; set; }
        
        public ushort Id { get; protected set; }
        
        public Region(GameServer Server, GameTimer.TimeManager TimeManager, ushort Id)
        {
            this.TimeManager = TimeManager;
            this.Id = Id;
            Objects = new GameObject[0];
            ObjectsUpperLimit = 0;
            MovingObjects = new KDTree<GameObject>(new KDSharp.DistanceFunctions.SquaredEuclideanDistanceWithTranslation(() => TimeManager.CurrentTime, 3), 7);
            
            REGION_MAX_ALLOWED_OBJECTS = Server.Properties.Get<RegionProperties>().REGION_MAX_ALLOWED_OBJECTS;
            REGION_INTERNAL_ARRAY_INCREMENTS = Server.Properties.Get<RegionProperties>().REGION_INTERNAL_ARRAY_INCREMENTS;
        }
        
        void RegenObjectsTree()
        {
            MovingObjects.Regen();
            MovingObjects.TrimExcess();
        }
        
        void ResizeInternalArray()
        {
            var newSize = Math.Min(Objects.Length + REGION_INTERNAL_ARRAY_INCREMENTS, REGION_MAX_ALLOWED_OBJECTS);
            
            if (newSize <= Objects.Length)
                throw new InvalidOperationException("Internal Array is at Maximum allowed size.");
            
            Array.Resize<GameObject>(ref Objects, newSize);
        }
        
        internal uint AddObject(GameObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            
            lock (ObjectsSyncLock)
            {
                var index = ObjectsUpperLimit;
                
                if (AvailableIndices.Count > 0)
                {
                    var last = AvailableIndices.Count - 1;
                    index = AvailableIndices[last];
                    AvailableIndices.RemoveAt(last);
                }
                
                if (index > Objects.Length - 1)
                    ResizeInternalArray();
                
                Objects[index] = obj;
                ++ObjectsUpperLimit;
                return Convert.ToUInt32(index + 1);
            }
        }
        
        internal void RemoveObject(GameObject obj)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");
            
            lock (ObjectsSyncLock)
            {
                if (!obj.ObjectId.HasValue)
                    throw new InvalidOperationException("Object has no Id Value, was it added before ?");
                
                var index = Convert.ToInt32(obj.ObjectId.Value - 1);
                
                var existingObj = Objects[index];
                
                if (existingObj != obj)
                    throw new InvalidOperationException("Object's Id Value does not match its position in Internal array.");
                
                Objects[index] = null;
                
                if (index == ObjectsUpperLimit - 1)
                {
                    --ObjectsUpperLimit;
                    // Compact Data Collection
                    while (AvailableIndices.Count > 0 && AvailableIndices.Values[0] == ObjectsUpperLimit - 1)
                    {
                        --ObjectsUpperLimit;
                        AvailableIndices.RemoveAt(0);
                    }
                }
                else
                {
                    AvailableIndices.Add(-index, Convert.ToUInt32(index));
                }
                    
            }
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

namespace DOL.GS.World
{
    /// <summary>
    /// Description of RegionLocation.
    /// </summary>
    public class RegionLocation : Point3D, IRegionLocation
    {
        public int RegionId { get; protected set; }
        
        public new IRegionLocation GetPointFromHeading(ushort Heading, double Distance)
        {
            var point = base.GetPointFromHeading(Heading, Distance);
            return new RegionLocation(point.X, point.Y, point.Z, RegionId);
        }
        
        public double GetDistanceTo(IRegionLocation Point)
        {
            return GetDistanceTo(Point, 1);
        }
        
        public double GetDistanceTo(IRegionLocation Point, double ZFactor)
        {
            return RegionId != Point.RegionId ? double.PositiveInfinity : GetDistanceTo(Point as IPoint3D, ZFactor);
        }
        
        public bool IsWithinRadius(IRegionLocation Point, double Distance)
        {
            return IsWithinRadius(Point, Distance, 1);
        }
        
        public bool IsWithinRadius(IRegionLocation Point, double Distance, bool IgnoreZ)
        {
            return RegionId == Point.RegionId &&
                (IgnoreZ ? IsWithinRadius(Point as IPoint2D, Distance) : IsWithinRadius(Point, Distance, 1));
        }
        
        public bool IsWithinRadius(IRegionLocation Point, double Distance, double ZFactor)
        {
            return RegionId == Point.RegionId && IsWithinRadius(Point as IPoint3D, Distance, ZFactor);
        }
        
        
        public RegionLocation(int X, int Y, int Z, int RegionId)
            : base(X, Y, Z)
        {
            this.RegionId = RegionId;
        }
        
        public RegionLocation()
        {
        }
    }
}

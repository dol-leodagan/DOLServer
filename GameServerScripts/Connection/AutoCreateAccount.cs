﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;

using DOL.GS;
using DOL.GS.Scripts;
using DOL.GS.ClientObjects;
using DOL.Utils.Text;
using DOL.Utils.ThreadSafeCollections;
using DOL.Database;

using log4net;

namespace DOL.Scripts.Connection
{
    public class AutoCreateAccountProperties : AbstractServerProperties
    {
        [ServerProperty(Description = "Minimal Login (Account Name) Length allowed for Auto Account Creation.", Default = (byte)3)]
        public byte MINIMAL_LOGIN_LENGTH { get; set; }
        
        [ServerProperty(Description = "Minimal Password Length allowed for Auto Account Creation.", Default = (byte)8)]
        public byte MINIMAL_PASSWORD_LENGTH { get; set; }
        
        [ServerProperty(Description = "Maximal Login (Account Name) Length allowed for Auto Account Creation.", Default = (byte)22)]
        public byte MAXIMAL_LOGIN_LENGTH { get; set; }
        
        [ServerProperty(Description = "Maximal Password Length allowed for Auto Account Creation.", Default = (byte)32)]
        public byte MAXIMAL_PASSWORD_LENGTH { get; set; }
        
        [ServerProperty(Description = "Is Account Auto Creation Enabled ?", Default = true)]
        public bool ENABLE_ACCOUNT_AUTOCREATE { get; set; }
    }
    
    /// <summary>
    /// Handle Auto Account Creation Upon Connection.
    /// </summary>
    public class AutoCreateAccount : IDisposable
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        readonly static ReaderWriterDictionary<GameServer, AutoCreateAccount> _AutoCreateAccountListeners = new ReaderWriterDictionary<GameServer, AutoCreateAccount>();
        
        [ScriptsInitEvent]
        public static void Init(GameServer server)
        {
            _AutoCreateAccountListeners.Add(server, new AutoCreateAccount(server));
        }
        
        [ScriptsUnloadEvent]
        public static void Unload(GameServer server)
        {
            AutoCreateAccount autocreate;
            if (_AutoCreateAccountListeners.TryRemove(server, out autocreate))
                autocreate.Dispose();
        }
        
        GameServer Server { get; set; }
        
        /// <summary>
        /// Create a new instance of <see cref="AutoCreateAccount"/> Manager.
        /// </summary>
        /// <param name="Server">GameServer to Listen to.</param>
        public AutoCreateAccount(GameServer Server)
        {
            this.Server = Server;
            this.Server.GlobalEvents.ClientAccountCreated += OnClientAccountCreated;
        }
        
        public void Dispose()
        {
            Server.GlobalEvents.ClientAccountCreated -= OnClientAccountCreated;
            Server = null;
        }
        
        void OnClientAccountCreated(GameServer sender, GameClientHandler client, GameAccount account)
        {
            var properties = Server.Properties.Get<AutoCreateAccountProperties>();
            if (!properties.ENABLE_ACCOUNT_AUTOCREATE)
                return;
            
            if (account.isAllowedToConnect)
                return;
            
            var loginMinimalLength = properties.MINIMAL_LOGIN_LENGTH;
            var passwordMinimalLength = properties.MINIMAL_PASSWORD_LENGTH;
            var loginMaximalLength = properties.MAXIMAL_LOGIN_LENGTH;
            var passwordMaximalLength = properties.MAXIMAL_PASSWORD_LENGTH;
            
            if (account.DeniedReason == eConnectionDeniedReason.WrongLogin || account.DeniedReason == eConnectionDeniedReason.Undefined)
            {
                if (account.Login.Length >= loginMinimalLength
                    && account.Login.Length <= loginMaximalLength
                    && account.Password.Length >= passwordMinimalLength
                    && account.Password.Length <= passwordMaximalLength)
                {
                    try
                    {
                        using (var context = Server.Database.GetContext<DOLDatabaseContext>())
                        {
                            // Create if no Account Exists...
                            if (context.Accounts.Any(acc => acc.Name == account.Login))
                                return;
                                
                            var newAccount = context.Accounts.Create();
                            newAccount.Name = account.Login;
                            newAccount.Password = account.Password.GeneratePasswordHash();
                            context.Accounts.Add(newAccount);
                            context.SaveChanges();

                            account.LoadFromDatabase(newAccount);
                            account.DeniedReason = eConnectionDeniedReason.None;
                        }
                        
                        if (log.IsInfoEnabled)
                            log.InfoFormat("New Account Auto-Created : Login {0} (client: {1})", account.Login, client);
                    }
                    catch (Exception ex)
                    {
                        if (log.IsErrorEnabled)
                            log.ErrorFormat("Error while Auto Account Creation : Login {0} (client: {1}){2}{3}",
                                           account.Login, client, Environment.NewLine, ex);
                        
                        account.DeniedReason = eConnectionDeniedReason.Error;
                    }
                }
                else
                {
                    if (log.IsInfoEnabled)
                        log.InfoFormat("Auto Account Creation Denied because of Invalid Credentials : Login {0} - Password Length {1} (client: {2})",
                                       account.Login, account.Password.Length, client);
                    
                    account.DeniedReason = eConnectionDeniedReason.InvalidCredentials;
                }
            }
        }
    }
}

﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;
using System.Linq;

using DOL.GS;
using DOL.GS.Scripts;
using DOL.GS.Schedulers;

using log4net;

namespace DOL.Scripts.Tests
{
    /// <summary>
    /// Dumb Test Script.
    /// </summary>
    public static class ScriptTest
    {
        /// <summary>
        /// Defines a logger for this class.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [ScriptsInitEvent]
        public static void Init(GameServer Server)
        {
            log.InfoFormat("Test Script Successfully Initialized with given GameServer Object : {0}", Server);
            Server.ServerScheduler.Start(() => { var dumb = Enumerable.Range(0, 1000).ToArray(); return 500; }, 1);
            
            foreach (var timer in Server.WorldManager.TimeManagers)
                new GameTimerAction(timer, () =>  { var dumb = Enumerable.Range(0, 1000).ToArray(); return 500; }).Start(1);
            
            using (var ctx = Server.Database.GetContext<TestContext>())
            {
                var accrel = ctx.AccountRelations.Include("Account").ToArray();
            }
            
            using (var ctx = Server.Database.GetContext<TestContext>())
            {
                var accrel = ctx.TestAutoIncGenerations.Add(new TestAutoIncGeneration());
                ctx.SaveChanges();
            }
            
            using (var ctx = Server.Database.GetContext<TestContext>())
            {
                var accrel = ctx.TestGuidGenerations.Add(new TestGuidGeneration());
                ctx.SaveChanges();
            }
        }
        
        [ScriptsUnloadEvent]
        public static void Unload(GameServer Server)
        {
            log.InfoFormat("Test Script Successfully Unloaded with given GameServer Object : {0}", Server);
        }
    }
}
﻿/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
using System;

using System.Data.Entity;

using DOL.Database;
using DOL.Database.DataObjects;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOL.Scripts.Tests
{
    /// <summary>
    /// Dumb Modular Context
    /// </summary>
    public class TestContext : BaseContext<TestContext>
    {
        public DbSet<AccountTestRelation> AccountRelations {get; set; }
        public DbSet<TestGuidGeneration> TestGuidGenerations {get; set; }
        public DbSet<TestAutoIncGeneration> TestAutoIncGenerations {get; set; }
        
        public TestContext(DatabaseHandler DbHandler)
            : base(DbHandler)
        {
        }
    }
    
    public class AccountTestRelation : DataObject
    {
        public int AccountTestRelationId { get; set; }
        public uint TestRelationUint { get; set; }
        public Account Account { get; set; }
    }
    
    public class TestAutoIncGeneration : DataObject
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }
    
    public class TestGuidGeneration : DataObject
    {
        public Guid Id { get; set; }
        
        public TestGuidGeneration()
        {
            Id = Guid.NewGuid();
        }
    }
}
